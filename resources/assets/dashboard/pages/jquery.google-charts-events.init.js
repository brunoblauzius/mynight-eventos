/**
 * Theme: Adminox Admin Template
 * Author: Coderthemes
 * Module/App: Flot-Chart
 */

! function($) {
    "use strict";

    var GoogleChart = function() {
        this.$body = $("body")
    };
    //creates donut chart
    GoogleChart.prototype.createDonutChart = function(selector, data, colors) {
        var options = {
            fontName: 'Open Sans',
            fontSize: 13,
            height: 300,
            pieHole: 0.55,
            width: 500,
            chartArea: {
                left: 50,
                width: '100%',
                height: '100%'
            },
            colors: colors
        };

        var google_chart_data = google.visualization.arrayToDataTable(data);
        var pie_chart = new google.visualization.PieChart(selector);
        pie_chart.draw(google_chart_data, options);
        return pie_chart;
    },
    //init
    GoogleChart.prototype.init = function (pie_data) {
        var $this = this;

        //creating donut chart
        $this.createDonutChart($('#donut-chart-event')[0], pie_data, ['#5553ce','#297ef6', '#e52b4c', '#ffa91c', '#32c861']);

        //on window resize - redrawing all charts
        $(window).on('resize', function() {
            $this.createDonutChart($('#donut-chart-event')[0], pie_data, ['#188ae2', '#4bd396', '#f9c851', '#f5707a', '#6b5fb5']);
        });
    },
    //init GoogleChart
    $.GoogleChart = new GoogleChart, $.GoogleChart.Constructor = GoogleChart
}(window.jQuery),

//initializing GoogleChart
function($) {
    "use strict";
    //loading visualization lib - don't forget to include this
    google.load("visualization", "1", {packages:["corechart"]});
    //after finished load, calling init method
    google.setOnLoadCallback(function() {

        //creating pie chart
        var pie_data =[];

        $.ajax({
            url: window.web_app + '/api/events/'+window.event_id+'/payment-sales',
            data:{},
            dataType :'json',
            method:'get'
        }).done(function (response){
            pie_data = response;
            $.GoogleChart.init(pie_data);
        }).fail(function (response) {
        });



    });
}(window.jQuery);
