/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('notifications', require('./notifications/Notifications.vue'));

Vue.use(require('vue-pusher'), {
    api_key: 'a7f09455c7520dc3626c',
    options: {
        cluster: 'us2',
        encrypted: true,
    }
});

const notification = new Vue({
    el: '#notification-list',
    created(){
        let that = this;
        let channel = this.$pusher.subscribe('estoque-baixo.'+window.user_id);
        channel.bind('estoque.baixo', function(data) {
            data = data.notification;
            if (("Notification" in window)) {

                if (Notification.permission === "granted") {
                    let options = {
                        tag: data.id,
                        body: data.description,
                        icon: window.web_app + '/dashboard/images/logo_sm.ico'
                    };

                    var notification = new Notification(data.title, options);
                    notification.onclick = function(){
                        window.location.href = window.web_app + '/admin/notifications/show';
                    }
                }
            }
        });
    }
});
