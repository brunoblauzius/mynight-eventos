@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Listas.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Lista.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row" style="margin-bottom: 20px;">
        <div class="col-12">
            <a class="btn btn-info pull-right" href="" id="btn-add-new">
                Adicionar nova lista
            </a>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Listas cadastradas.</b></h4>
                <p class="text-muted font-14 m-b-20">
                    Gerencie as listas de ingressos:
                </p>
                @if($listas->count() > 0)
                <table class="table m-0 table-colored-bordered table-bordered-inverse">
                        <thead>
                        <th>#</th>
                        <th>Descrição</th>
                        <th>Tipo do Público</th>
                        <th>Quantdade</th>
                        <th>Valor (R$)</th>
                        <th width="100"></th>
                        </thead>
                        <tbody>
                        @foreach( $listas as $lista)
                            <tr>
                                <td>{{str_pad($lista->id, 3, '0', STR_PAD_LEFT)}}</td>
                                <td><span class="text text-primary">{{$lista->description}}</span></td>
                                <td>
                                    @component('components.gender', ['gender' => $lista->gender]) @endcomponent
                                </td>
                                <td>
                                    {{$lista->amount}} un.
                                </td>
                                <td>
                                    @component('components.valor', ['valor' => $lista->value])@endcomponent
                                </td>
                                <td>
                                    <a href="{{route('admin.lists.edit.item', $lista->id)}}" class="btn btn-xs btn-primary" title="Editar"><i class="fa fa-pencil"></i></a>
                                    <a href="{{route('api.destroy.item',$lista->id)}}" class="btn btn-xs btn-danger delete-list" title="Excluir"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                </table>
                @else
                    <div class="alert alert-info text-center">
                        <strong>Nenhum registro encontrado.</strong>
                    </div>
                @endif
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Formulário</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{route('admin.lists.store')}}" method="post" id="form-cadastro">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group ">
                                            <input type="text" name="description" class="form-control" placeholder="Descrição">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            <input type="text" name="amount" class="form-control" placeholder="Quantidade de ingressos">
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-group ">
                                            <money name="value" class="form-control" placeholder="Valor (R$)"></money>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mt-3">
                                        <div class="custom-control custom-radio">
                                            <input name="gender" type="radio" id="masculino" class="custom-control-input" value="1">
                                            <label class="custom-control-label" for="masculino">Masculino</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input name="gender" type="radio" id="Feminino" class="custom-control-input" value="2">
                                            <label class="custom-control-label" for="Feminino">Feminino</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input name="gender" type="radio" id="Feminino|Masculino" class="custom-control-input" value="3">
                                            <label class="custom-control-label" for="Feminino|Masculino">Feminino|Masculino</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-center">
                                        <button class="btn btn-primary">salvar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        function destroy(element, href){
            if(confirm('Deseja realmente excluir esse item?')){
                return $.ajax({
                    url: href,
                    data:{},
                    method: 'delete',
                    dataType: 'json'
                });
            }
        }

        function editJson(element, href){
            return $.ajax({
                url: href,
                data:{},
                method: 'get',
                dataType: 'json'
            });
        }


        $(document).ready(function(){
            $('#btn-add-new').click(function(e){
                e.preventDefault();
                $('#myModal').modal('show');
            });


            $('#form-cadastro').submit(function(event){
                event.preventDefault();
                let element = $(this);
                let inputs = $(this).serialize();

                $.ajax({
                    url: element.attr('action'),
                    type: element.attr('method'),
                    dataType: 'json',
                    data: inputs
                })
                    .done(function(res){
                        alert(res[0]);
                        document.location.reload();
                    })
                    .fail(function(res){
                        let html = '';
                        $.each(res.responseJSON, function(item, value){
                            html += value;
                        });
                        alert(html);
                    });
            });


            $(document).on('click', '.delete-list', function(event){
                event.preventDefault();
                let element = $(this);
                let href = element.prop('href');
                destroy(element, href).done(function(response){
                    alert(response);
                    document.location.reload();
                }).fail(function(response){
                    alert(response.responseText);
                });
            });


            $(document).on('click', '.edit-list', function(event){
                event.preventDefault();
                let element = $(this);
                let href = element.prop('href');

                editJson(element, href).done(function(response){

                    let form = $('#form-cadastro');
                    form.prop('action',  );

                }).fail(function(response){
                    alert(response.responseText);
                });
            });

        });
    </script>
@endsection