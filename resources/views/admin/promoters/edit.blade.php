@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 15px;">
                <a href="{{route('admin.promoters')}}" class="btn btn-warning">Voltar</a>
            </div>
        </div>

        <div class="panel panel-site">
            <div class="panel-heading">Promoters - editar cadastro</div>
            <div class="panel-body">
                <div class="col-md-6">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{route('admin.promoters.update', $promoter->id)}}" method="post" name="form-promoter" id="form-promoter">
                        {{csrf_field()}}
                        <div class="form-group">
                            <small>Nome: <span>*</span></small>
                            <input type="text" class="form-control" name="name" id="name" placeholder="Nome:" value="{{$promoter->name}}">
                        </div>
                        <div class="form-group">
                            <small>E-mail: <span>*</span></small>
                            <input type="text" class="form-control" name="email" id="email" placeholder="E-mail:" value="{{$promoter->user->email}}">
                        </div>
                        <div class="form-group">
                            <small>Senha: <span>*</span></small>
                            <input type="password" class="form-control" name="password" id="password" placeholder="Senha:">
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-primary">Salvar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection