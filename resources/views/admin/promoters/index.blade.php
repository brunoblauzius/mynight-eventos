@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 15px;">
                <a href="{{route('admin.promoters.create')}}" class="btn btn-info pull-right">Cadastrar novo</a>
            </div>
        </div>

        <div class="panel panel-site">
            <div class="panel-heading">Promoters</div>
            <div class="panel-body">
                <table class="table table-striped table-head table-hover">
                    <thead>
                        <th>#</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>status</th>
                        <th></th>
                    </thead>
                    <tbody>
                        @if(count($promoters)>0)
                            @foreach( $promoters  as $promoter )
                                <tr>
                                    <td>{{$promoter->id}}</td>
                                    <td>{{$promoter->user->name}}</td>
                                    <td>{{$promoter->user->email}}</td>
                                    <td>
                                        @if($promoter->status)
                                            <small class="label label-success">Ativo</small>
                                        @else
                                            <small class="label label-danger">Inativo</small>
                                        @endif
                                    </td>
                                    <td width="70">
                                        <a href="{{route('admin.promoters.edit', $promoter->id)}}" class="btn btn-xs btn-info">Editar</a>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr class="table-warning">
                                <td colspan="5">Nenhum registro encontrado.</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                <nav class="m-t-10">
                    {{$promoters->links('vendor.pagination.bootstrap-4')}}
                </nav>
            </div>
        </div>

    </div>
@endsection