@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.list')}}">Meus Eventos.</a></li>
                    <li class="breadcrumb-item active">Vincular listas.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div style="margin-bottom:15px;">
                <a class="btn btn-warning" href="{{route('admin.events.edit',$event->id)}}"><i class="fa fa-mail-reply"></i> Voltar</a>
            </div>

            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Configuração do estoque para o evento - ( {{$event->title}} ).</b></h4>
                <form action="{{route('admin.event.bar.vinculate', $event->id)}}" method="post" id="form-promoter">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <small>Selecione o produto:</small>
                                <select name="produtos_id" id="produtos_id" class="form-control">
                                    <option value="">-- selecione --</option>
                                    @foreach( $produtos as $produto )
                                        <option value="{{$produto->id}}"> {{$produto->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-2">
                            <div class="form-group">
                                <small>Quantidade:</small>
                                <input class="form-control" name="amount" id="amount" type="number" placeholder="0">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <small>Valor(R$):</small>
                                <money class="form-control" name="value" id="value" type="text" placeholder="0,00"></money>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" style="padding-top: 22px">
                                <button class="btn btn-primary btn-block">salvar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Vinculados.</b></h4>

                @if($event->eventBar->count() > 0)
                    <table class="table m-0 table-colored-bordered table-bordered-inverse">
                        <thead class="">
                        <th>Produto</th>
                        <th>
                            Estoque<br>
                            Disponível
                        </th>
                        <th class="text-right">
                            Valor R$
                        </th>
                        <th width="110" class="text-right">Valor <br>Total R$</th>
                        <th width="80"></th>
                        </thead>
                        <tbody>
                        @php
                            $i          = 0;
                            $rendaTotal = 0;
                            $unTotal    = 0;
                        @endphp
                        @foreach( $event->eventBar as $bar)
                            <tr>
                                <td><strong>{{ucfirst($bar->produto->name)}}</strong></td>
                                <td>{{$bar->amount}} un.</td>
                                <td class="text-right">@component('components.valor', ['valor'=> $bar->value]) @endcomponent  </td>
                                <td class="text-right">@component('components.valor', ['valor'=> ($bar->amount * $bar->value)]) @endcomponent  </td>
                                <td>
                                    <a href="{{route('admin.event.produto.edit',[
                                        'event_id'   => $bar->events_id,
                                        'produto_id' => $bar->produto->id,
                                     ])}}" class="btn btn-xs btn-info btn-bordered"><i class="fa fa-pencil"></i></a>

                                    <a href="{{route('admin.event.produto.delete', [
                                        'event_id'   => $bar->events_id,
                                        'produto_id' => $bar->produto->id,
                                     ])}}" class="btn btn-xs btn-danger btn-bordered" title="Excluir"><i class="fa fa-times"></i></a>

                                </td>
                            </tr>
                            @php
                                $unTotal    += $bar->amount;
                                $rendaTotal += ( $bar->amount * $bar->value);
                                $i++;
                            @endphp
                        @endforeach
                        <tr>
                            <th colspan="1">Valor em estoque.</th>
                            <th colspan="1">{{$unTotal}} un.</th>
                            <th colspan="1"></th>
                            <th colspan="2">@component('components.valor', ['valor'=> $rendaTotal]) @endcomponent</th>
                        </tr>
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-info text-center">
                        Ainda não foi vinculado nenhum promoter para este evento.
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        let produtos = JSON.parse('{!! json_encode($produtos->toArray()) !!}');

        $(document).ready(function () {
            $('#produtos_id').change(function(){
                let element = $(this);
                let achou   = false;
                $.each(produtos, function(item , value){
                    if(element.val() == value.id){
                        achou = true;
                        $('#value').val(value.valor);
                    }
                });
                if(achou == false){
                    $('#value').val(null);
                }
            });
        });
    </script>
@endsection