@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos/Caixa.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.list')}}">Meus Eventos.</a></li>
                    <li class="breadcrumb-item active">Vincular Caixa.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div style="margin-bottom:15px;">
                <a class="btn btn-warning" href="{{route('admin.events.edit',$event->id)}}"><i class="fa fa-mail-reply"></i> Voltar</a>
            </div>

            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Configure o caixa para este evento.</b></h4>
                <form action="{{route('admin.events.vincularCaixa', $event->id)}}" method="post" id="form-promoter">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <small>Selecione o caixa para vinculars:</small>
                                <select name="users_id" id="users_id" class="form-control">
                                    <option value="">-- selecione --</option>
                                    @foreach( $users as $user )
                                        <option value="{{$user->id}}">{{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" style="padding-top: 22px">
                                <button class="btn btn-primary btn-block">salvar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-8">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Caixas Vinculados.</b></h4>

                @if($event->caixas->count() > 0)
                    <table class="table m-0 table-colored-bordered table-bordered-inverse">
                        <thead class="">
                        <th width="90%">Nome</th>
                        <th></th>
                        </thead>
                        <tbody>
                        @foreach( $event->caixas as $caixa)
                            <tr>
                                <td><strong>{{ucfirst($caixa->name)}}</strong></td>
                                <td>
                                    <a href="{{route('admin.events.desvincularCaixa',[
                                        'id' => $event->id,
                                        'user_id' => $caixa->id,
                                    ])}}" class="btn btn-xs btn-danger btn-bordered" title="Excluir"><i class="fa fa-times"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @else
                    <div class="alert alert-info text-center">
                        Ainda não foi vinculado nenhum promoter para este evento.
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection