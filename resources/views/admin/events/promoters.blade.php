@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.list')}}">Meus Eventos.</a></li>
                    <li class="breadcrumb-item active">Vincular listas.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div style="margin-bottom:15px;">
                <a class="btn btn-warning" href="{{route('admin.events.edit',$event->id)}}"><i class="fa fa-mail-reply"></i> Voltar</a>
            </div>

            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Configuração de promoters.</b></h4>
                <form action="{{route('admin.event.promoters.vinculate', $event->id)}}" method="post" id="form-promoter">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-4">
                            <div class="form-group">
                                <small>Selecione o promoter:</small>
                                <select name="user_id" id="promoter_id" class="form-control">
                                    <option value="">-- selecione --</option>
                                    @foreach( $promoters as $promoter )
                                        <option value="{{$promoter->id}}">{{$promoter->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-group">
                                <small>Selecione a lista:</small>
                                <select name="list_id" id="list_id" class="form-control">
                                    <option value="">-- selecione --</option>
                                    @foreach($event->eventLists as $list)
                                        <option value="{{$list->id}}">{{$list->description}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <small>Quantidade:</small>
                                <input class="form-control" name="amount" id="amount" type="number" placeholder="0">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" style="padding-top: 22px">
                                <button class="btn btn-primary btn-block">salvar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Promoters Vinculados.</b></h4>

                @if($event->eventPromoters->count() > 0)
                    <table class="table m-0 table-colored-bordered table-bordered-inverse">
                    <thead class="">
                    <th>Nome</th>
                    <th>Lista</th>
                    <th>
                        Quantidade<br>
                        Disponível
                    </th>
                    <th>
                        Valor R$
                    </th>
                    <th>Valor <br>Total R$</th>
                    <th></th>
                    </thead>
                    <tbody>
                    @php
                        $i = 0;
                        $rendaTotal = 0;
                        $unTotal    = 0;
                    @endphp
                    @foreach( $event->eventPromoters as $promoter)
                        <tr>
                            <td><strong>{{ucfirst($promoter->name)}}</strong></td>
                            <td>
                                {{$promoter->description}}<br>
                                @component('components.gender', ['gender' => $promoter->gender]) @endcomponent
                            </td>
                            <td>{{$promoter->pivot['amount']}} un.</td>
                            <td>@component('components.valor', ['valor'=> $promoter->value]) @endcomponent  </td>
                            <td>@component('components.valor', ['valor'=> ($promoter->pivot['amount'] * $promoter->value)]) @endcomponent  </td>
                            <td>
                                <a href="{{route('admin.event.desvincularPromoter', [
                                        'event_id' => $event->id,
                                        'id'       => $promoter->pivot['id']
                                     ])}}" class="btn btn-xs btn-danger btn-bordered" title="Excluir"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        @php
                            $unTotal    += $promoter->pivot['amount'];
                            $rendaTotal += ($promoter->pivot['amount'] * $promoter->value);
                            $i++;
                        @endphp
                    @endforeach
                    <tr>
                        <th colspan="2">Renda Total</th>
                        <th colspan="2">{{$unTotal}} un.</th>
                        <th colspan="2">@component('components.valor', ['valor'=> $rendaTotal]) @endcomponent</th>
                    </tr>
                    </tbody>
                </table>
                @else
                    <div class="alert alert-info text-center">
                        Ainda não foi vinculado nenhum promoter para este evento.
                    </div>
                @endif
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    let listas = JSON.parse('{!! json_encode($event->eventLists) !!}');
    $(document).ready(function () {
        $('#list_id').change(function(){
            let element = $(this);
            let achou   = false;
            $.each(listas, function(item , value){
                if(element.val() == value.id){
                    achou = true;
                    $('#amount').val(value.amount);
                }
            });
            if(achou == false){
                $('#amount').val(null);
            }
        });
    });
</script>
@endsection
