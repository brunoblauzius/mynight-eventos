@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.list')}}">Meus Eventos.</a></li>
                    <li class="breadcrumb-item active">Vincular listas.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12" style="padding-bottom: 25px;">
            <div class="card-box">
                <form action="{{route('admin.events.portaria', $event->id)}}" name="search-action">
                    <div class="row">
                        <div class="col-6">
                            <input type="text" id="search" name="search" value="{{old('search')}}" placeholder="Nome do cliente:" class="form-control">
                        </div>
                        <div class="col-2">
                            <button class="btn btn-primary btn-block" id="buscar">Buscar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card-box">

                <h4 class="m-t-0 header-title"><b>Evento: {{$event->title}}.</b></h4>
                @if($listas->count() > 0)
                    <table class="table m-0 table-colored-bordered table-bordered-inverse">
                        <thead>
                            <th>Nome</th>
                            <th>Promoter</th>
                            <th>Lista</th>
                        </thead>
                        <tbody>
                            @foreach( $listas as $lista )
                                <tr>
                                    <th class="text text-primary">{{$lista->name}}</th>
                                    <td>{{ucwords($lista->promoter)}}</td>
                                    <td>
                                        {{ucwords($lista->description)}}<br>
                                        @component('components.gender', ['gender' => $lista->gender]) @endcomponent
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div>
                        {{$listas->links()}}
                    </div>
                @else
                    <div class="alert alert-info text-center">
                        <strong>Nenhum registro foi encontrado para esse evento.</strong>
                    </div>
                @endif
            </div>
        </div>
    </div>

    {{--<script>--}}
        {{--$(document).ready(function(){--}}
           {{--$('#buscar').click(function(event){--}}
               {{--event.preventDefault();--}}
               {{--let anchor = $(this);--}}
               {{--let data = {search:$('#search').val()};--}}
               {{--let url  = anchor.data('href');--}}

               {{--postJson(anchor, data, url).done(function(response){--}}

               {{--});--}}

           {{--});--}}
        {{--});--}}
    {{--</script>--}}

@endsection
