@extends('layouts.dashboard')

@section('css')
    <link href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.list')}}">Meus Eventos.</a></li>
                    <li class="breadcrumb-item active">Cadastrar.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-12">
            <form class="" action="{{ route('admin.events.store') }}" method="post">
                {{ csrf_field() }}
                <div class="row">

                    <div class="col-8">
                        <div class="card-box">
                            <h4 class="m-t-0 m-b-20 header-title"><b>Cadastrar evento.</b></h4>

                            <div class="form-group">
                                <label for="title">Categoria do evento:
                                    <el-tooltip content="Esta opção selecionada a categoria do evento isso facilita na gestão do evento como um todo no sistema." placement="top">
                                        <i class="fa fa-question-circle text-primary"></i>
                                    </el-tooltip></label>
                                <select name="events_categories_id" class="form-control">
                                    <option value=""> selecione a categoria </option>
                                    @foreach($categories as $category)
                                        @php $selected = (old('events_categories_id')==$category->id)?'selected':'';@endphp
                                        <option {{$selected}} value="{{$category->id}}"> {{$category->name}} </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="title">Titulo do evento:</label>
                                <input type="text" class="form-control" name="title" id="title"
                                       value="{{ old('title') }}">
                            </div>
                            <div class="form-group">
                                <label for="meta_description">Meta Descrição:
                                    <el-tooltip content="Digite meta tags para o casamento ex. #casamento" placement="top">
                                        <i class="fa fa-question-circle text-primary"></i>
                                    </el-tooltip>
                                </label>
                                <textarea name="meta_description" rows="4" cols="80" class="form-control"
                                          id="meta_description" maxlength="156">{{ old('meta_description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="description">Descrição:</label>
                                <textarea name="description" style="height:300px " cols="80" class="form-control"
                                          id="description">{{ old('description') }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="card-box">
                            <h4 class="m-t-0 m-b-20 header-title"><b>Dados gerais do evento.</b></h4>

                            <div class="form-group">
                                <div class="checkbox">
                                    @php $checked = (old('visible_website')==1)? 'checked':''; @endphp
                                    <input type="hidden" class="" name="visible_website" id="" value="0">
                                    <input type="checkbox" {{$checked}} class="" name="visible_website" id="visible_website" value="1">
                                    <label for="visible_website">
                                        Deixar visível no site:
                                        <el-tooltip content="Esta opção selecionada deixa o evento visível no site, não é indicada para eventos privados ex. casamentos ou formaturas" placement="top">
                                            <i class="fa fa-question-circle text-primary"></i>
                                        </el-tooltip>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Inicio do evento.</label>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input class="form-control" name="start_at" placeholder="dd/mm/yyyy" id="datepicker" type="text" value="{{old('start_at')}}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input id="timepicker2" class="form-control timepicker2" name="start_hour" type="text">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-clock"></i></span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Fim do evento.</label>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="dd/mm/yyyy"  name="end_at" id="datepicker2" type="text" value="{{ old('end_at') }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input id="timepicker2" class="form-control timepicker2" name="end_hour" type="text">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-clock"></i></span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                </div>
                            </div>

                            <label for="local">Local:</label>
                            <div class="form-group">
                                <input type="radio" id="local.place" checked name="local" value="1">
                                <label for="local.place">{{ Auth::user()->place->name }}</label>
                            </div>

                            <div class="form-group">
                                <input type="radio" id="local.address" name="local" value="0">
                                <label for="local.address">Outro</label>
                                <input type="text" name="address" placeholder="Endereço" value="{{ old('address') }}"
                                       class="form-control">
                            </div>


                        </div>
                    </div>
                </div>

                <button type="submit" name="button" class="btn btn-primary">salvar</button>
            </form>

        </div>
    </div>

@section('js')

    <script src="{{ asset('plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <script src="{{ asset('js/eventos.init.js') }}"></script>
    <script src="{{ asset('admin/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#datepicker, #datepicker2').datepicker({
                format: "dd/mm/yyyy"
            });
            $('#datepicker').datepicker('setDate', moment(new Date()).format('DD/MM/YYYY'))
            $('#datepicker2').datepicker('setDate', moment(new Date()).add(1, 'days').format('DD/MM/YYYY'))

            $('.timepicker2').timepicker({
                showMeridian: false,
                icons: {
                    up: 'mdi mdi-chevron-up',
                    down: 'mdi mdi-chevron-down'
                }
            });

            tinymce.init({
                selector: 'textarea#description',
                toolbar: [
                    'undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright',
                ],
                menubar: false,
                branding: false
            });
        })
    </script>
@endsection

@endsection
