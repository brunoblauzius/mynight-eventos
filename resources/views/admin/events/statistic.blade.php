@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Evento - {{$evento->title}}.</h4>

                <div class="float-right">
                    <a href="{{route('admin.event.export.sales', $evento->id)}}" class="btn btn-success btn-bordered"><i class="fa fa-file-excel-o"></i> Exportar vendas</a>
                    <a class="btn btn-custom" href="{{ route('admin.events.edit', ['id' => $evento->id]) }}"><i
                                class="fa fa-pencil"></i> Editar</a>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection


@section('content')

    <div class="">
        <div class="row">
            <div class="col-sm-3 col-md-3 col-lg-3 col-xs-12">
                <div class="card m-b-30 text-white bg-success text-xs-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2 col-sm-2">
                                <i class="fa fa-money m-t-5" style="font-size: 30px;"></i>
                            </div>
                            <div class="col-10 col-sm-10">
                                    <span style="font-size: 25px; " class="m-b-0 m-t-0">
                                        R$ {{number_format($evento->sales->sum('total_value'), 2,',','.')}}
                                    </span><br>
                                <small>Vendas.</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3 col-md-3 col-xs-12">
                <div class="card m-b-30 text-white bg-purple text-xs-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2 col-sm-2">
                                <i class="fa fa-shopping-cart m-t-5" style="font-size: 30px;"></i>
                            </div>
                            <div class="col-10 col-sm-10">
                                <span style="font-size: 25px; "
                                      class="m-b-0 m-t-0">{{$evento->sales->count()}}</span><br>
                                <small>Transações.</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3 col-md-3 col-xs-12">
                <div class="card m-b-30 text-white bg-warning text-xs-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2 col-sm-2">
                                <i class="fa fa-laptop m-t-5" style="font-size: 30px;"></i>
                            </div>
                            <div class="col-10 col-sm-10">
                                <span style="font-size: 25px; "
                                      class="m-b-0 m-t-0">{{$evento->caixas->count()}}</span><br>
                                <small>Dispositivos.</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-3 col-md-3  col-xs-12">
                <div class="card m-b-30 text-white bg-custom text-xs-center">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-2 col-sm-2">
                                <i class="fa fa-dollar m-t-5" style="font-size: 30px;"></i>
                            </div>
                            <div class="col-10 col-sm-10">
                                <span style="font-size: 25px; " class="m-b-0 m-t-0">R$ {{$valueTotal}}</span><br>
                                <small>Valor total estoque.</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="card-box">
                <h4 class="header-title m-t-0">Produtos no estoque.</h4>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card-box">
                <h4 class="header-title m-t-0">Gráfico de tipos de pagamentos.</h4>
                <div class=" text-center">
                    <div class="chart m-t-30 col-xs-12" id="donut-chart-event">
                        <div class="alert alert-info text-center"><i class="fa fa-spin fa-spinner"
                                                                     style="width: auto;height: auto; line-height: 1px; margin-right: 10px;"></i>
                            Aguarde...
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12 col-xs-12">
            <div class="card-box">
                <h4 class="header-title m-t-0 m-b-30">Estatisticas.</h4>
                <ul class="nav nav-tabs tabs-bordered">
                    <li class="nav-item">
                        <a href="#home-b1" data-toggle="tab" aria-expanded="false" class="nav-link active show">
                            Estoque
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#home-b2" data-toggle="tab" aria-expanded="false" class="nav-link">
                            Transações
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#home-b3" data-toggle="tab" aria-expanded="false" class="nav-link">
                            Produtos mais vendidos
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#home-b4" data-toggle="tab" aria-expanded="false" class="nav-link">
                            Transações por Caixa
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#home-b5" data-toggle="tab" aria-expanded="false" class="nav-link">
                            Transações por Tipos de Pagamento
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active show" id="home-b1">
                        @if($evento->eventBar->count()>0)
                            <table class="table m-0 table-colored-bordered table-bordered-inverse">
                                <thead>
                                <th>Produto</th>
                                <th>Un.</th>
                                <th class="hide-phone">Valor(R$)</th>
                                </thead>
                                <tbody>
                                @foreach($evento->eventBar as $bar)
                                    <tr>
                                        <td>
                                            <small>
                                                <strong>@php echo ($bar->produto!=null)?$bar->produto->name:null;@endphp</strong>
                                            </small>
                                        </td>
                                        <td width="60">
                                            @php
                                                $amount = $bar->amount;
                                                $index  = -1;
                                                if($bar->produto != null && $products_sold != null && count($products_sold)>0){
                                                    $index = $products_sold->search(function ($item, $key) use ($bar) {
                                                        return $item->id == $bar->produto->id;
                                                    });
                                                    $amount = ($index !== false )?$products_sold[$index]->estoque:$bar->amount;
                                                }
                                            @endphp
                                            @if($bar->amount > 5)
                                                <strong style="font-size:14px;"
                                                        class="label label-success">{{$amount}}</strong>
                                            @else
                                                <strong class="label label-warning">{{$amount}}</strong>
                                            @endif
                                        </td>
                                        <td class="hide-phone">{{$bar->value}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                Nenhum produto cadastrado para o evento.
                            </div>
                        @endif
                    </div>
                    <div class="tab-pane" id="home-b2">
                        @if($evento->sales->count()>0)
                            <table class="table m-0 table-colored-bordered table-bordered-inverse">
                                <thead>
                                <tr>
                                    <th>Data</th>
                                    <th>Qtde. Produtos</th>
                                    <th>Total (R$)</th>
                                    <th>Modo de pagamento.</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $evento->sales as $sale )
                                    <tr>
                                        <td>{{$sale->data_venda}}</td>
                                        <td>{{$sale->total_amount}} Un.</td>
                                        <td>R$ {{number_format($sale->total_value, 2, ',', '.')}}</td>
                                        <td>
                                            <span class="label label-purple">{{join(', ', $sale->paymentsMethods->where('valor', '>', 0)->pluck('formaDePagamento.nome')->toArray())}}</span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                Nenhum produto cadastrado para o evento.
                            </div>
                        @endif
                    </div>
                    <div class="tab-pane" id="home-b3">
                        @if($produtos_mais_vendidos->count()>0)
                            <table class="table m-0 table-colored-bordered table-bordered-inverse">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Produto</th>
                                    <th>Total Vendido</th>
                                    <th>Total (R$)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $produtos_mais_vendidos as $produto_vendido )
                                    <tr>
                                        <td><img src="{{$produto_vendido->produto->thumb_path}}"
                                                 title="{{$produto_vendido->produto->name}}" class="thumb-md"></td>
                                        <td><strong>{{$produto_vendido->produto->name}}</strong></td>
                                        <td>{{$produto_vendido->amount}}</td>
                                        <td>R$ {{number_format($produto_vendido->value,2,',', '.')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                Nenhum produto cadastrado para o evento.
                            </div>
                        @endif
                    </div>
                    <div class="tab-pane" id="home-b4">
                        @if($sales_per_user->count()>0)
                            <table class="table m-0 table-colored-bordered table-bordered-inverse">
                                <thead>
                                <tr>
                                    <th>Operador</th>
                                    <th>Total de produtos</th>
                                    <th>Total em Vendas(R$)</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $sales_per_user as $sale_user )
                                    <tr>
                                        <td><strong>{{$sale_user->user->name}}</strong></td>
                                        <td>{{$sale_user->total_amount}}</td>
                                        <td>R$ {{number_format($sale_user->total_value,2,',', '.')}}</td>
                                        <td width="100">
                                            @php
                                                $index = -1;
                                                if($evento->closure!= null && count($evento->closure)>0){
                                                    $index = $evento->closure->search(function($item, $index) use ($sale_user){
                                                       return $item->users_id == $sale_user->user->id;
                                                    });
                                                }
                                            @endphp
                                            @if($index !== -1)
                                                <a href="{{route('api.fechamento.caixa',$evento->closure[$index]->id)}}"
                                                   target="_blank" class="btn btn-custom">
                                                    <i class="fa fa-print"></i> Imprimir Fechamento
                                                </a>
                                            @else

                                                <button
                                                        data-eventid="{{$evento->id}}"
                                                        data-userid="{{$sale_user->user->id}}"
                                                        class="btn btn-rounded btn-primary fechamento">
                                                    <i class="fi-ban"></i> FECHAR CAIXA
                                                </button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                Nenhum produto cadastrado para o evento.
                            </div>
                        @endif

                    </div>
                    <div class="tab-pane" id="home-b5">
                        @if($payment_sales->count()>0)
                            <table class="table m-0 table-colored-bordered table-bordered-inverse">
                                <thead>
                                <tr>
                                    <th>Tipo de Pagamento</th>
                                    <th>Total (R$)</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach( $payment_sales as $payment )
                                    <tr>
                                        <td><strong>{{$payment->nome}}</strong></td>
                                        <td>R$ {{number_format($payment->valor,2,',', '.')}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @else
                            <div class="alert alert-info text-center">
                                Nenhum produto cadastrado para o evento.
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection

@section('js')
    <script>window.event_id = '{{$evento->id}}'</script>
    <!-- Google Charts js -->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <!-- Init -->
    <script src="{{asset('dashboard/pages/jquery.google-charts-events.init.js')}}"></script>

    <script>
        var that = this;
        $(document).on('click', '.fechamento', function (event) {
            event.preventDefault();
            var event_id = $(this).data('eventid');
            var user_id = $(this).data('userid');
            $.ajax({
                url: window.web_app + '/api/forca-de-vendas/fechamento-caixa',
                data: {
                    users_id: user_id,
                    events_id: event_id
                },
                dataType: 'json',
                method: 'post'
            }).done(function (response) {
                that.Vue.swal(
                    'Sucesso',
                    'Fechamento concluido com sucesso.',
                    'success')
                    .then(function (response) {
                        window.location.reload();
                    });
            })
                .fail(function (response) {
                    let html = '';
                    that.dados_abertura_caixa.valor = 0;
                    if (typeof response.responseJSON == 'object' || typeof response.responseJSON == 'Array') {
                        $.each(response.responseJSON, function (item, value) {
                            html += value + "<br>";
                        });
                    } else {
                        html = response.responseJSON
                    }
                    that.Vue.swal(
                        'Atenção',
                        html,
                        'warning');
                });
        })
    </script>
@endsection
