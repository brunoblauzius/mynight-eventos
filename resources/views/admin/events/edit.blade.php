@extends('layouts.dashboard')

@section('css')
    <link href="{{asset('plugins/timepicker/bootstrap-timepicker.min.css')}}" rel="stylesheet">
    <link href="{{asset('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css')}}" rel="stylesheet">
@endsection

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Evento - {{$event->title}}.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.statistic', ['id' => $event->id])}}">{{$event->title}}.</a></li>
                    <li class="breadcrumb-item active">Editar.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row" style="margin-bottom:20px;">
        <div class="col-md-12 col-sm-12">
            <a href="{{route('admin.event.lists.vincular', $event->id)}}" class="btn btn-outline-info">1º Configurar Listas</a>
            <a href="{{route('admin.event.promoters', $event->id)}}" class="btn btn-outline-info">2º Configurar Promoters</a>
            <a href="{{route('admin.events.createCaixa', $event->id)}}" class="btn btn-outline-info">3º Configurar Caixas</a>
            <a href="{{route('admin.events.createBar', $event->id)}}" class="btn btn-outline-info">4º Adicionar ao Bar</a>
            <a href="{{route('admin.event.minha.lista', $event->id)}}" class="btn btn-outline-primary"> Gerenciar Ingressos.</a>
            <a href="{{route('admin.events.portaria', $event->id)}}" class="btn btn-outline-danger"> Portaria</a>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <form class="" action="{{ route('admin.events.update', ['id' => $event->id]) }}" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-8">
                        <div class="card-box">
                            <h4 class="m-t-0 m-b-20 header-title"><b>Editar evento.</b></h4>

                            <div class="form-group">
                                <label for="title">Categoria do evento:
                                    <el-tooltip content="Esta opção selecionada a categoria do evento isso facilita na gestão do evento como um todo no sistema." placement="top">
                                        <i class="fa fa-question-circle text-primary"></i>
                                    </el-tooltip></label>
                                <select name="events_categories_id" class="form-control">
                                    <option value=""> selecione a categoria </option>
                                    @foreach($categories as $category)
                                        @php $selected = ($event->events_categories_id==$category->id)?'selected':'';@endphp
                                        <option {{$selected}} value="{{$category->id}}"> {{$category->name}} </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="title">Titulo do evento:</label>
                                <input type="text" class="form-control" name="title" id="title"
                                       value="{{ $event->title }}">
                            </div>

                            <div class="form-group">
                                <label for="meta_description">Meta Descrição:</label>
                                <textarea name="meta_description" rows="4" cols="80" class="form-control"
                                          id="meta_description"
                                          maxlength="156">{{ $event->meta_description }}</textarea>
                            </div>

                            <div class="form-group">
                                <label for="description">Descrição:</label>
                                <textarea name="description" cols="80" class="form-control" style="height:200px;"
                                          id="description">{{ $event->description }}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="col-4">
                        <div class="card-box">
                            <h4 class="m-t-0 m-b-20 header-title"><b>Dados gerais do evento.</b></h4>
                            @if(!empty($event->image))
                                <img src="{{ asset('storage/'. $event->image) }}" class="img-thumbnail img-responsive" alt="">
                            @else
                                <img src="http://placehold.it/1980x600" class="img-thumbnail img-responsive" alt="">
                            @endif
                            <button type="button" class="btn btn-primary btn-block" data-toggle="modal"
                                    data-target="#changeEventCover">
                                Alterar capa do evento
                            </button>
                            <hr>

                            <div class="form-group">
                                <div class="checkbox">
                                    @php $checked = ($event->visible_website==1)? 'checked':''; @endphp
                                    <input type="hidden" class="" name="visible_website" id="" value="0">
                                    <input type="checkbox" {{$checked}} class="" name="visible_website" id="visible_website" value="1">
                                    <label for="visible_website">
                                        Deixar visível no site:
                                        <el-tooltip content="Esta opção selecionada deixa o evento visível no site, não é indicada para eventos privados ex. casamentos ou formaturas" placement="top">
                                            <i class="fa fa-question-circle text-primary"></i>
                                        </el-tooltip>
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Inicio do evento.</label>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input class="form-control" name="start_at" placeholder="dd/mm/yyyy" id="datepicker" type="text" value="{{ $event->exibe_data_ini }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input id="timepicker2" class="form-control timepicker2" name="start_hour" type="text" value="{{ $event->exibe_hora_ini }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-clock"></i></span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Fim do evento.</label>
                                <div class="row">
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input class="form-control" placeholder="dd/mm/yyyy"  name="end_at" id="datepicker2" type="text" value="{{ $event->exibe_data_fim }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-calendar"></i></span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                    <div class="col-6">
                                        <div class="input-group">
                                            <input id="timepicker2" class="form-control timepicker2" name="end_hour" type="text" value="{{ $event->exibe_hora_fim }}">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="mdi mdi-clock"></i></span>
                                            </div>
                                        </div><!-- input-group -->
                                    </div>
                                </div>
                            </div>

                            <label for="local">Local:</label>
                            <div class="form-group">
                                <input type="radio" id="local.place" name="local" value="1"
                                       @if($event->address == \Auth::user()->place->address) checked @endif>
                                <label for="local.place">{{ Auth::user()->place->name }}</label>
                            </div>

                            <div class="form-group">
                                <input type="radio" id="local.address" name="local" value="0"
                                       @if($event->address != \Auth::user()->place->address) checked @endif>
                                <label for="local.address">Outro</label>
                                <input type="text" name="address" placeholder="Endereço" value="{{ $event->address }}"
                                       class="form-control">
                            </div>

                            <div class="form-group">
                                <label for="">Galeria de imagens</label>
                                <select class="form-control" name="galery_id" id="galery_id">
                                    <option value="">Selecione a galeria de imagens</option>
                                    @foreach ($galeries as $galery)
                                        <option @if($event->galery_id == $galery->id) selected
                                                @endif value="{{ $galery->id }}">{{ $galery->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <button type="submit" name="button" class="btn btn-primary">Editar evento</button>
            </form>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="changeEventCover" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form class="" action="{{ route('admin.events.upload-cover', ['id' => $event->id]) }}" method="post"
                      enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Upload capa do evento</h4>
                    </div>
                    <div class="modal-body">


                        <input type="file" id="cover" name="cover" style="display: none;">
                        <label for="cover" class="css-upload-cover-box">
                            <div class="btn btn-custom">
                                <span>Selecione a imagem</span>
                            </div>
                        </label>

                        @if(!empty($event->image))
                            <img src="{{ asset('storage/'.  $event->image) }}" id="upload-cover-preview" class="img-thumbnail img-responsive" alt="">
                        @else
                            <img src="http://placehold.it/1980x600" id="upload-cover-preview" class="img-thumbnail img-responsive"
                                 alt="">
                        @endif
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">Enviar imagem</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


@section('js')

    <script src="{{ asset('plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('plugins/timepicker/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

    <script src="{{ asset('js/eventos.init.js') }}"></script>
    <script src="{{ asset('admin/plugins/tinymce/js/tinymce/tinymce.min.js') }}"></script>

    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#upload-cover-preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '#cover', function () {
            readURL(this);
        });

        $(document).ready(function () {

            $('#datepicker, #datepicker2').datepicker({
                format: "dd/mm/yyyy",
            });

            $('.timepicker2').timepicker({
                showMeridian: false,
                icons: {
                    up: 'mdi mdi-chevron-up',
                    down: 'mdi mdi-chevron-down'
                }
            });

            tinymce.init({
                selector: 'textarea#description',
                toolbar: [
                    'undo redo | styleselect | bold italic | link image | alignleft aligncenter alignright',
                ],
                menubar: false,
                branding: false
            });
        })
    </script>
@endsection


@endsection
