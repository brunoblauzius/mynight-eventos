@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.list')}}">Meus Eventos.</a></li>
                    <li class="breadcrumb-item active">Vincular listas.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">

            <div style="margin-bottom:15px;">
                <a class="btn btn-warning" href="{{route('admin.events.createBar',$event->id)}}"><i class="fa fa-mail-reply"></i> Voltar</a>
            </div>

            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Configuração do estoque para o evento - ( {{$event->title}} ).</b></h4>
                <form action="{{route('admin.event.bar.update.vinculate')}}" method="post" id="form-promoter">
                    <input name="id" type="hidden" value="{{$product->id}}">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <small>Selecione o produto:</small>
                                <select name="produtos_id" id="produtos_id" class="form-control" disabled="disabled">
                                    <option value="">-- selecione --</option>
                                    @foreach( $produtos as $produto )
                                        <option
                                                @php echo ($produto->id == $product->produtos_id)?'selected':null;@endphp
                                                value="{{$produto->id}}"> {{$produto->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-2">
                            <div class="form-group">
                                <small>Quantidade:</small>
                                <input
                                        class="form-control"
                                        name="amount"
                                        id="amount"
                                        type="number"
                                        value="{{$product->amount}}"
                                        placeholder="0">
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group">
                                <small>Valor(R$):</small>
                                <money
                                        class="form-control"
                                        name="value"
                                        id="value"
                                        type="text"
                                        value="{{$product->value}}"
                                        placeholder="0,00"></money>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="form-group" style="padding-top: 22px">
                                <button class="btn btn-primary btn-block">Salvar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        let produtos = JSON.parse('{!! json_encode($produtos->toArray()) !!}');

        $(document).ready(function () {
            $('#produtos_id').change(function(){
                let element = $(this);
                let achou   = false;
                $.each(produtos, function(item , value){
                    if(element.val() == value.id){
                        achou = true;
                        $('#value').val(value.valor);
                    }
                });
                if(achou == false){
                    $('#value').val(null);
                }
            });
        });
    </script>
@endsection