@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.list')}}">Meus Eventos.</a></li>
                    <li class="breadcrumb-item active">Vincular listas.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')

        <div class="row">
            <div class="col-12">
                <div style="margin-bottom:15px;">
                    <a class="btn btn-warning" href="{{route('admin.events.edit',$event->id)}}"><i class="fa fa-mail-reply"></i> Voltar</a>
                </div>
            </div>

            <div class="col-4">
                <div class="card-box">
                    <div class="panel-heading"> Inserir nome na lista </div>
                    <div class="panel-body">
                        <form action="{{route('admin.events.attach-person')}}" method="post" id="form-lista">
                            {{csrf_field()}}
                            <input name="event_id" id="event_id" type="hidden" value="{{$event->id}}">
                            <div class="col-12">
                                <div class="form-group">
                                    <small>Selecione o promoter:</small>
                                    <select name="user_id" id="promoter_id" data-href="{{route('admin.lists.listpromoter')}}" class="form-control">
                                        <option value="">-- selecione --</option>
                                        @foreach( $event->eventPromoters as $promoter )
                                            <option value="{{$promoter->id}}">{{$promoter->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <small>Selecione a Lista:</small>
                                    <select name="list_id" id="list_id" class="form-control">
                                        <option value="">-- selecione --</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group">
                                    <small>Adicionar nome e telefone:</small>
                                    <textarea class="form-control" name="name" placeholder="José da silva - 99.99999-9999"></textarea>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="form-group text-center">
                                    <button type="submit" class="btn btn-primary">Salvar</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col-8">
                <div class="card-box">
                    <h4 class="m-t-0 header-title"><b>Lista - {{ucfirst($event->title)}} .</b></h4>
                    @if( $event->minhaLista->count() > 0 )
                    <table class="table m-0 table-colored-bordered table-bordered-inverse">
                        <thead>
                            <th>Masculino: <span style="font-size:20px">{{$total['male']}}</span></th>
                            <th>Feminino: <span style="font-size:20px">{{$total['female']}}</span></th>
                            <th>Unisex: <span style="font-size:20px">{{$total['unisex']}}</span></th>
                        </thead>
                    </table>
                    <table class="table table-primary table-head">
                        <thead>
                            <th>Nome</th>
                            <th>Lista</th>
                            <th class="hidden-xs">Promoter</th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach( $event->minhaLista as $item)
                            <tr>
                                <th>{{$item->name}}</th>
                                <td>{{$item->lista->description}}</td>
                                <td class="text text-primary hidden-xs">{{ucwords($item->user->name)}}</td>
                                <td></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @else
                        <div class="alert alert-warning text-center">
                            Nenhum registro foi encontrado.
                        </div>
                    @endif
                </div>
            </div>
        </div>
@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('#promoter_id').change(function(event){
                let anchor = $(this);
                let data   = {
                    event_id: $('#event_id').val(),
                    user_id: anchor.val()
                };
                let url    = anchor.data('href');
                if(anchor.val() != null || anchor.val() != '')
                {
                    getJson(anchor, data, url)
                        .done(function (response) {
                            // console.log('success',response.event_promoters)
                            let html = '';
                            $.each(response.event_promoters, function (key, item) {
                                html += '<option value="' + item.id + '">' + item.description + '</option>';
                            })
                            $('#list_id').html(html);
                        })
                        .fail(function (response) {
                            console.log('fail', response)
                        });
                }
            });
        });
    </script>
@endsection