@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.list')}}">Meus Eventos.</a></li>
                    <li class="breadcrumb-item active">Vincular listas.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">

        <div class="col-12">
            <div style="margin-bottom:15px;">
                <a class="btn btn-warning" href="{{route('admin.events.edit',$event_id)}}"><i class="fa fa-mail-reply"></i> Voltar</a>
            </div>
            <div class="card-box">

                <h4 class="m-t-0 header-title"><b>Gestão de Ingressos.</b></h4>
                <p class="text-muted font-14 m-b-20">
                    deseja vincular listas para este evento:
                </p>
                <table class="table m-0 table-colored-bordered table-bordered-inverse">
                    <thead>
                    <th>#</th>
                    <th>Descrição</th>
                    <th>Tipo do Público</th>
                    <th>Quantdade</th>
                    <th>Valor (R$)</th>
                    <th width="100"></th>
                    </thead>
                    <tbody>
                    @foreach( $lists as $lista)
                        <tr>
                            <td>{{str_pad($lista->id, 3, '0', STR_PAD_LEFT)}}</td>
                            <td><span class="text text-primary">{{$lista->description}}</span></td>
                            <td>
                                @component('components.gender', ['gender' => $lista->gender]) @endcomponent
                            </td>
                            <td>
                                {{$lista->amount}} un.
                            </td>
                            <td>
                                @component('components.valor', ['valor' => $lista->value]) @endcomponent
                            </td>
                            <td>

                                @if(in_array($lista->id, $lista_vinculado))
                                    <button
                                            class="btn btn-warning btn-sm btn-block add-item"
                                            data-eventid="{{$event_id}}"
                                            data-listsid="{{$lista->id}}"
                                            data-vinculado="1"
                                    >Desvincular
                                    </button>
                                @else
                                    <button
                                            class="btn btn-primary btn-sm  btn-block add-item"
                                            data-eventid="{{$event_id}}"
                                            data-listsid="{{$lista->id}}"
                                            data-vinculado="0"
                                    >Vincular
                                    </button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

   @section('js')
       <script>
           $(document).ready(function () {
               $('.add-item').click(function (event) {
                   let event_id = $(this).data('eventid');
                   let lists_id = $(this).data('listsid');
                   let vinculado = $(this).data('vinculado');
                   $.ajax({
                       url: '{{route('admin.event.attach.lists')}}',
                       data: {
                           event_id: event_id,
                           lists_id: lists_id,
                           vinculado: vinculado
                       },
                       dataType: 'json',
                       method: 'post'
                   })
                       .done(function (response) {
                           alert(response);
                           window.location.reload();
                       })
                       .fail(function (response) {
                           alert(response.responseText);
                       });

               });
           })
       </script>
   @endsection
@endsection