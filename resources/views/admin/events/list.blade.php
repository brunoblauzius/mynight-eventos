@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Meus Eventos.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection


@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Eventos.</b></h4>
                <p class="text-muted font-14 m-b-20">
                    cadastro de usuários que terá acesso ao sistema
                    <a href="{{ route('admin.events.create') }}" class="btn btn-xs btn-primary btn-bordered pull-right"><i
                                class="fa fa-plus"></i> Incluir novo</a>
                </p>
            </div>
        </div>
    </div>

    <div class="row">
        @foreach($events as $event)
            <div class="col-md-4 col-sm-4 col-xs-12 flex-active">
                <a class="m-b-30" href="{{ route('admin.events.statistic', ['id' => $event->id]) }}">
                    <div class="card  card-inverse text-white">
                        <img class="card-img img-fluid" src="@if(!empty($event->image)) {{ asset('storage/'. $event->image) }} @else http://placehold.it/1980x600 @endif" alt="{{ $event->title }}">
                        <div class="card-img-overlay">
                            @if(empty($event->image))
                                <h3 class="card-title text-white">{{ $event->title }}</h3>
                                <p class="card-text">
                                    <small class="">Last updated 3 mins ago</small>
                                </p>
                            @endif
                        </div>
                    </div>
                    <h5 class="card-title">{{ $event->title }} <small class=""> - {{ $event->exibe_data_ini }}</small></h5>
                </a>
            </div>
        @endforeach
    </div>
    <nav class="m-t-10">
        {{$events->links('vendor.pagination.bootstrap-4')}}
    </nav>
@endsection
