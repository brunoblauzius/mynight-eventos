@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Galeria.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Galeria {{ $galery->title }}.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection


@section('content')
    <div class="row">
        <div class="col-12">
            <a href="{{ route('admin.galeries') }}" class="btn btn-warning m-b-20">Voltar</a>
        </div>
    </div>


    <div class="row">
        <div class="col-4">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Enviar imagens.</b></h4>
                <form class="" action="{{ route('admin.galery.addPhotos', ['galery_id' => $galery->id]) }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="photos">Fotos: <small>(max: 2mb por imagem)</small></label>
                        <input type="file" multiple name="photos[]" value="" id="photos">
                    </div>
                    <hr>
                    <button type="submit" name="button" class="btn btn-primary">Enviar imagens</button>
                </form>
            </div>
        </div>

        <div class="col-8">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Galeria {{ $galery->title }}.</b></h4>
                <div class="panel-body">
                    @foreach($galery->photos as $photo)
                        <div class="col-3">
                            <img src="{{ asset('storage/' . $photo->thumb) }}" alt="" class="img-responsive img-thumbnail" style="margin-bottom:15px;">
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection
