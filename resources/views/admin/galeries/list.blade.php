@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Galeria.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Galeria.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection


@section('content')

    <div class="row">
        <div class="col-4">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Criar galeria.</b></h4>
                <form class="" action="{{ route('admin.galery.store') }}" method="post" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="">Titulo:</label>
                        <input type="text" name="title" class="form-control" value="">
                    </div>

                    <div class="form-group">
                        <label for="photos">Fotos:
                            <small>(max: 2mb por imagem)</small>
                        </label>
                        <input type="file" multiple name="photos[]" value="" id="photos">
                    </div>
                    <hr>
                    <button type="submit" name="button" class="btn btn-primary">Criar galeria</button>
                </form>
            </div>
        </div>
        <div class="col-8">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Galerias de fotos.</b></h4>

                @forelse($galeries as $galery)
                    <div class="col-md-4 col-sm-4">
                        <div class="galery-box" style="margin-bottom:15px;">
                            <a href="{{ route('admin.galery', ['galery_id' => $galery->id]) }}">
                                <h5 class="galery-title">{{ $galery->title }}</h5>
                                @if (count($galery->photos) > 0)
                                    @foreach ($galery->photos as $photo)
                                        <img src="{{ asset('storage/' . $photo->thumb) }}"
                                             class="img-responsive img-rounded" alt="">
                                        @break
                                    @endforeach
                                @else
                                    <img src="{{ asset('images/albuns/1.jpg') }}" class="img-responsive img-rounded"
                                         alt="">
                                @endif

                            </a>
                        </div>

                    </div>
                @empty
                    <div class="alert alert-info text-center">Nenhuma galeria criada</div>
                @endforelse
                <nav class="m-t-10">
                    {{$galeries->links('vendor.pagination.bootstrap-4')}}
                </nav>
            </div>
        </div>
    </div>
@endsection
