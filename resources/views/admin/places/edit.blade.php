@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Configurações da empresa.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Minha Empresa.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    @endsection

@section('content')
    <div class="row">
        <div class="col-8">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Configurações.</b></h4>
                <p class="text-muted font-14 m-b-20">
                    Configure sua empresa, e local do estabelecimento
                </p>
                <div class="panel-body">
                    <form class="" action="{{ route('admin.place.update') }}" method="post">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="name">Nome</label>
                                    <input type="text" name="name" value="{{ $place->name }}" id="name" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label for="phone">Telefone</label>
                                    <input type="text" name="phone" value="{{ $place->phone }}" id="phone" class="form-control">
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="address">Endereço</label>
                                    <input type="text" name="address" value="{{ $place->address }}" id="address" class="form-control">
                                </div>
                            </div>

                            <div class="col-12" >
                                <div class="form-group "style="border-radius:5px; background-color:#fafafa; padding: 5px">
                                    <div class="col-sm-12">
                                        <span >Defina o horario de funcionamento da sua empresa.</span><br>
                                    </div>
                                    <div class="row">
                                        <div class="col-6">
                                            <label for="address">Horario de abertura:</label>
                                            <input type="time" name="hora_abertura" value="{{ $place->hora_abertura }}" id="address" class="form-control">
                                        </div>
                                        <div class="col-6">
                                            <label for="address">Horario de fechamento:</label>
                                            <input type="time" name="hora_fechamento" value="{{ $place->hora_fechamento }}" id="address" class="form-control">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="col-12"  style="margin-top: 20px; margin-bottom: 10px;">
                                        <span >Defina os dias de funcionamento da sua empresa.</span><br>
                                    </div>

                                    <div class="col-12">
                                        @component('components.checkbox', [
                                            'checked' => $place->dom,
                                            'name'  => 'dom',
                                            'tag_name' => 'Domingo'
                                        ]) @endcomponent
                                            @component('components.checkbox', [
                                                'checked' => $place->seg,
                                                'name'  => 'seg',
                                                'tag_name' => 'Segunda'
                                            ]) @endcomponent
                                            @component('components.checkbox', [
                                                'checked' => $place->ter,
                                                'name'  => 'ter',
                                                'tag_name' => 'Terça'
                                            ]) @endcomponent
                                            @component('components.checkbox', [
                                                'checked' => $place->qua,
                                                'name'  => 'qua',
                                                'tag_name' => 'Quarta'
                                            ]) @endcomponent
                                            @component('components.checkbox', [
                                                'checked' => $place->qui,
                                                'name'  => 'qui',
                                                'tag_name' => 'Quinta'
                                            ]) @endcomponent
                                            @component('components.checkbox', [
                                                'checked' => $place->sex,
                                                'name'  => 'sex',
                                                'tag_name' => 'Sexta'
                                            ]) @endcomponent
                                            @component('components.checkbox', [
                                                'checked' => $place->sab,
                                                'name'  => 'sab',
                                                'tag_name' => 'Sábado'
                                            ]) @endcomponent
                                    </div>

                                    <div class="clearfix"></div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="form-group">
                                    <label for="description">Descrição</label>
                                    <textarea name="description" rows="8" cols="80" id="description" class="form-control">{{ $place->description }}</textarea>
                                </div>
                            </div>
                        </div>
                        <button type="submit" name="button" class="btn btn-primary">Salvar</button>
                    </form>
                </div>
            </div>
        </div>

        <div class="col-4">
            <div class="card-box">
                <div class="panel panel-site">
                    <div class="panel-heading">
                        Logo {{ $place->name }}
                    </div>
                    <div class="panel-body">
                        <img src="{{ place_photo($place->photo) }}" class="img-responsive" alt="">
                        <hr>
                        <form action="{{ route('admin.place.upload-photo') }}" method="post" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <label for="place-photo">Alterar imagem</label>
                                <input type="file" name="photo" id="place-photo" value="">
                            </div>
                            <button type="submit" class="btn btn-block btn-primary">Enviar imagem</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
