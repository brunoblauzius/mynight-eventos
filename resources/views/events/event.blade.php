@extends('layouts.default')
@section('header-class')
    transperant
@endsection
@section('content')
    <!-- Profile Cover -->
    <section class="image-bg lis-grediant grediant-bt-dark text-white pb-4 profile-inner">
        <div class="background-image-maker"></div>
        <div class="holder-image"><img src="{{ cover_image($event->image) }}" alt="" class="img-fluid d-none"></div>
        <div class="container">
            <div class="row justify-content-center wow ">
                <div class="col-12 col-md-8 mb-4 mb-lg-0">
                    <a href="#" class="text-white">
                        <div class="media d-block d-md-flex text-md-left text-center"><img
                                    src="{{ place_photo($event->place->photo) }}" style="width: 150px;"
                                    class="img-fluid d-md-flex mr-4 border border-white lis-border-width-4 rounded mb-4 mb-md-0"
                                    alt=""/>
                            <div class="media-body align-self-center">
                                <h2 class="text-white font-weight-bold lis-line-height-1">{{ $event->title}}</h2>
                                <p class="mb-0">{{ $event->place->name }}</p>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-md-4 align-self-center">
                    <ul class="list-unstyled mb-0 lis-line-height-2 text-md-right text-center">
                        <li><i class="fa fa-phone pr-2"></i>{{ $event->place->phone }}</li>
                        <li>
                            <A href="#" class="text-white"><i class="fa fa-map-o pr-2"></i> {{ $event->address }}</A>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <!--End Profile Cover -->
    <!-- Profile header -->
    <!--<div class="profile-header">
        <div class="container">
            <div class="row">
                <div class="col-12 col-xl-6 order-xl-1 order-2 text-xl-right text-center">
                    <ul class="nav nav-pills flex-column flex-sm-row lis-font-poppins" id="myTab" role="tablist">
                        <li class="nav-item ml-0"> <a class="nav-link lis-light py-4 lis-relative mr-3 active" data-toggle="tab" href="#venue" role="tab" aria-controls="venue" aria-expanded="true"> Detalhes</a> </li>
                        <li class="nav-item ml-0"> <a class="nav-link lis-light py-4 lis-relative mr-3" data-toggle="tab" href="#artist" role="tab" aria-controls="artist"> Lista vip</a> </li>
                        <li class="nav-item ml-0"> <a class="nav-link lis-light py-4 lis-relative mr-3" data-toggle="tab" href="#ticket" role="tab" aria-controls="ticket">Ingresso online</a> </li>
                        <li class="nav-item ml-0"> <a class="nav-link lis-light py-4 lis-relative" data-toggle="tab" href="#tc" role="tab" aria-controls="tc"> Cardapio</a> </li>
                    </ul>
                </div>
                <div class="col-12 col-xl-6 align-self-center order-xl-2 order-1 text-xl-right text-center ">
                    <div class="cover-btn mt-4 mt-xl-0 lis-f-14">
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item py-2 mr-0"><a href="#" class="lis-light rounded-left"><i class="fa fa-envelope-o pr-1"></i> Add review</a> </li>
                            <li class="list-inline-item py-2 mr-0"><a href="#" class="lis-light"><i class="fa fa-heart-o pr-1"></i> Bookmark</a> </li>
                            <li class="list-inline-item py-2 mr-0"><a href="#" class="lis-light"><i class="fa fa-share pr-1"></i> Share</a> </li>
                            <li class="list-inline-item py-2 mr-0"><a href="#" class="lis-light rounded-right"><i class="fa fa-ellipsis-h"></i></a> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>-->
    <!-- End header -->
    <!-- Profile Content -->
    <section class="lis-bg-light pt-5">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-8 mb-5 mb-lg-0">
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="venue" role="tabpanel" aria-labelledby="venue">
                            <h6 class="lis-font-weight-500"><i class="fa fa-align-right pr-2 lis-f-14"></i> Descrição
                            </h6>
                            <div class="card lis-brd-light mb-4 wow">
                                <div class="card-body p-4">
                                    {!! $event->description !!}
                                </div>
                            </div>

                            @if (!empty($event->lat) && !empty($event->long))
                                <h6 class="lis-font-weight-500"><i class="fa fa-map-o pr-2 lis-f-14"></i> Mapa</h6>
                                <div class="card lis-brd-light mb-4 wow">
                                    <div class="card-body p-4">
                                        <div id="map" style="height: 400px;"></div>
                                        <script>
                                            var map;

                                            function initMap() {
                                                var myLatLng = {lat: {{ $event->lat }}, lng: {{$event->long}}};
                                                map = new google.maps.Map(document.getElementById('map'), {
                                                    center: myLatLng,
                                                    zoom: 16
                                                });

                                                var marker = new google.maps.Marker({
                                                    position: myLatLng,
                                                    map: map,
                                                    title: '{{ $event->place->name }} - {{ $event->title }}'
                                                });
                                            }
                                        </script>

                                    </div>
                                </div>
                            @endif

                            @if ($event->galery)
                                <h6 class="lis-font-weight-500"><i class="fa fa-photo pr-2 lis-f-14"></i> Galeria de
                                    fotos:</h6>
                                <div class="card lis-brd-light wow">
                                    <div class="card-body py-4 px-0">
                                        <div class="center3 slider">
                                            @foreach ($event->galery->photos as $photo)
                                                <div>
                                                    <div class="gallery text-center lis-relative"><img
                                                                src="{{ asset('storage/' . $photo->thumb) }}" alt=""
                                                                class="img-fluid rounded"/>
                                                        <div class="gallery-fade fade w-100 h-100 rounded">
                                                            <div class="d-table w-100 h-100">
                                                                <div class="d-table-cell align-middle">
                                                                    <div class="h4">
                                                                        <a data-toggle="lightbox"
                                                                           data-gallery="example-gallery"
                                                                           href="{{ url('storage/' . $photo->file) }}"
                                                                           class="text-white"> <i
                                                                                    class="fa fa-search-plus"></i></a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <div class="tab-pane fade" id="ticket" role="tabpanel" aria-labelledby="ticket">
                            <div class="card lis-brd-light wow ">
                                <div class="card-body p-4">
                                    <div class="media d-block d-md-flex text-left">
                                        <div class="d-md-flex mr-0 mr-md-5">
                                            <p class="lis-primary mb-0">Nov. 13, 2017 <span class="lis-light d-block">10:00AM</span>
                                            </p>
                                        </div>
                                        <div class="media-body align-self-center mt-sm-0 mt-3">
                                            <div class="float-none float-sm-right"><a href="#"
                                                                                      class="btn btn-primary btn-default mb-1">
                                                    Book Now</a>
                                                <p class="mb-0 mt-sm-0 mt-3">Entry Pass : <span
                                                            class="lis-font-weight-500 lis-dark lis-font-poppins lis-font-weight-600">$125</span>
                                                </p>
                                            </div>
                                            <p class="lis-dark mb-0 mt-2 mt-md-0">First Day :</p>
                                            <h6 class="lis-font-weight-500 mb-2">Welcome Speech</h6>
                                            <p class="mb-0">Primary Hall, First Street, New York, USA</p>
                                        </div>
                                    </div>
                                    <div class="lis-devider my-4"></div>
                                    <div class="media d-block d-md-flex text-left">
                                        <div class="d-md-flex mr-0 mr-md-5">
                                            <p class="lis-primary mb-0">Nov. 14, 2017 <span class="lis-light d-block"> 10:00AM</span>
                                            </p>
                                        </div>
                                        <div class="media-body align-self-center mt-sm-0 mt-3">
                                            <div class="float-none float-sm-right"><a href="#"
                                                                                      class="btn btn-primary btn-default mb-1">
                                                    Book Now</a>
                                                <p class="mb-0 mt-sm-0 mt-3">Entry Pass : <span
                                                            class="lis-font-weight-500 lis-dark lis-font-poppins lis-font-weight-600">$90</span>
                                                </p>
                                            </div>
                                            <p class="lis-dark mb-0 mt-2 mt-md-0">Second Day :</p>
                                            <h6 class="lis-font-weight-500 mb-2">DJ Sam Music Concert</h6>
                                            <p class="mb-0">Secondary Hall, First Street, New York, USA</p>
                                        </div>
                                    </div>
                                    <div class="lis-devider my-4"></div>
                                    <div class="media d-block d-md-flex text-left">
                                        <div class="d-md-flex mr-0 mr-md-5">
                                            <p class="lis-primary mb-0">Nov. 15, 2017 <span class="lis-light d-block">11:00AM</span>
                                            </p>
                                        </div>
                                        <div class="media-body align-self-center mt-sm-0 mt-3">
                                            <div class="float-none float-sm-right"><a href="#"
                                                                                      class="btn btn-primary btn-default mb-1">
                                                    Book Now</a>
                                                <p class="mb-0 mt-sm-0 mt-3">Entry Pass : <span
                                                            class="lis-font-weight-500 lis-dark lis-font-poppins lis-font-weight-600">$140</span>
                                                </p>
                                            </div>
                                            <p class="lis-dark mb-0 mt-2 mt-md-0">Third Day :</p>
                                            <h6 class="lis-font-weight-500 mb-2">Best Party Songs Making</h6>
                                            <p class="mb-0">Central Hall, First Street, New York, USA</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <h6 class="lis-font-weight-500"><i class="fa fa-calendar-check-o pr-2 lis-f-14"></i> Começa em</h6>
                    <div class="card bg-success mb-4 wow ">
                        <div class="card-body py-4">
                            <div class="soon event-countdown"
                                 data-due="{{ $event->start_at }}"
                                 data-layout="group overlap"
                                 data-face="slot doctor glow"
                                 data-padding="false"
                                 data-scale-max="l"
                                 data-visual="ring color-light width-thin glow-progress length-70 gap-0 offset-65"
                                 data-labels-days="Dia,Dias"
                                 data-labels-hours="Hora,Horas"
                                 data-labels-minutes="Minuto,Minutos"
                                 data-labels-seconds="Segundo,Segundos"></div>
                        </div>
                    </div>


                    <h6 class="lis-font-weight-500"><i class="fa fa-map pr-2 lis-f-14"></i> Local</h6>
                    <div class="card lis-brd-light mb-4 wow ">
                        <div class="card-body p-4">
                            <div class="media d-md-flex">
                                <a href="#"><img src="{{ place_photo($event->place->photo) }}"
                                                 class="img-fluid d-flex mr-4 rounded-circle" alt="" width="70"/></a>
                                <div class='media-body align-self-center'>
                                    <h6 class="mb-0"><a href="{{ route('place', ['slug' => $event->place->slug]) }}"
                                                        class="lis-dark">{{ $event->place->name }}</a></h6>
                                    <p class='mb-0'><i class="fa fa-phone pr-2"></i> {{ $event->place->phone }}</p>
                                </div>
                            </div>
                            <ul class="list-unstyled my-4 lis-line-height-2">
                                {{--<li><i class="fa fa-phone pr-2"></i> {{ $event->place->phone }}</li>--}}
                                <li class="font-13"><i class="fa fa-map-o pr-2"></i> {{ $event->place->address }}</li>
                            </ul>
                            <ul class="list-inline my-0">
                                <li class="list-inline-item mr-0"><a href="#"
                                                                     class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i
                                                class="fa fa-facebook"></i></a></li>
                                <li class="list-inline-item mr-0"><a href="#"
                                                                     class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i
                                                class="fa fa-twitter"></i></a></li>
                                <li class="list-inline-item mr-0"><a href="#"
                                                                     class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i
                                                class="fa fa-linkedin"></i></a></li>
                                <li class="list-inline-item mr-0"><a href="#"
                                                                     class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i
                                                class="fa fa-tumblr"></i></a></li>
                                <li class="list-inline-item mr-0"><a href="#"
                                                                     class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i
                                                class="fa fa-pinterest-p"></i></a></li>
                            </ul>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- End Profile Content -->
@endsection
