@extends('layouts.default')
@section('content')
<section>
    <div class="container pt-5 mt-5">
        <div class="row justify-content-center">
            <div class="col-12 col-md-10 text-center">
                <div class="heading pb-4">
                    <h5 class="lis-light">Encontre os melhores lugares</h5>
                    <h2 class="f-weight-500">Liste por categoria os estabelecimentos cadastrados no portal.</h2> </div>
            </div>
        </div>
        <div class="row">
            @foreach( $categories as $category )
            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide3.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg4 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-radio-mic"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">{{$category->name}}</h6> <span class="lis-font-roboto">13 Encontrados</span> </div>
                </div>
            </div>
            @endforeach

        </div>

        <div class="row">
            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide1.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg4 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-fast-food"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Sertanejo</h6> <span class="lis-font-roboto">13 listing</span> </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide1.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg4 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-fast-food"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Funk</h6> <span class="lis-font-roboto">13 listing</span> </div>
                </div>
            </div>

            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide1.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg4 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-fast-food"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">POP</h6> <span class="lis-font-roboto">13 listing</span> </div>
                </div>
            </div>




            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide5.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg6 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-medical-sign-alt"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Medical</h6> <span class="lis-font-roboto">18 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide1.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg4 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-fast-food"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Restaurant</h6> <span class="lis-font-roboto">13 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide2.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg2 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-beer"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Bar & Club</h6> <span class="lis-font-roboto">15 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/bg1.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg5 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-travelling"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Tour & Travels</h6> <span class="lis-font-roboto">10 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide3.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg3 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-radio-mic"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Entertainment</h6> <span class="lis-font-roboto">18 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide4.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg1 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-hotel-alt"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Hotel</h6> <span class="lis-font-roboto">13 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide6.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg7 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-home"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Property</h6> <span class="lis-font-roboto">18 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide7.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg8 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-volleyball"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Sports</h6> <span class="lis-font-roboto">13 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 mb-xl-0 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide8.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg9 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-truck-loaded"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Transport</h6> <span class="lis-font-roboto">18 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 mb-sm-0 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide9.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg10 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-industries"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Industries</h6> <span class="lis-font-roboto">13 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 mb-4 mb-sm-0 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide10.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg11 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-monitor"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Property</h6> <span class="lis-font-roboto">18 listing</span> </div>
                </div>
            </div>
            <div class="col-12 col-sm-6 col-xl-3 wow fadeInUp">
                <div class="card lis-brd-light text-center text-lg-left lis-info lis-relative">
                    <a href="#">
                        <div class="lis-grediant grediant-tb-light lis-relative modImage rounded"> <img src="dist/images/slide11.jpg" alt="" class="img-fluid rounded" /> </div>
                        <div class="lis-absolute lis-left-20 lis-top-20 lis-bg12 lis-icon lis-rounded-circle-50 text-center">
                            <div class="text-white mb-0 lis-line-height-2_5 h4"><i class="icofont icofont-book-alt"></i></div>
                        </div>
                    </a>
                    <div class="hover-text lis-absolute lis-left-20 lis-bottom-20 lis-font-roboto text-white text-left">
                        <h6 class="text-white mb-0">Education</h6> <span class="lis-font-roboto">13 listing</span> </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
