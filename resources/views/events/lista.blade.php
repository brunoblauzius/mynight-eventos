@extends('layouts.default')
@section('content')
    <!-- Sidebar -->
    <section class="">
        <div class="container">
            <div class="row pt-5">
                <div class="col-12 col-md-12">
                    <div class="lis-relative">
                        <form class="" method="get">
                            <h5 class="mb-2">O que você está procurando?</h5>
                            <p>Procure aqui</p>
                            <div class="row">
                                <div class="col-12 col-sm-10 col-lg-6">
                                    <div class="form-group lis-relative">
                                        <input type="text" name="search" value="{{ $search }}"
                                               class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4"
                                               placeholder="O que você está procurando?">
                                        <div class="lis-search"><i class="fa fa-search text-success lis-left-0"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-2">
                                    <button type="submit" class="btn btn-success btn-default"><i
                                                class="fa fa-search pr-2"></i> Procurar evento
                                    </button>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-sm-6 align-self-center">
                                    <p>{{ $events->total() }} Eventos encontrados</p>
                                </div>
                            </div>
                        </form>
                        <div class="row">
                            @include('components.event-list', ['events' => $events])

                        </div>
                        <div class="row mt-5">
                            <div class="col-md-12">
                                {{ $events->links('vendor.pagination.site') }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Sidebar -->
@endsection
