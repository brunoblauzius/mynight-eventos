@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Produtos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Produtos.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">

        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Cadastrar Produtos.</b></h4>


                <div class="">
                    <form action="{{route('admin.produto.update', $produto->id)}}" method="post" name="cadastrar" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-6">
                                <div class="col-12">
                                    <div class="form-group ">
                                        <small>Descrição do Produto:</small>
                                        <input type="text" name="name" id="name" class="form-control" placeholder="Nome do produto:" value="{{$produto->name}}">
                                        <input type="hidden" name="id" id="id" value="{{$produto->id}}">
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group ">
                                        <small>Grupo do Produto:</small>
                                        <select name="produto_grupos_id" id="produto_grupos_id" class="form-control">
                                            @foreach($grupoProdutos as $grupo)
                                                @php $selected = ($produto->produto_grupos_id == $grupo->id)?'selected':null; @endphp
                                                <option value="{{$grupo->id}}" {{$selected}}>{{$grupo->descricao}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="form-group ">
                                        <small>Valor Padrão(R$):</small>
                                        <money name="valor" id="valor" class="form-control" placeholder="Valor padrão do produto (R$):" value="{{empty(old('valor'))?$produto->valor:old('valor')}}"></money>
                                    </div>
                                </div>

                                <div class="col-12">
                                    <div class="mt-6">
                                        <small class="m-t-0"><b>Status do produto.</b></small>
                                        <div class="custom-control custom-radio">
                                            @php $ativo = ($produto->status==1)?'checked':null; @endphp
                                            <input name="status" type="radio" id="ativo" class="custom-control-input" {{$ativo}} value="1">
                                            <label class="custom-control-label" for="ativo">Ativo</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            @php $inativo = ($produto->status==0)?'checked':null; @endphp
                                            <input name="status" type="radio" id="inativo" {{$inativo}} class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="inativo">Inativo</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group text-center">
                                        <button class="btn btn-primary">salvar</button>
                                    </div>
                                </div>
                            </div>

                            <div class="col-6">
                                <div class="row">

                                    <input type="file" id="cover" name="cover" style="display: none;">
                                    <label for="cover" class="css-upload-cover-box">
                                        <div class="btn btn-custom">
                                            <span>Selecione a imagem</span>
                                        </div>
                                    </label>
                                    @php $cover = (empty($produto->image))? "http://placehold.it/200X200" :asset('/storage/'.$produto->photo_path); @endphp
                                    <img src="{{$cover}}" id="upload-cover-preview" class="img-thumbnail img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
    <!-- end row -->
@endsection


@section('js')
    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#upload-cover-preview').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        $(document).on('change', '#cover', function () {
            readURL(this);
        });
    </script>
@endsection