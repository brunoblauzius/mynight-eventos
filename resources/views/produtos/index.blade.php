@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Produtos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Produtos.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Produtos.</b></h4>
                <p class="text-muted font-14 m-b-20">
                    Cadastre os produtos que irá comercializar.
                    <a href="{{route('admin.produto.create')}}" class="btn btn-xs btn-primary btn-bordered pull-right" id="btn-add-new"><i class="fa fa-plus"></i> Incluir novo</a>
                </p>

                <div class="table-responsive">
                    @if($produtos->count()>0)
                        <table class="table m-0 table-colored-bordered table-bordered-inverse">
                            <thead>
                            <tr>
                                <th >#</th>
                                <th width="50%">Nome</th>
                                <th>Grupo</th>
                                <th>Valor(R$)</th>
                                <th>Status</th>
                                <th>Criado em</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($produtos as $produto)
                                <tr>
                                    <td><img src="{{$produto->thumb_path}}" class="thumb-md"></td>
                                    <td>{{$produto->name}}</td>
                                    <td>{{$produto->grupo->descricao}}</td>
                                    <td>{{$produto->valor}}</td>
                                    <td>
                                        @if($produto->status == 1)
                                            <span class="label label-success"><i class="fa fa-check-circle"></i> Ativo</span>
                                        @else
                                            <span class="label label-danger"><i class="fa fa-times-circle"></i> Inativo</span>
                                        @endif
                                    </td>
                                    <td>{{$produto->criado_em}}</td>
                                    <td>
                                        <div class="btn-group-xs">
                                            <a href="{{route('admin.produtos.edit', $produto->id)}}" class="btn btn-bordered btn-info"><i class="fa fa-pencil"></i></a>
                                            <a href="{{route('admin.produtos.destroy', $produto->id)}}" class="btn btn-bordered btn-danger delete-list"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <nav class="m-t-10">
                            {{$produtos->links('vendor.pagination.bootstrap-4')}}
                        </nav>
                    @else
                        <div class="alert alert-info text-center">
                            <span>Nenhum produto cadastrado até o momento.</span>
                        </div>
                    @endif
                </div>
            </div>
        </div>

    </div>
    <!-- end row -->


@endsection


@section('js')
    <script>

        function destroy(element, href){
            if(confirm('Deseja realmente excluir esse item?')){
                return $.ajax({
                    url: href,
                    data:{},
                    method: 'delete',
                    dataType: 'json'
                });
            }
        }

        function editJson(element, href){
            return $.ajax({
                url: href,
                data:{},
                method: 'get',
                dataType: 'json'
            });
        }


        $(document).ready(function(){

            $(document).on('click', '.delete-list', function(event){
                event.preventDefault();
                let element = $(this);
                let href = element.prop('href');
                destroy(element, href).done(function(response){
                    alert(response);
                    document.location.reload();
                }).fail(function(response){
                    alert(response.responseText);
                });
            });


            $(document).on('click', '.edit-list', function(event){
                event.preventDefault();
                let element = $(this);
                let href = element.prop('href');
                editJson(element, href).done(function(response){
                    let form = $('#form-cadastro-grupo-edit');
                    form.find('input[name="descricao"]').val(response.descricao);
                    form.find('input[name="id"]').val(response.id);
                    if(response.status == 1){
                        form.find('input[name="id"]').prop(response.id);
                    }
                    $('#myModalEdit').modal('show');
                }).fail(function(response){
                    alert(response.responseText);
                });
            });

        });
    </script>
@endsection