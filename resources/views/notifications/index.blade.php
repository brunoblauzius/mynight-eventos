@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Notificações.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="#">Minhas Notificações.</a></li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <section class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Minhas Notificações.</b></h4>

                @foreach($lista as $item)
                    <div class="row mb-3">
                        <div class="col-sm-1" >
                            <div class="bg-danger text-white mt-3 ml-3" style="font-size:18px; width: 50px; height: 50px; padding: 12px 20px; border-radius: 50px">
                                <i class="{{$item->icon}}"></i>
                            </div>
                        </div>
                        <div class="col-sm-10">
                            <h4 class="text text-primary">{{$item->title}}</h4>
                            <small class="text text-dark">{{$item->description}}</small>
                            <small class="text text-dark pull-right">Data criação: {{$item->criado_em}}</small>
                        </div>
                    </div>
                @endforeach

                <div class="mt-5">
                    {{$lista->links('vendor.pagination.bootstrap-4')}}
                </div>
            </div>
        </div>
    </section>

@endsection