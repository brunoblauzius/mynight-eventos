@extends('layouts.no-dash')

@section('content')
    <div class="account-pages">
        <div class="account-box">
            <div class="account-logo-box">
                <h5 class="text-uppercase font-bold m-b-5 m-t-20">Reset Password</h5>
                <p class="m-b-0">Digite o email cadastrado para recuperar sua senha.</p>
            </div>

            <div class="account-content">

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="control-label">Seu E-Mail:</label>
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                @if ($errors->has('email'))
                                    <span class="text text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="col-12 ">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Enviar link para recuperar senha.
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>


@endsection
