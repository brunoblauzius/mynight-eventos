@extends('layouts.no-dash')

@section('content')
    <div class="account-pages">
        <div class="account-box">
            <div class="account-logo-box">
                <h2 class="text-uppercase text-center">
                    <a href="{{url('/')}}" class="text-success">
                        <span><img src="{{asset('dashboard/images/logo-payticket-b.png')}}" alt="{{ env('APP_NAME') }}" height="50" ></span>
                    </a>
                </h2>
                <h5 class="text-uppercase font-bold m-b-5 m-t-50">Login</h5>
                <p class="m-b-0">Para entrar no sistema digite seu login e senha.</p>
            </div>
            <div class="account-content">
                <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} m-b-20 row">
                        <div class="col-12">
                            <input id="email" type="text" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Digite seu usuário:">

                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong class="text text-danger">{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row m-b-20">
                        <div class="col-12">
                            <a href="{{ route('password.request') }}" class="text-muted pull-right"><small>Esqueceu sua senha?</small></a>
                            <input id="password" type="password" class="form-control" name="password" required placeholder="Digite sua senha:">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong class="text text-danger">{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row m-b-20">
                        <div class="col-12">

                            <div class="checkbox checkbox-success">
                                <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                <label for="remember">
                                    Lembrar minha senha.
                                </label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group row text-center m-t-10">
                        <div class="col-12">
                            <button class="btn btn-md btn-block btn-primary waves-effect waves-light" type="submit">Entrar no sistema</button>
                        </div>
                    </div>

                </form>

                <div class="row">
                    <div class="col-12">
                        <div class="text-center">
                            <button title="Facebook" type="button" class="btn m-r-5 btn-facebook waves-effect waves-light">
                                <i class="fa fa-facebook"></i>
                            </button>
                        </div>
                    </div>
                </div>

                <div class="row m-t-50">
                    <div class="col-sm-12 text-center">
                        <p class="text-muted">Você não tem uma conta? <a href="{{url('/para-empresas')}}" class="text-dark m-l-5"><b>Cadastrar-se</b></a></p>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- end card-box-->
@endsection
