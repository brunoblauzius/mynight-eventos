<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    Olá <strong>{{$user->name}}</strong>
    <p>
        Obrigado por se cadastrar na payticket e acreditar em nosso trabalho, estamos ansiosos para que comece a utilizar o sistema e saber como vai ser sua experiencia gerenciando seus eventos conosco.<br>
        A e claro você deve clicar no link para que prossiga o seu cadastro.
    </p>
    <br>
    <br>
    <a href="{{route('active.companies', $user->remember_token)}}" style="background-color:#2196F3; text-decoration: none; color: #fff; font-size: 16px; padding: 10px; border-radius: 5px;">ATIVAR MINHA CONTA</a>

</body>
</html>