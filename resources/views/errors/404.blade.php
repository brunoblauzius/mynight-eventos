@extends('layouts.no-dashboard')

@section('content')
    <div class="max-w-sm m-8">

        <div class="text-black text-5xl md:text-15xl font-black">
            404
        </div>

        <div class="w-16 h-1 bg-purple-light my-3 md:my-6"></div>

        <p class="text-grey-darker text-2xl md:text-3xl font-light mb-8 leading-normal">
            A página que você está procurando não foi encontrada. </p>

        <a class="btn btn-md btn-block btn-primary waves-effect waves-light m-t-20" href="{{url('/')}}"> Voltar a página
            inicial</a>
    </div>
@endsection