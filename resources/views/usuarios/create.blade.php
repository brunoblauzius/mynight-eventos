@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Usuários</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Usuários</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- end row -->
@endsection

@section('content')

    <div class="row">
        <div class="col-9">
            <div class="card-box">
                <h4 class="m-t-0 m-b-30 header-title">Formulário.</h4>

                @php
                    $route = (empty($user->id))?route('usuario.store'):route('usuario.update', $user->id);
                @endphp
                <form action="{{$route}}" method="post" id="usuario-store" role="form">
                    {{csrf_field()}}
                    @if($user->id > 0)
                        <input type="hidden" name="id" value="{{$user->id}}">
                    @endif
                    <div class="row">
                        <div class="col-8">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nome.</label>
                                <input class="form-control" name="name" aria-describedby="nameHelp" placeholder="Nome:" type="text" value="{{$user->name}}">
                                <small id="nameHelp" class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Status do usuário.</label>
                                <div class="checkbox">
                                        @php
                                            $checked = '';
                                            if($user->id <= 0){
                                                $checked = 'checked';
                                            }
                                            else if($user->status == 1 && $user->id>0){
                                                $checked = 'checked';
                                            }
                                        @endphp
                                    <input type="hidden" value="0" name="status">
                                    <input id="checkbox0" type="checkbox" {{$checked}} name="status" value="1">
                                    <label for="checkbox0">
                                        Ativo
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tipo de usuário</label>
                                <select class="form-control" name="roles_id">
                                    @foreach($roles as $role)
                                        @php
                                            $selected = '';
                                            if($user->role != null && $user->role->id == $role->id){
                                                $selected = 'selected';
                                            }
                                        @endphp
                                        <option value="{{$role->id}}" {{$selected}}> {{$role->name}} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">E-mail</label>
                                <input class="form-control" id="exampleInputEmail1" name="email" aria-describedby="emailHelp" placeholder="E-mail" type="email" value="{{$user->email}}">
                                <small id="emailHelp" class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Telefone</label>
                                <input class="form-control" name="telefone" aria-describedby="telefoneHelp" placeholder="Telefone" value="{{$user->telefone}}">
                                <small id="telefoneHelp" class="form-text text-muted"></small>
                            </div>
                        </div>
                    </div>


                    <h4>Dados de acessos.<hr></h4>

                    <div class="row">
                        <div class="col-10">
                            <div class="form-group">
                                <label for="exampleInputPassword2">Login</label>
                                <input class="form-control" name="login" id="login" aria-describedby="loginHelp" placeholder="Login:" type="text" value="{{$user->login}}">
                                <small id="loginHelp" class="form-text text-muted"></small>
                            </div>
                        </div>
                        <div class="col-2" style="padding-top: 36px;" id="login-livre">

                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="exampleInputPassword1">Senha</label>
                                <input class="form-control" id="exampleInputPassword1"  placeholder="Password" name="password" type="password">
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="exampleInputPassword3">Repetir a senha</label>
                                <input class="form-control" id="exampleInputPassword3"  placeholder="Password" name="confirm_password" type="password">
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Salvar</button>
                </form>
            </div>
        </div>

        @if($user->id > 0)
            <div class="col-3">
                <div class="card-box">
                    <img src="{{asset('dashboard/images/user-thumb-placeholder.jpg')}}" class="img-thumbnail img-responsive m-b-20">
                    <input type="file" name="thumb" class="m-b-20">
                    <button class="btn btn-primary btn-block"> Salvar </button>
                </div>
            </div>
        @endif

    </div>

@endsection


@section('js')
<script>
$(document).ready(function () {
   $('#login').focusout(function (event) {
       var that = $(this);
       $('#login-livre').empty();
       $('#loginHelp').empty();
       if(that.val() != '') {
           $.ajax({
               url: window.web_app + '/api/usuarios/check-is-user',
               data: {
                   login: that.val(),
               },
               method: 'get',
               dataType: 'json'
           }).done(function (response) {
               $('#login-livre').html('<span class="label label-success"><i class="fa fa-check"></i> Login Livre</span>');
           }).fail(function (response) {
               $('#login-livre').html('<span class="label label-danger"><i class="fa fa-check"></i> Login já esta em uso.</span>');
               $('#loginHelp').removeClass('text-muted').addClass('text-danger').text('Este login esta em uso, por favor tente outra combinação.');
               that.val(null);
           });
       }
   })
});
</script>
@endsection