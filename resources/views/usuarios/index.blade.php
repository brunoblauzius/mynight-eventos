@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Usuários</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Usuários</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Usuários do sistema.</b></h4>
                <p class="text-muted font-14 m-b-20">
                    cadastro de usuários que terá acesso ao sistema
                    <a href="{{route('usuarios.create')}}" class="btn btn-xs btn-primary btn-bordered pull-right"><i class="fa fa-plus"></i> Incluir novo</a>
                </p>

                <div class="table-responsive">
                    <table class="table m-0 table-colored-bordered table-bordered-inverse">
                        <thead>
                            <tr>
                                <th>Nome</th>
                                <th>E-mail</th>
                                <th>Telefone</th>
                                <th>Tipo</th>
                                <th>Criado em</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($usuarios as $usuario)
                                <tr>
                                    <th scope="row">{{$usuario->name}}</th>
                                    <td>{{$usuario->email}}</td>
                                    <td>{{$usuario->telefone}}</td>
                                    <td><span class="label label-info">{{$usuario->role->name}}</span></td>
                                    <td>{{$usuario->criado_em}}</td>
                                    <td>
                                        <div class="btn-group-xs">
                                            <a href="{{route('usuario.edit',$usuario->id)}}" class="btn btn-xs btn-info btn-bordered waves-effect waves-light"><i class="fa fa-pencil"></i></a>
                                            <a href="{{route('usuario.destroy',$usuario->id)}}" class="btn btn-xs btn-danger btn-bordered waves-effect waves-light"><i class="fa fa-times"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- end row -->

@endsection
