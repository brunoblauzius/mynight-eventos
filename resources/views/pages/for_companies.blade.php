@extends('layouts.default')
@section('description')
    A Payticket é um dos mais completos sistemas criado para gestão de eventos, focado na gestão de ingreços, despesas, venda do bar e gerenciamento de pessoas.
@endsection
@section('content')
    <section class="lis-bg-light pb-5" style="background: url('{{asset('dashboard/images/background/for_companies.jpg')}}')">
        <div class="container pt-5">
            <div class="row wow ">
                <div class="col-12 col-sm-6">
                    <div class="page-title">
                        <h2>Para Empresas</h2>
                        <p class="mb-0 text-dark">Organize seu evento e venda ingressos em nossa nova plataforma.</p>
                    </div>
                </div>
                <div class="col-12 col-sm-6 text-left text-sm-right">
                    <ol class="breadcrumb mb-0 pl-0 bg-transparent">
                        <li class="breadcrumb-item d-inline-block float-none"><a href="{{ route('home') }}" class="lis-light">Inicio</a></li>
                        <li class="breadcrumb-item d-inline-block float-none active">Para Empresas</li>
                    </ol>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="row">
            <div class="container">
                <div class="col-12 col-lg-12 mb-5 mb-lg-0">

                    <div class="heading text-center" style="margin-bottom: 80px">
                        <h2 class="text text-primary">O que a Payticket tem a oferecer para seu evento!</h2>
                    </div>

                    <div class="row">
                        <div class="col-4 mb-5">
                            <div class="text-center mb-4">
                                <i class="fa fa-bar-chart text-primary" style="font-size: 40px;"></i>
                            </div>
                            <div class="text-center">
                                <h3 style="font-size: 20px;">Relatórios em tempo real</h3>
                                Acompanhe o desempenho do(s) seu(s) evento(s) através de gráficos
                            </div>
                        </div>

                        <div class="col-4 mb-5">
                            <div class="text-center mb-4">
                                <i class="fa fa-mobile-phone text-primary" style="font-size: 40px;"></i>
                            </div>
                            <div class="text-center">
                                <h3 style="font-size: 20px;">App Organizador</h3>
                                Controle seu evento de onde estiver e realize uma gestão com maior mobilidade
                            </div>
                        </div>

                        <div class="col-4 mb-5">
                            <div class="text-center mb-4">
                                <i class="fa fa-users text-primary" style="font-size: 40px;"></i>
                            </div>
                            <div class="text-center">
                                <h3 style="font-size: 20px;">Gestão</h3>
                                Administre seus eventos dando acesso individual à área específica para cada colaborador
                            </div>
                        </div>

                        <div class="col-4 mb-5">
                            <div class="text-center mb-4">
                                <i class="fa fa-credit-card text-primary" style="font-size: 40px;"></i>
                            </div>
                            <div class="text-center">
                                <h3 style="font-size: 20px;">Vendas de ingressos</h3>
                                Publique seu evento facilmente em alguns passos e venda os ingressos pelo site e aplicativo
                            </div>
                        </div>

                        <div class="col-4 mb-5">
                            <div class="text-center mb-4">
                                <i class="fa fa-envelope text-primary" style="font-size: 40px;"></i>
                            </div>
                            <div class="text-center">
                                <h3 style="font-size: 20px;">Seu Público</h3>
                                Colete informações de seus participantes e tenha um mailing exclusivo e pessoal
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row bg-light pt-3 pb-5">
            <div class="container">
                <div class="heading text-center" style="margin-bottom: 80px; margin-top: 30px;">
                    <h2 class="text text-primary">Quanto Custa?</h2>
                </div>

                <div class="row">
                    <div class="col-4 text-center">
                        <div class="card p-3">
                            <h4>Eventos Gratuitos</h4>
                            <span class="text text-primary mb-3 mt-4" style="font-size: 60px">
                                R$ 0
                            </span>
                            <p class="mt-4">
                                Isso mesmo. Se o seu evento é grátis, você pode utilizar todos os benefícios e ferramentas da Sympla <strong>sem nenhum custo</strong>. Nenhum mesmo!
                            </p>
                        </div>
                    </div>

                    <div class="col-4 text-center">
                        <div class="card p-3">
                            <h4>Eventos Pagos</h4>
                            <span class="text text-primary mb-3 mt-4" style="font-size: 60px">
                                10 %
                            </span>
                            <p class="mt-4">
                                Taxa cobrada por ingresso vendido. Já inclui o valor cobrado pelas administradoras de cartões de crédito e pode ser repassada integralmente ao comprador, sem nenhum custo para o produtor.
                            </p>
                            <small>Mínimo de R$4,00 por ingresso</small>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row pt-3 pb-5">
            <div class="container">
                <div class="heading text-center" style="margin-bottom: 80px; margin-top: 30px;">
                    <h2 class="text text-primary">Comece agora! Cadastre-se</h2>
                </div>
                <div class="alert">

                </div>
                <form action="{{route('api.cadastro.site')}}" method="post" id="form-cadastro">
                    <div class="col-6">
                        <div class="form-group">
                            <small>Seu Nome: <strong class="text text-danger">*</strong></small>
                            <input type="text" name="name" class="form-control" placeholder="Nome:">
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <small>Seu E-mail: <strong class="text text-danger">*</strong></small>
                            <input type="text" name="email" class="form-control" placeholder="E-mail:">
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="form-group">
                            <small>Nome da sua empresa: <strong class="text text-danger">*</strong></small>
                            <input type="text" name="nome_empresa" class="form-control" placeholder="Nome da sua empresa:">
                        </div>
                    </div>

                    <div class="col-6">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <small>Senha: <strong class="text text-danger">*</strong></small>
                                    <input type="password" name="password" class="form-control" placeholder="Senha:">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <small>Confirmação Senha: <strong class="text text-danger">*</strong></small>
                                    <input type="password" name="confirmed" class="form-control" placeholder="Confirmação Senha:">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-6">
                        <button type="submit" class="btn btn-lg mt-2 btn-success btn-block" id="btn-form-register">Cadastrar</button>
                    </div>
                </form>

            </div>
        </div>

    </section>
@endsection

@section('js')
<script>
    $(document).ready(function(){
       $('#btn-form-register').click(function (event) {
           event.preventDefault();
           var that = $('#form-cadastro');
           var button = $(this);
           var text   = '';
           $.ajax({
               url: that.attr('action'),
               data: that.serialize(),
               dataType: 'json',
               method: that.attr('method'),
               beforeSend: function(before){
                   text = button.text();
                   button.prop('disabled', true).html('Aguarde <i class="fa fa-spin fa-circle-o-notch"></i>');
                   $('.alert').removeAttr('style').empty();
               }
           })
           .done(function (response) {
               button.removeAttr('disabled').html(text);
               $('.alert')
                   .removeClass('alert-danger')
                   .addClass('alert-success')
                   .html(response)
                   .delay(3000)
                   .fadeOut(200);
                    that.find('input').val(null);
           })
           .fail(function (error) {
                button.removeAttr('disabled').html(text);
                var html = '';
                $.each(error.responseJSON, function(key, item){
                    html += '<i class="fa fa-circle"></i> ' + item + '<br>';
                });
                $('.alert').addClass('alert-danger').html(html);
           });
       });
    });
</script>
@endsection