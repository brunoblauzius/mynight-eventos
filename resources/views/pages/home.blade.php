@extends('layouts.default')
@section('header-class')
    transperant
@endsection
@section('description')
    A Payticket é um dos mais completos sistemas criado para gestão de eventos, focado na gestão de ingreços, despesas, venda do bar e gerenciamento de pessoas.
@endsection
@section('content')
    <!-- Page Inner -->
    <section class="image-bg lis-grediant grediant-tb">
        <div class="background-image-maker"></div>
        <div class="holder-image"><img src="{{ asset('dashboard/images/background/bg0001.png') }}" alt=""
                                       class="img-fluid d-none"></div>
        <div class="container">
            <div class="row justify-content-center pt-5">
                <div class="col-12 col-md-12 text-center wow ">
                    <div class="tab-content bg-white p-5 rounded-bottom rounded-top" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form class="" action="{{ route('events') }}" method="get">
                                <div class="row">
                                    <div class="col-12 col-sm-6 col-lg-6">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4"
                                                   placeholder="O que você está procurando?" name="search"/>
                                            <div class="lis-search"><i class="fa fa-search text-success"></i></div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-4 col-lg-4">
                                        <div class="form-group">
                                            <input type="text"
                                                   class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4"
                                                   placeholder="Em qual cidade você está?"/>
                                            <div class="lis-search"><i class="fa fa-map-o text-success"></i></div>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-2 col-lg-2">
                                        <button type="submit" class="btn btn-success btn-lg"><i
                                                    class="fa fa-search pr-1"></i> Procurar
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="row">
                                <div class="col-12 col-md-12">
                                    <div class="form-group">
                                        <input type="text"
                                               class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4"
                                               placeholder="O que você está procurando?" name="search"/>
                                        <div class="lis-search"><i class="fa fa-search lis-primary"></i></div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6 col-lg-4">
                                    <div class="form-group">
                                        <input type="text"
                                               class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4"
                                               placeholder="Location"/>
                                        <div class="lis-search"><i class="fa fa-map-o lis-primary"></i></div>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-4"><a href="#" class="btn btn-primary btn-block btn-lg"><i
                                                class="fa fa-search pr-1"></i> Search</a></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--End Page Inner -->

    <!-- Visited Places -->
    <div class="lis-bg-light pt-5 pb-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-12 col-md-12 text-center">
                    <div class="heading pb-4">
                        <h2>Eventos Populares Próximo de você.</h2>
                        <h5 class="font-weight-normal lis-light">Fique por dentro das melhores festas da sua cidade</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                @include('components.event-list', ['events' => $events])
            </div>
        </div>
    </div>
    <!--End Visited Places -->
@endsection
