@extends('layouts.default')
@section('description')
    A Payticket é um dos mais completos sistemas criado para gestão de eventos, focado na gestão de ingreços, despesas, venda do bar e gerenciamento de pessoas.
@endsection
@section('content')
<!-- Page Inner -->
<section class="lis-bg-light pb-5">
    <div class="container pt-5">
        <div class="row wow ">
            <div class="col-12 col-sm-6">
                <div class="page-title">
                    <h2>Contato</h2>
                    <p class="mb-0">Ficou com alguma dúvida?</p>
                </div>
            </div>
            <div class="col-12 col-sm-6 text-left text-sm-right">
                <ol class="breadcrumb mb-0 pl-0 bg-transparent">
                    <li class="breadcrumb-item d-inline-block float-none"><a href="{{ route('home') }}" class="lis-light">Inicio</a></li>
                    <li class="breadcrumb-item d-inline-block float-none active">Contato</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!--End Page Inner -->
<!-- Contact Us -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8 mb-5 mb-lg-0">
                <h6 class="lis-font-weight-500"><i class="fa fa-commenting pr-2 lis-f-14"></i>  Nos deixe uma mensagem</h6>
                <div class="card lis-brd-light wow ">
                    <div class="card-body p-4">

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{route('contact.send')}}" method="post" name="form-contato">
                            {{csrf_field()}}
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group lis-relative">
                                        <input type="text" name="name" value="{{old('name')}}" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="Seu nome">
                                        <div class="lis-search"> <i class="fa fa-user text-success lis-left-0"></i> </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group lis-relative">
                                        <input type="text" name="email" value="{{old('email')}}" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="Seu email">
                                        <div class="lis-search"> <i class="fa fa-envelope text-success lis-left-0"></i> </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12">
                                    <div class="form-group lis-relative">
                                        <input type="text" name="subject" value="{{old('subject')}}" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="Assunto">
                                        <div class="lis-search"> <i class="fa fa-drivers-license-o text-success lis-left-0"></i> </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12">
                                    <div class="form-group lis-relative mb-0">
                                        <textarea name="message" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="Mensagem">{{old('message')}}</textarea>
                                        <div class="lis-search"> <i class="fa fa-pencil text-success lis-left-0 lis-top-10"></i> </div>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-12"> <button type="submit" class="btn btn-success btn-default mt-3"> Enviar</button> </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!--<div class="col-12 col-lg-4">
                <h6 class="lis-font-weight-500"><i class="fa fa-map-o pr-2 lis-f-14"></i>  Find Us There</h6>
                <div id="map" class="w-100" style="height: 180px;"></div>
                <dl class="row mb-0 mt-4 lis-line-height-2"> <dt class="col-xl-3 col-sm-4 lis-font-weight-500 lis-dark">Address:</dt>
                    <dd class="col-xl-9 col-sm-8">FirstStreet, New York, USA</dd> <dt class="col-xl-3 col-sm-4 lis-font-weight-500 lis-dark">Phone:</dt>
                    <dd class="col-xl-9 col-sm-8">+88 25 5894 2589</dd> <dt class="col-xl-3 col-sm-4 lis-font-weight-500 lis-dark">Fax:</dt>
                    <dd class="col-xl-9 col-sm-8">+88 52 9485 9552</dd> <dt class="col-xl-3 col-sm-4 lis-font-weight-500 lis-dark">Timing:</dt>
                    <dd class="col-xl-9 col-sm-8">Mon-Sat 9:30AM - 7:30PM Sunday Closed</dd>
                </dl>
            </div>-->
        </div>
    </div>
</section>
<!--End Contact Us -->
@endsection
