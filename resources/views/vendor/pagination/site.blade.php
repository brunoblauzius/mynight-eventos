@if ($paginator->hasPages())
    <nav>
        <ul class="pagination list-inline mb-0 text-center text-uppercase lis-f-14 justify-content-center">
            @if ($paginator->onFirstPage())
                <li class="p-1 page-item disabled"><a class="page-link lis-brd-light lis-light rounded" href="#">&laquo;</a></li>
            @else
                <li class="p-1 page-item"><a class="page-link lis-brd-light lis-light rounded" href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled"><span>{{ $element }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="p-1 page-item d-none d-sm-inline-block active"><a class="page-link lis-light rounded" href="#">{{ $page }}</a></li>
                        @else
                            <li class="p-1 page-item d-none d-sm-inline-block"><a class="page-link lis-light rounded" href="{{ $url }}">{{ $page }}</a></li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="p-1 page-item"><a class="page-link lis-brd-light lis-light rounded" href="{{ $paginator->nextPageUrl() }}">&raquo</a></li>
            @else
                <li class="p-1 page-item disabled"><a class="page-link lis-brd-light lis-light rounded" href="#">&raquo</a></li>
            @endif
        </ul>
    </nav>
@endif
