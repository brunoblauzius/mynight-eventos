@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Eventos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active"><a href="{{route('admin.events.list')}}">Meus Eventos.</a></li>
                    <li class="breadcrumb-item active">Vincular listas.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <div class="row">
        <div class="col-6">

            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Editar - {{$list->description}}.</b></h4>
                <p class="text-muted font-14 m-b-20">
                    Conclua a edição dos dados.
                </p>

                    <form action="{{route('admin.lists.update.item', $list->id)}}" method="post" id="form-update">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-12">
                                <div class="form-group">
                                    <input type="text" name="description" class="form-control" placeholder="Descrição" value="{{$list->description}}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="form-group">
                                    <input type="text" name="amount" class="form-control" placeholder="Quantidade de ingressos" value="{{$list->amount}}">
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-group">
                                    <money name="value" class="form-control" placeholder="Valor (R$)" value="{{$list->value}}"></money>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mt-3">
                                <div class="custom-control custom-radio">
                                    <input name="gender" type="radio" id="masculino" class="custom-control-input" value="1" @php echo ($list->gender == 1)? 'checked' : null; @endphp>
                                    <label class="custom-control-label" for="masculino">Masculino</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input name="gender" type="radio" id="Feminino" class="custom-control-input" value="2" @php echo ($list->gender == 2)? 'checked' : null; @endphp>
                                    <label class="custom-control-label" for="Feminino">Feminino</label>
                                </div>
                                <div class="custom-control custom-radio">
                                    <input name="gender" type="radio" id="Feminino|Masculino" class="custom-control-input" value="3" @php echo ($list->gender == 3)? 'checked' : null; @endphp>
                                    <label class="custom-control-label" for="Feminino|Masculino">Feminino|Masculino</label>
                                </div>
                            </div>
                        </div>


                        <div class="form-group  col-md-12 text-center">
                            <button class="btn btn-primary ">salvar</button>
                        </div>
                    </form>
            </div>

        </div>
    </div>

@endsection

@section('js')
    <script>
        $(document).ready(function(){
            $('#form-update').submit(function(event){
                event.preventDefault();
                let element = $(this);
                let inputs = $(this).serialize();
                $.ajax({
                    url: element.attr('action'),
                    type: element.attr('method'),
                    dataType: 'json',
                    data: inputs
                })
                    .done(function(res){
                        alert(res[0]);
                        document.location.href = '{{route('admin.lists')}}';
                    })
                    .fail(function(res){
                        let html = '';
                        $.each(res.responseJSON, function(item, value){
                            html += value;
                        });
                        alert(html);
                    });
            });
        });
    </script>
@endsection