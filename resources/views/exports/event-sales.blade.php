<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<table>
    <thead>
    <tr>
        <th>Codigo venda</th>
        <th>Evento</th>
        <th>Usuario</th>
        <th>Produto</th>
        <th>Valor</th>
        <th>Qtde</th>
        <th>Total</th>
        <th>Criado Em</th>
        <th>Status</th>
        @foreach($methods_payment as $methods)
            <th>{{$methods['nome']}}</th>
        @endforeach
    </tr>
    </thead>
    <tbody>
        @foreach( $sales as $sale )
            @php
                $index = -1;
                $index = $sales_payment->search(function($item, $key) use ($sale){
                    return $item->id == $sale->codigo_venda;
                });
                $payment = (count($sales_payment[$index]->paymentsMethods) > 0)? $sales_payment[$index]->paymentsMethods : null;
            @endphp

            <tr>
                <td>{{$sale->codigo_venda}}</td>
                <td>{{$sale->evento}}</td>
                <td>{{$sale->usuario}}</td>
                <td>{{$sale->produto}}</td>
                <td>@component('components.valor', ['valor' => $sale->valor])@endcomponent</td>
                <td>{{$sale->qtde}}</td>
                <td>@component('components.valor', ['valor' => $sale->total])@endcomponent</td>
                <td>{{$sale->criado_em}}</td>
                <td>{{$sale->status}}</td>
                @if($payment != null)
                    @foreach($payment as $pagamento)
                        @if($pagamento->valor > 0)
                            <td>@component('components.valor', ['valor' => $pagamento->valor])@endcomponent</td>
                        @else
                            <td></td>
                        @endif
                    @endforeach
                @else

                @endif
            </tr>
        @endforeach
    </tbody>
</table>

</body>
</html>