@extends('layouts.default')

@section('content')
<section class="">
    <div class="container">
        <div class="row pt-5">
            <div class="col-sm-4">
                <img src="{{ place_photo($place->photo) }}" class="img-fluid" alt="">
                <ul class="list-group pt-2">
                  <li class="list-group-item"><span class="fa fa-phone"></span> {{ $place->phone }}</li>
                  <li class="list-group-item"><span class="fa fa-home"></span> {{ $place->address }}</li>
                  <li class="list-group-item">
                      <span class="fa fa-clock-o"></span> Aberto das {{$place->hora_abertura}} até {{$place->hora_fechamento}} <br>
                      Nos dias:<br>
                      @component('components.diasabertura',['value' => $place->dom,'nome' => 'Domingo']) @endcomponent
                      @component('components.diasabertura',['value' => $place->seg,'nome' => 'Segunda']) @endcomponent
                      @component('components.diasabertura',['value' => $place->ter,'nome' => 'Terça']) @endcomponent
                      @component('components.diasabertura',['value' => $place->qua,'nome' => 'Quarta']) @endcomponent
                      @component('components.diasabertura',['value' => $place->qui,'nome' => 'Quinta']) @endcomponent
                      @component('components.diasabertura',['value' => $place->sex,'nome' => 'Sexta']) @endcomponent
                      @component('components.diasabertura',['value' => $place->sab,'nome' => 'Sábado']) @endcomponent
                  </li>
                </ul>
            </div>
            <div class="col-sm-8">
                <h1 class="text-center">{{ $place->name }}</h1>

                <h6 class="lis-font-weight-500"><i class="fa fa-align-right pl-2 lis-f-14"></i> Sobre</h6>
                <div class="card lis-brd-light wow  mb-4" style="visibility: visible; animation-name: fadeInUp;">
                    <div class="card-body p-4">
                        {{ $place->description }}
                    </div>
                </div>

                @if (!empty($place->lat) && !empty($place->long))
                    <h6 class="lis-font-weight-500"><i class="fa fa-map-o pr-2 lis-f-14"></i> Mapa</h6>
                    <div class="card lis-brd-light mb-4 wow">
                        <div class="card-body p-4">
                            <div id="map" style="height: 400px;"></div>
                        </div>
                    </div>
                @endif

            </div>
            <div class="col-sm-12">
                <h5 class="lis-font-weight-500"><i class="fa fa-align-right pl-2 lis-f-14"></i> Confira as próximas atrações</h5>
                <div class="row">
                    @include('components.event-list', ['events' => $events])
                </div>
                <div class="row mt-5">
                    <div class="col-md-12">
                        {{ $events->links('vendor.pagination.site') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        var map;
        function initMap() {
            var myLatLng = {lat: {{ $place->lat }}, lng: {{$place->long}}};
            map = new google.maps.Map(document.getElementById('map'), {
                center: myLatLng,
                zoom: 16
            });

            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
                title: '{{ $place->name }}'
            });
        }
    </script>
</section>
@endsection
