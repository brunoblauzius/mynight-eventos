@extends('layouts.default')
@section('content')
<section>
    <div class="container">
        <div class="pt-5">
            <form class="" method="get">
                <h4 class="mb-2">Procure aqui, as baladas, bares ou restaurantes da sua cidade!</h4>
                <p>O melhor da sua cidade você encontra aqui.</p>
                <div class="row">

                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="form-group lis-relative">
                            <input type="text" name="search" value="{{ $search }}" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="O que você está procurando?">
                            <div class="lis-search"> <i class="fa fa-search lis-primary lis-left-0"></i> </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-4">
                        <div class="form-group lis-relative">
                            <input type="text" name="cidade" value="{{ $cidade }}" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4" placeholder="Cidade:">
                            <div class="lis-search"> <i class="fa fa-map-o lis-primary lis-left-0"></i> </div>
                            <div class="lis-search"> <a href="#" class="lis-light"><i class="fa fa-crosshairs lis-left-auto lis-right-0"></i></a> </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="form-group lis-relative">
                            <select name="category_id" class="style-select form-control border-top-0 border-left-0 border-right-0 rounded-0 pl-4">
                                <option> Todas as categorias</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                            <div class="lis-search"> <i class="fa fa-tags lis-primary lis-left-0"></i> </div>
                        </div>
                    </div>
                    <!--<div class="col-12 col-sm-6 col-lg-3">
                        <div class="form-group lis-relative">
                            <p class="pt-2 mb-2">Radius <span id="ex6CurrentSliderValLabel"> <span id="ex6SliderVal">530 </span></span>
                            </p>
                            <input id="ex6" type="text" data-slider-min="300" data-slider-max="1000" data-slider-step="1" data-slider-value="3" /> </div>
                    </div>-->

                </div>
                <!--<button class="filter-btn bg-transparent border-0 lis-dark collapsed" data-toggle="collapse" data-target="#demo"> Mais filtros </button>
                <div id="demo" class="collapse p-3 mt-3 form-wrapper">
                    <div class="row mt-3">
                        <div class="col-12 col-sm-4 col-lg-3">
                            <label id="checkbox1" class="custom-control custom-checkbox d-block">
                                <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Care Parking</span> </label>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <label id="checkbox5" class="custom-control custom-checkbox d-block">
                                <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Events</span> </label>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <label id="checkbox3" class="custom-control custom-checkbox d-block">
                                <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Wireless Internet</span> </label>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <label id="checkbox7" class="custom-control custom-checkbox d-block">
                                <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Friendly Workspace</span> </label>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <label id="checkbox2" class="custom-control custom-checkbox d-block">
                                <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Smoking Allowed</span> </label>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <label id="checkbox6" class="custom-control custom-checkbox d-block">
                                <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Acceted Bank Cards</span> </label>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <label id="checkbox4" class="custom-control custom-checkbox d-block">
                                <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Elevator Building</span> </label>
                        </div>
                        <div class="col-12 col-sm-4 col-lg-3">
                            <label id="checkbox8" class="custom-control custom-checkbox d-block">
                                <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description">Street Parking</span> </label>
                        </div>
                    </div>
                </div>-->
                <div class="row my-4">
                    <div class="col-12 col-md-12">
                        <button type="submit" class="btn btn-primary btn-default"><i class="fa fa-search pr-2"></i> Procurar casa</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 align-self-center">
                        <p>{{ $places->total() }} Casa(s) encontrada(s)</p>
                    </div>
                </div>
            </form>
            <div class="row">
                @if($places->count() > 0)
                    @foreach($places as $place)
                        <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-4 text-center place-box">
                            <img src="{{ asset('images/default/place.jpg') }}" class="img-fluid" alt="{{$place->name}}">
                            <img src="{{ place_photo($place->photo) }}" alt="{{$place->name}}" class="lis-mt-minus-60 mx-auto mb-4 mb-lg-0 border lis-border-width-2 rounded-circle border-white" width="120" />
                            <h5 class="mb-0 lis-font-weight-600 place-name"><a href="{{ route('place', ['slug' => $place->slug]) }}">{{ $place->name }}</a></h5>
                        </div>
                    @endforeach
                @else
                    <div class="alert alert-warning text-center">
                        Nenhum registro encontrado.
                    </div>
                @endif
            </div>


            <div class="row mt-5">
                <div class="col-md-12">
                    {{ $places->appends(request()->except('page'))->links('vendor.pagination.site') }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
