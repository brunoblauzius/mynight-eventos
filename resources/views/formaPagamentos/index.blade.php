@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Formas de Pagamentos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Pagamentos.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Formas de pagamento.</b></h4>
                {{--<p class="text-muted font-14 m-b-20">--}}
                    {{--Cadastre os grupos de produtos que irá vender.--}}
                    {{--<a href="#" class="btn btn-xs btn-primary btn-bordered pull-right" id="btn-add-new"><i class="fa fa-plus"></i> Incluir novo</a>--}}
                {{--</p>--}}

                <div class="table-responsive">
                    <table class="table m-0 table-colored-bordered table-bordered-inverse">
                        <thead>
                        <tr>
                            <th width="60%">Descrição</th>
                            <th>Status</th>
                            <th>Criado em</th>
                            <th width="60">#</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($formasPagamentos as $formaPagamento)
                            <tr>
                                <td>{{$formaPagamento->nome}}</td>
                                <td>
                                    @if($formaPagamento->status == 1)
                                        <span class="label label-success"><i class="fa fa-check-circle"></i> Ativo</span>
                                    @else
                                        <span class="label label-danger"><i class="fa fa-times-circle"></i> Inativo</span>
                                    @endif
                                </td>
                                <td>{{$formaPagamento->criado_em}}</td>
                                <td>
                                    @if($formaPagamento->default == 0)
                                        <div class="btn-group-xs">
                                            <a href="{{route('admin.grupos.edit', $formaPagamento->id)}}" class="btn btn-bordered btn-info edit-list"><i class="fa fa-pencil"></i></a>
                                            <a href="{{route('admin.grupos.destroy', $formaPagamento->id)}}" class="btn btn-bordered btn-danger delete-list"><i class="fa fa-times"></i></a>
                                        </div>
                                    @else
                                        <span class="label label-purple">Cadastro Geral</span>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
    <!-- end row -->

@endsection


@section('js')
    <script>

        function destroy(element, href){
            if(confirm('Deseja realmente excluir esse item?')){
                return $.ajax({
                    url: href,
                    data:{},
                    method: 'delete',
                    dataType: 'json'
                });
            }
        }

        function editJson(element, href){
            return $.ajax({
                url: href,
                data:{},
                method: 'get',
                dataType: 'json'
            });
        }


        $(document).ready(function(){

        });
    </script>
@endsection