<div class="custom-control custom-checkbox checkbox-inline">
    @php $checked = ($checked)?'checked':null;@endphp
    <input type="hidden" name="{{$name}}" value="0">
    <input class="custom-control-input" type="checkbox" {{$checked}} id="{{$name}}" name="{{$name}}" value="1">
    <label class="custom-control-label" for="{{$name}}">{{$tag_name}}</label>
</div>