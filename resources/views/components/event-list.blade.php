@if($events->count() > 0 )
    @foreach($events as $event)
        <div class="col-12 col-md-6 col-lg-6 col-xl-4 mb-4">
            <div class="card lis-brd-light wow text-center text-lg-left">
                <a href="{{ route('event', ['slug' => $event->slug]) }}">
                    <div class="lis-grediant grediant-tb-light2 lis-relative modImage lis-radius rounded-top event-list-cover"> <img src="{{ asset('storage/'. $event->image) }}" alt="" class="img-fluid rounded-top w-100" /> </div>
                </a>
                <div class="card-body pt-0">
                    <div class="media d-block d-lg-flex lis-relative"> <img src="{{ ($event->place!=null)?place_photo($event->place->photo):null }}" alt="" class="lis-mt-minus-15 img-fluid d-lg-flex mx-auto mr-lg-3 mb-4 mb-lg-0 border lis-border-width-2 rounded-circle border-white" width="80" />
                        <div class="media-body align-self-start mt-2">
                            <h6 class="mb-0 lis-font-weight-600"><A href="{{ route('event', ['slug' => $event->slug]) }}" class="lis-dark">{{ $event->title }}</A></h6>
                        </div>
                    </div>
                    <div class="">
                        <small>Data: {{ $event->data_start }}</small> <small>Inicio: {{ $event->hora_start }}</small> <br>
                        {{--<small>{{ $event->address }}</small>--}}
                    </div>
                    <div class="clearfix">
                        <div class="float-none float-lg-left mb-3 mb-lg-0 mt-1">
                            {{--<A href="#" class="text-white"><i class="icofont icofont-beer px-2 lis-bg2 py-2 lis-rounded-circle-50 lis-f-14"></i></A>--}}
                            <!--<A href="#" class="text-white"><i class="icofont icofont-fast-food px-2 lis-bg4 py-2 lis-rounded-circle-50 lis-f-14"></i></A>
                            <A href="#" class="lis-id-info lis-light p-2 lis-rounded-circle-50 lis-f-14">1 More...</A>-->
                        </div>
                        <div class="float-none float-lg-right mt-1">
                            <A href="#" class="lis-light lis-f-14"><i class="fa fa-envelope-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                            <A href="#" class="lis-light lis-f-14"><i class="fa fa-heart-o lis-id-info  lis-rounded-circle-50 text-center"></i></A>
                            <A href="#" class="lis-green-light text-white p-2 lis-rounded-circle-50 lis-f-14"><i class="fa fa-star"></i> 3.0</A>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@else
<div class="col-12">
    <div class="alert alert-info text-center">
        Nenhuma atração foi encontrada no momento.
    </div>
</div>
@endif
