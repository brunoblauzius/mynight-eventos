<small>
    @if($gender == 2)
        <span class="label label-famale">Feminino</span>
    @elseif($gender == 1)
        <span class="label label-male">Masculino</span>
    @else
        <span class="label label-male">Masculino</span>
        <span class="label label-famale">Feminino</span>
    @endif
</small>