@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">PDV - FORÇA DE VENDAS.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Controle Financeiro.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-12 ">
        <div class="card-box">
            <form action="{{route('admin.pdv.sales')}}" method="get" name="form">
                <div class="row">
                    <div class="col-md-4">
                        <select class="form-control" name="events_id">
                            <option>** selecione o evento **</option>
                            @foreach($events as $event)
                                <option
                                        @php echo (Request::get('events_id') == $event->id)?'selected':null;@endphp
                                        value="{{$event->id}}">{{$event->title}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <select class="form-control" name="users_id">
                            <option value="">** selecione o vendedor **</option>
                            @foreach($users as $user)
                                <option
                                        @php echo (Request::get('users_id') == $user->id)?'selected':null;@endphp
                                        value="{{$user->id}}">{{$user->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-4">
                        <button class="btn btn-block btn-custom"><i class="fa fa-search"></i> filtrar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="card-box">
            @if($sales->count() > 0)
                <table class="table m-0 table-colored-bordered table-bordered-inverse">
                    <thead>
                        <tr>
                            <th>Caixa</th>
                            <th>Evento</th>
                            <th>Data</th>
                            <th>Qtde. Produtos</th>
                            <th>Total (R$)</th>
                            <th>Modo de pagamento.</th>
                            <th width="90">#</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach( $sales as $sale )
                            <tr class="@php echo ($sale->canceled == 1)?'text-danger':null;@endphp">
                                <td><strong>{{($sale->user != null)?$sale->user->name:''}}</strong></td>
                                <td><strong>{{($sale->event != null)?$sale->event->title:'Venda: '.$sale->data_venda}}</strong></td>
                                <td>{{$sale->data_venda}}</td>
                                <td>{{$sale->total_amount}} Un.</td>
                                <td>R$ {{number_format($sale->total_value, 2, ',', '.')}}</td>
                                <td>
                                    <span class="label label-purple">{{join(', ', $sale->paymentsMethods->where('valor', '>', 0)->pluck('formaDePagamento.nome')->toArray())}}</span>
                                </td>
                                <td>
                                    @if($sale->canceled != 1)
                                    <a href="{{route('sales.canceled', $sale->id)}}" class="btn btn-xs btn-danger btn-bordered canceled-sales" title="Cancelar Venda"><i class="fa fa-times"></i></a>
                                    @endif
                                    <a href="{{route('sales.printSale', $sale->id)}}" class="btn btn-xs btn-purple btn-bordered print" title="Imprimir Venda"><i class="fa fa-print"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <nav class="m-t-10">
                    {{$sales->appends(Request::except('page'))->links('vendor.pagination.bootstrap-4')}}
                </nav>
            @else
                <div class="alert alert-info text-center">
                    <strong>Nenhum registro encontrado</strong>
                </div>
            @endif
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $('.print').click(function (event) {
            event.preventDefault();
            var url = $(this).attr('href');
            window.open(
                url,
                "_blank",
                "toolbar=0,scrollbars=1,resizable=0,top=10,left=500,width=400,height=600"
            );
        });

        $('.canceled-sales').click(function (event) {
            event.preventDefault();
            var url = $(this).attr('href');

            Vue.swal({
                title: 'Você quer cancelar este item?',
                text: "Atenção ao selecionar [SIM] este item será cancelado da venda!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#64c5b1',
                cancelButtonColor: '#d33',
                confirmButtonText: 'SIM, quero cancelar'
            }).then((result) => {
                $.ajax({
                    url: url,
                    data:{},
                    dataType:'json',
                    method: 'get'
                }).done(function(response){
                    Vue.swal(
                        'Sucesso',
                        response[0],
                        'success'
                    ).then((result) => {
                        window.location.reload();
                    })
                });
            });

        });

    });
</script>
@endsection