@extends('layouts.print')

@section('content')
    <div style="width: 600px;">
        @php $i = 1;@endphp
        @foreach( $sale->saleProducts as $produto)
            <h1 style="text-align: center">TICKET PARA RETIRADA</h1>
            <h1 style="text-align: center; font-size: 80px; margin-top: 30px;">{{($sale->event != null)?$sale->event->title:'Venda: '.$sale->data_venda}}</h1><br>
            <h1 style="text-align: center; font-size: 50px; margin-top: 30px;"><br>
                {{$produto->produto->name}}<br>
                R$ {{$produto->value}}
            </h1>
            <h1 style="text-align: center">{{$sale->data_venda}} ({{str_pad($sale->id,6,'0', STR_PAD_LEFT)}})</h1>
            <h1 style="text-align: center">
                CAIXA: {{$sale->user->name}}<br>
                {{$i}} de {{$sale->saleProducts->count()}} TOTAL: R$ {{number_format($sale->saleProducts->sum('value'),2, ',', '.')}}
            </h1>
            <br><h1 style="margin: auto; text-align: center"><img src="{{asset('dashboard/images/logo-sextou.png')}}" height="100" ></h1><br>
            <h3 style="text-align: center">----------------------- CORTE ----------------------</h3>
            @php $i++;@endphp
        @endforeach
    </div>
@endsection