@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">PDV - {{$event->title}}.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">PDV.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')

    <pdv
            :user="{{Auth::user()}}"
            :event="{{$event}}"
    ></pdv>

@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('body').addClass('enlarged');
            $('#form-update').submit(function (event) {
                event.preventDefault();
                let element = $(this);
                let inputs = $(this).serialize();
                $.ajax({
                    url: element.attr('action'),
                    type: element.attr('method'),
                    dataType: 'json',
                    data: inputs
                })
                    .done(function (res) {
                        alert(res[0]);
                        document.location.href = '{{route('admin.lists')}}';
                    })
                    .fail(function (res) {
                        let html = '';
                        $.each(res.responseJSON, function (item, value) {
                            html += value;
                        });
                        alert(html);
                    });
            });
        });
    </script>
@endsection