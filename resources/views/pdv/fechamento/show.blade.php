@extends('layouts.print')

@section('content')

    <style>
        body{
            font-size: 16px;
        }
        table{

            width: 100%;
        }
        thead>tr{
            padding: 5px;
        }
        thead>tr>td{
            border-bottom: 1.5px dashed black;
        }
        tfoot>tr>td{
            border-top: 1.5px dashed black;
        }
        tr>td{
            padding: 3px;
        }
    </style>
<div style="width: 600px; margin: auto;">
<pre>
<span style="margin-bottom: 0px; padding-bottom: 0px; font-size: 50px; font-weight: bold; text-align: center">
 Fechamento de Caixa
</span>

<span style="margin: 0 0; padding: 0px 0px; ">
Secure ID:
<strong style="font-size: 18px">{{$fechamento['fechamento']->secure_id}}</strong>
</span>

Evento: <strong>{{$fechamento['fechamento']->event->title}}</strong>
Abertura: <strong>{{($fechamento['abertura']!=null)?$fechamento['abertura']->criado_em:null}}</strong>
Fechamento: <strong>{{$fechamento['fechamento']->criado_em}}</strong>
Responsavel: <strong>{{$fechamento['fechamento']->user->name}}</strong>


    <span style="margin-bottom: 0px; padding-bottom: 0px; font-size: 30px; font-weight: bold; text-align: center">
          Resumo do Caixa
    </span>
@php $total =0;@endphp
@foreach($fechamento['resumo_caixa'] as $resumo)
@php $total += $resumo->valor;@endphp
{{$resumo->nome}}:
<strong style="font-size: 22px;">R$ {{number_format($resumo->valor, 2, ',', '.')}}</strong>
@endforeach
Total:
<strong style="font-size: 22px;">R$ {{number_format($total, 2, ',', '.')}}</strong>

    <span style="margin-bottom: 0px; padding-bottom: 0px; font-size: 30px; font-weight: bold; text-align: center">
          Tipo Transação
    </span>
    <table>
        <thead>
            <tr>
                <td>Tipo</td>
                <td style="text-align: right">R$</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Venda de Ticket</td>
                <td style="text-align: right">{{number_format($fechamento['produtos']->sum('value'), 2, ',', '.')}}</td>
            </tr>
            <tr>
                <td>Cancelamentos</td>
                <td style="text-align: right">{{number_format($fechamento['vendas_canceladas']->sum('total_value'), 2, ',', '.')}}</td>
            </tr>
        </tbody>
    </table>
    <span style="margin-bottom: 0px; padding-bottom: 0px; font-size: 30px; font-weight: bold; text-align: center">
          Vendas Produtos
    </span>
    <table>
        <thead>
            <tr>
                <td>Produto</td>
                <td>Qtde</td>
                <td style="text-align: right">R$</td>
            </tr>
        </thead>
        <tbody>
            @php $total =0;@endphp
            @foreach($fechamento['produtos'] as $sales)
                @php $total += $sales->value;@endphp
                <tr>
                    <td>{{$sales->produto->name}}</td>
                    <td>{{$sales->amount}}</td>
                    <td style="text-align: right">{{number_format($sales->value, 2, ',', '.')}}</td>
                </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <td colspan="3" style="text-align: right;"><strong>Total R$ {{number_format($total, 2, ',', '.')}}</strong></td>
            </tr>
        </tfoot>
    </table>



Assinatura:




----------------------------------------------------------------------
                      <strong>{{$fechamento['fechamento']->user->name}}</strong>
                        {{$fechamento['fechamento']->criado_em}}


                      <img src="{{asset('dashboard/images/logo-payticket-b.png')}}" alt="{{ env('APP_NAME') }}" height="50" >
                        www.payticket.com.br
</pre>
</div>
@endsection