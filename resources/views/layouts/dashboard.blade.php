<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>{{ env('APP_NAME') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Sextou sistemas para eventos" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="csrf-token" content="{{csrf_token()}}">

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('dashboard/images/logo_sm.ico')}}">

    <script src="https://js.pusher.com/4.3/pusher.min.js"></script>

    <!-- App css -->
    <link href="{{asset('dashboard/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('dashboard/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('dashboard/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('dashboard/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet" type="text/css" />

    @yield('css')

    <script>
        window.user_id = '{{(Auth::check())?Auth::user()->id:0}}';
        window.web_app = '{{url('/')}}';
        window._token     = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    </script>

</head>


<body>

<!-- Begin page -->
<div id="wrapper">
    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="{{url('/home')}}" class="logo">
            <span>
                <img src="{{asset('dashboard/images/logo-payticket.png')}}" alt="" height="40">
            </span>
                <i>
                    <img src="{{asset('dashboard/images/logo_sm.png')}}" alt="" height="40">
                </i>
            </a>
        </div>

        <nav class="navbar-custom">

            <ul class="list-inline float-right mb-0" id="notification-list">

                <notifications></notifications>


                <li class="list-inline-item dropdown notification-list" id="avisos">
                    <a class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                       aria-haspopup="false" aria-expanded="false">
                        @if(!Auth::check() || empty(Auth::user()->avatar))
                            <img src="{{asset('dashboard/images/users/user-thumb.png')}}" alt="user" class="rounded-circle">
                        @else
                            <img src="{{asset('dashboard/images/users/'.Auth::user()->avatar)}}" alt="user" class="rounded-circle">
                        @endif
                    </a>
                    <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                        <!-- item-->
                        <div class="dropdown-item noti-title">
                            <h5 class="text-overflow"><small>Bem vindo! @if(Auth::check()) {{Auth::user()->name}} @endif</small> </h5>
                        </div>

                        <!-- item-->
                        @can('menu.setting.companies')
                            <a href="{{ route('admin.place.edit') }}" class="dropdown-item notify-item">
                                <i class="mdi mdi-account-circle"></i> Minha Empresa
                            </a>
                        @endcan
                        <!-- item-->
                        <a href="javascript:void(0);" class="dropdown-item notify-item">
                            <i class="mdi mdi-lock-open"></i> <span>Lock Screen</span>
                        </a>

                        <!-- item-->

                        <a href="{{ route('logout') }}" class="dropdown-item notify-item"
                           onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                            <i class="mdi mdi-power"></i> Logout
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>

                    </div>
                </li>

            </ul>

            <ul class="list-inline menu-left mb-0">
                <li class="float-left">
                    <button class="button-menu-mobile open-left waves-light waves-effect">
                        <i class="dripicons-menu"></i>
                    </button>
                </li>
                <li class="hide-phone app-search">

                </li>
            </ul>

        </nav>

    </div>
    <!-- Top Bar End -->
    <!-- ========== Left Sidebar Start ========== -->
    <div class="left side-menu">
        <div class="slimscroll-menu" id="remove-scroll">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu" id="side-menu">
                    <li class="menu-title">Navegação</li>
                    @can('menu.settings.default')
                        <li>
                            <a href="javascript: void(0);"><i class="fi-briefcase"></i> <span> Cadastros </span> <span class="menu-arrow"></span></a>
                            <ul class="nav-second-level" aria-expanded="false">
                                <li><a href="{{route('usuarios.index')}}">Usuários</a></li>
                                {{--<li><a href="admin-grid.html">Operações</a></li>--}}
                                <li><a href="{{route('grupos.index')}}">Grupos de Produtos</a></li>
                                <li><a href="{{route('produtos.index')}}">Produtos</a></li>
                                <li><a href="{{route('admin.lists')}}"> Gerenciar Listas </a></li>
                                <li><a href="{{route('admin.formasPagamento')}}"> Formas de Pagamento </a></li>
                            </ul>
                        </li>
                    @endcan

                    @can('menu.events')
                        <li>
                            <a href="{{route('admin.events.list')}}"><i class="fi-folder"></i> <span> Eventos </span></a>
                        </li>
                        <li>
                            <a href="{{route('admin.galeries')}}"><i class="fa fa-photo"></i> <span> Gerenciar Galerias </span></a>
                        </li>
                    @endcan

                    @can('menu.pdv')
                        <li>
                            <a href="{{route('admin.pdv')}}"><i class="fa fa-money"></i> <span> PDV </span></a>
                        </li>
                    @endcan
                    @can('menu.finance.control')
                        <li>
                            <a href="{{route('admin.pdv.sales')}}"><i class="fa fa-dollar"></i> <span> Controle Financeiro </span></a>
                        </li>
                    @endcan
                    {{--@can('menu.meus.locais')--}}
                        {{--<li>--}}
                            {{--<a href="javascript: void(0);"><i class="fa fa-home"></i> <span> Meus locais </span></a>--}}
                        {{--</li>--}}
                    {{--@endcan--}}
                </ul>

            </div>
            <!-- Sidebar -->
            <div class="clearfix"></div>

        </div>
        <!-- Sidebar -left -->

    </div>
    <!-- Left Sidebar End -->
    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="content-page">
        <!-- Start content -->
        <div class="content">
            <div class="container-fluid" id="app">

                @yield('header_page')

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                @yield('content')

            </div> <!-- container -->

        </div> <!-- content -->

        <footer class="footer text-right">
            2017 - 2018 © www.payticket.com.br - eventos
        </footer>

    </div>
    <!-- ============================================================== -->
    <!-- End Right content here -->
    <!-- ============================================================== -->
</div>
<!-- END wrapper -->

<script>
    function notifyMe() {
        // Let's check if the browser supports notifications
        if (!("Notification" in window)) {
            alert("This browser does not support desktop notification");
        }
        // Otherwise, we need to ask the user for permission
        else if (Notification.permission !== "denied") {
            Notification.requestPermission().then(function (permission) {
                // If the user accepts, let's create a notification
            });
        }

        // At last, if the user has denied notifications, and you
        // want to be respectful there is no need to bother them any more.
    }
    notifyMe();
</script>

<!-- jQuery  -->
<script src="{{asset('dashboard/js/jquery.min.js')}}?{{time()}}"></script>
<script src="{{asset('dashboard/js/popper.min.js')}}?{{time()}}"></script><!-- Popper for Bootstrap -->
<script src="{{asset('dashboard/js/bootstrap.min.js')}}?{{time()}}"></script>
<script src="{{asset('dashboard/js/metisMenu.min.js')}}?{{time()}}"></script>
<script src="{{asset('dashboard/js/waves.js')}}?{{time()}}"></script>
<script src="{{asset('dashboard/js/jquery.slimscroll.js')}}?{{time()}}"></script>

<!-- App js -->
<script src="{{asset('dashboard/js/jquery.core.js')}}"></script>
<script src="{{asset('dashboard/js/jquery.app.js')}}"></script>

<script src="{{ asset('js/inputmask.js') }}"></script>
<script src="{{ asset('js/inputmask.extensions.js') }}"></script>
<script src="{{ asset('js/inputmask.numeric.extensions.js') }}"></script>
<script src="{{ asset('js/inputmask.date.extensions.js') }}"></script>
<script src="{{ asset('js/jquery.inputmask.js') }}"></script>
<script src="{{ asset('js/functions.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/notifications.js') }}"></script>

@yield('js')

</body>
</html>