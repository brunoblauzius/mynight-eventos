<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>{{ env('APP_NAME') }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta content="A fully featured admin theme which can be used to build CRM, CMS, etc." name="description" />
    <meta content="Sextou sistemas para eventos" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{asset('dashboard/images/logo_sm.ico')}}">

    <!-- App css -->
    <link href="{{asset('dashboard/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('dashboard/css/icons.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('dashboard/css/metismenu.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('dashboard/css/style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/css/style.css')}}" rel="stylesheet" type="text/css" />
    <script src="{{asset('dashboard/css/modernizr.min.css')}}"></script>

</head>


<body class="bg-dark">

<!-- HOME -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">

                <div class="wrapper-page">
                    @yield('content')
                </div>
                <!-- end wrapper -->

            </div>
        </div>
    </div>
</section>
<!-- END HOME -->



<script>
    var resizefunc = [];
</script>

<!-- jQuery  -->
<script src="{{asset('dashboard/js/jquery.min.js')}}"></script>
<script src="{{asset('dashboard/js/popper.min.js')}}"></script><!-- Popper for Bootstrap -->
<script src="{{asset('dashboard/js/bootstrap.min.js')}}"></script>
<script src="{{asset('dashboard/js/metisMenu.min.js')}}"></script>
<script src="{{asset('dashboard/js/waves.js')}}"></script>
<script src="{{asset('dashboard/js/jquery.slimscroll.js')}}"></script>

<!-- App js -->
<script src="{{asset('dashboard/js/jquery.core.js')}}"></script>
<script src="{{asset('dashboard/js/jquery.app.js')}}"></script>

</body>
</html>