<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ env('APP_NAME') }}</title>
    <meta name="description" content="@yield('description')">
    <link rel="shortcut icon" href="{{asset('dashboard/images/logo_sm.ico')}}">
    <!--Plugin CSS-->
    <link href="{{ url('dist/css/plugins.min.css') }}" rel="stylesheet">
    <!--main Css-->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link href="{{ asset('vendor/countdown/css/soon.min.css') }}" rel="stylesheet">
    <link href="{{ url('dist/css/main.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">

    <script type="text/javascript" src="{{ asset('admin/plugins/jquery/jquery-3.3.1.min.js') }}"></script>
</head>

<body>
<!-- header -->
<div id="header-fix" class="header fixed-top @yield('header-class')">
    <nav class="navbar navbar-toggleable-md navbar-expand-lg navbar-light py-lg-0 py-4">
        <a class="navbar-brand mr-4 mr-md-5" href="{{ url('/') }}">
            <img src="{{ asset('dashboard/images/logo-payticket.png') }}" alt="" style="height: 30px;">
        </a>
        <div id="dl-menu" class="dl-menuwrapper d-block d-lg-none float-right">
            <button>Open Menu</button>
            <ul class="dl-menu">
                <li><a class="nav-link" href="{{ route('events') }}">Eventos</a></li>
                {{--<li><a class="nav-link" href="{{ route('place.list') }}">Casas</a></li>--}}
                {{--<li><a class="nav-link" href="{{ route('categories') }}">Categorias</a></li>--}}
                <li><a class="nav-link" href="{{ route('contact') }}">Contato</a></li>
                {{--<li> <a href="#modal" class="text-white login_form"><i class="fa fa-sign-in pr-2"></i> Sign In | Register</a></li>--}}
                {{--<li> <a href="add-listing.html" ><i class="fa fa-plus pr-1"></i> Add Listing</a></li>--}}
            </ul>
        </div>

        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav mr-auto">
                <li><a class="nav-link" href="{{ route('events') }}">Eventos</a></li>
                <li><a class="nav-link" href="{{ route('for.companies') }}">Para empresas</a></li>
                {{--<li><a class="nav-link" href="{{ route('place.list') }}">Casas</a></li>--}}
                {{--<li><a class="nav-link" href="{{ route('categories') }}">Categorias</a></li>--}}
                <li><a class="nav-link" href="{{ route('contact') }}">Contato</a></li>
            </ul>
            <ul class="list-unstyled my-2 my-lg-0">
                <li>
                    {{--<a href="" class="text-white login_form"><i class="fa fa-sign-in pr-2"></i> Login</a>--}}
                    <a href="{{url('/login')}}" class="text-white login_form"><i class="fa fa-sign-in pr-2"></i> Login</a>
                </li>
            </ul>
            {{--<a href="add-listing.html" class="btn btn-outline-light btn-sm ml-0 ml-lg-4 mt-3 mt-lg-0"><i class="fa fa-plus pr-1"></i> Add Listing</a> --}}
        </div>
    </nav>
</div>
<!--End header -->
@yield('content')
<!-- Footer-->
<section class="image-bg footer lis-grediant grediant-bt pb-0 pt-5">
    <div class="background-image-maker"></div>
    <div class="holder-image"><img src="{{ asset('dashboard/images/background/bg0001.png') }}" alt=""
                                   class="img-fluid d-none"></div>
    <div class="container">
        <div class="row pb-0">
            <div class="col-12 col-md-12 text-center">
                <div class="footer-logo">
                    <a href="#">
                        <img src="{{ asset('dashboard/images/logo-payticket.png') }}" alt="" style="height: 45px;">
                    </a>
                </div>
                <p class="my-4">Os melhores eventos da sua cidade reunidos em um só lugar.</p>
            </div>
        </div>
    </div>
    <div class="footer-bottom mt-5 py-4">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 text-center text-md-left mb-3 mb-md-0">
                    <span> &copy; 2018 {{ env('APP_NAME') }}. </span></div>
                <div class="col-12 col-md-6 text-center text-md-right">
                    <ul class="list-inline footer-social mb-0">
                        <li class="list-inline-item pr-3">
                            <A href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li class="list-inline-item pr-3">
                            <A href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!--End  Footer-->
<!-- Top To Bottom-->
<a href="#" class="scrollup text-center lis-bg-primary lis-rounded-circle-50">
    <div class="text-white mb-0 lis-line-height-1_7 h3"><i class="icofont icofont-long-arrow-up"></i></div>
</a>
<!-- End Top To Bottom -->
<!-- Login /Register Form-->
<div class="container">


    <div id="modal" class="popupContainer" style="display: none;">
        <header class="popupHeader">
            <span class="header_title">Entrar no sistema</span>
            <span class="modal_close"><i class="fa fa-times"></i></span>
        </header>

        <div class="popupBody">
            <!-- Social Login -->
            <!-- Username & Password Login form -->
            <div class="user_login">
                <form>
                    <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 "
                           placeholder="username or email address"/>
                    <br/>
                    <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0 "
                           placeholder="password"/>
                    <br/>
                    <div class="checkbox">
                        <input id="remember" type="checkbox"/>
                        <label for="remember">Lermbrar neste computador</label>
                    </div>
                    <div class="action_btns">
                        <a href="#" class="btn btn-primary btn-default mt-3 w-100">Login</a>
                    </div>
                </form>
                <br/>
                Sign in with your social network<br/>
                <ul class="list-inline my-0">
                    <li class="list-inline-item mr-0"><a href="#"
                                                         class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i
                                    class="fa fa-facebook"></i></a></li>
                    <li class="list-inline-item mr-0"><a href="#"
                                                         class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i
                                    class="fa fa-twitter"></i></a></li>
                </ul>
                <hr/>
                Don't have an account <a href="#" class="register_form">Sign Up</a>
            </div>

            <!-- Register Form -->
            <div class="user_register">
                <form>

                    <input type="text" class="form-control border-top-0 border-left-0 border-right-0 rounded-0"
                           placeholder="Username"/>
                    <br/>


                    <input type="email" class="form-control border-top-0 border-left-0 border-right-0 rounded-0"
                           placeholder="Email Address"/>
                    <br/>


                    <input type="password" class="form-control border-top-0 border-left-0 border-right-0 rounded-0"
                           placeholder="Password"/>
                    <br/>

                    <div class="checkbox">
                        <input id="send_updates" type="checkbox"/>
                        <label for="send_updates">Send me occasional email updates</label>
                    </div>

                    <div class="action_btns">
                        <a href="#" class="btn btn-primary btn-default mt-3 w-100">Register</a>
                    </div>
                </form>
                <br/>
                Register with your social network<br/>
                <ul class="list-inline my-0">
                    <li class="list-inline-item mr-0"><a href="#"
                                                         class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i
                                    class="fa fa-facebook"></i></a></li>
                    <li class="list-inline-item mr-0"><a href="#"
                                                         class="lis-light lis-social border lis-brd-light text-center lis-line-height-2_3 rounded d-block"><i
                                    class="fa fa-twitter"></i></a></li>
                </ul>
                <hr/>
                Already have an account <a href="#" class="login_form">Sign In</a>
            </div>
        </div>
    </div>
</div>
<!-- End Login /Register Form-->
<!-- jQuery -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAUErdhbm98UVF1T7VdnDH-w-dXgacVsQ&callback=initMap" async
        defer></script>
<script src="{{ url('vendor/countdown/js/soon.min.js') }}"></script>
<script src="{{ url('dist/js/plugins.min.js') }}"></script>
<script src="{{ url('dist/js/common.js') }}"></script>

@yield('js')


</body>

</html>
