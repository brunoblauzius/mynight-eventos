@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Grupos de Produtos.</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Página inicial</a></li>
                    <li class="breadcrumb-item active">Grupos.</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <h4 class="m-t-0 header-title"><b>Grupos de Produtos.</b></h4>
                <p class="text-muted font-14 m-b-20">
                    Cadastre os grupos de produtos que irá vender.
                    <a href="#" class="btn btn-xs btn-primary btn-bordered pull-right" id="btn-add-new"><i class="fa fa-plus"></i> Incluir novo</a>
                </p>

                <div class="table-responsive">
                    <table class="table m-0 table-colored-bordered table-bordered-inverse">
                        <thead>
                            <tr>
                                <th width="60%">Nome</th>
                                <th>Status</th>
                                <th>Criado em</th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($grupos as $grupo)
                                <tr>
                                    <td>{{$grupo->descricao}}</td>
                                    <td>
                                        @if($grupo->status == 1)
                                            <span class="label label-success"><i class="fa fa-check-circle"></i> Ativo</span>
                                        @else
                                            <span class="label label-danger"><i class="fa fa-times-circle"></i> Inativo</span>
                                        @endif
                                    </td>
                                    <td>{{$grupo->criado_em}}</td>
                                    <td>
                                        @if($grupo->default == 0)
                                            <div class="btn-group-xs">
                                                <a href="{{route('admin.grupos.edit', $grupo->id)}}" class="btn btn-bordered btn-info edit-list"><i class="fa fa-pencil"></i></a>
                                                <a href="{{route('admin.grupos.destroy', $grupo->id)}}" class="btn btn-bordered btn-danger delete-list"><i class="fa fa-times"></i></a>
                                            </div>
                                        @else
                                            <span class="label label-inverse">Cadastro Geral</span>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$grupos->links()}}
                </div>
            </div>
        </div>

    </div>
    <!-- end row -->


    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Formulário</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{route('admin.grupos.store')}}" method="post" id="form-cadastro-grupo">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group ">
                                            <input type="text" name="descricao" id="descricao" class="form-control" placeholder="Descrição">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mt-3">
                                        <div class="custom-control custom-radio">
                                            <input name="status" type="radio" id="ativo" class="custom-control-input" checked value="1">
                                            <label class="custom-control-label" for="ativo">Ativo</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input name="status" type="radio" id="inativo" class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="inativo">Inativo</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-center">
                                        <button class="btn btn-primary">salvar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Editar</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{route('admin.grupos.update')}}" method="post" id="form-cadastro-grupo-edit">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group ">
                                            <input type="text" name="descricao" id="descricao" class="form-control" placeholder="Descrição">
                                            <input type="hidden" name="id" id="id">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mt-3">
                                        <div class="custom-control custom-radio">
                                            <input name="status" type="radio" id="ativo-1" class="custom-control-input" checked value="1">
                                            <label class="custom-control-label" for="ativo-1">Ativo</label>
                                        </div>
                                        <div class="custom-control custom-radio">
                                            <input name="status" type="radio" id="inativo-1" class="custom-control-input" value="0">
                                            <label class="custom-control-label" for="inativo-1">Inativo</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group text-center">
                                        <button class="btn btn-primary">salvar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('js')
    <script>

        function destroy(element, href){
            if(confirm('Deseja realmente excluir esse item?')){
                return $.ajax({
                    url: href,
                    data:{},
                    method: 'delete',
                    dataType: 'json'
                });
            }
        }

        function editJson(element, href){
            return $.ajax({
                url: href,
                data:{},
                method: 'get',
                dataType: 'json'
            });
        }


        $(document).ready(function(){
            $('#btn-add-new').click(function(e){
                e.preventDefault();
                $('#myModal').modal('show');
            });


            $('#form-cadastro-grupo, #form-cadastro-grupo-edit').submit(function(event){
                event.preventDefault();
                let element = $(this);
                let inputs = $(this).serialize();
                let that = $(this);
                $.ajax({
                    url: element.attr('action'),
                    type: element.attr('method'),
                    dataType: 'json',
                    data: inputs
                })
                    .done(function(res){
                        alert(res[0]);
                        $('#descricao').val(null);
                        $('#id').val(null);
                        window.location.reload();
                    })
                    .fail(function(res){
                        let html = '';
                        $.each(res.responseJSON, function(item, value){
                            html += value;
                        });
                        alert(html);
                    });
            });


            $(document).on('click', '.delete-list', function(event){
                event.preventDefault();
                let element = $(this);
                let href = element.prop('href');
                destroy(element, href).done(function(response){
                    alert(response);
                    document.location.reload();
                }).fail(function(response){
                    alert(response.responseText);
                });
            });


            $(document).on('click', '.edit-list', function(event){
                event.preventDefault();
                let element = $(this);
                let href = element.prop('href');
                editJson(element, href).done(function(response){
                    let form = $('#form-cadastro-grupo-edit');
                    form.find('input[name="descricao"]').val(response.descricao);
                    form.find('input[name="id"]').val(response.id);
                    if(response.status == 1){
                        form.find('input[name="id"]').prop(response.id);
                    }
                    $('#myModalEdit').modal('show');
                }).fail(function(response){
                    alert(response.responseText);
                });
            });

        });
    </script>
@endsection