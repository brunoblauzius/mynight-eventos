@extends('layouts.dashboard')

@section('header_page')
    <div class="row">
        <div class="col-12">
            <div class="page-title-box">
                <h4 class="page-title float-left">Dashboard</h4>

                <ol class="breadcrumb float-right">
                    <li class="breadcrumb-item"><a href="#">Payticket - eventos</a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ol>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
@endsection

@section('content')
<div class="row" id="app">
    <div class="col-12">
        <dashboard
            :usuario="{{json_encode($usuario)}}"
            :accounts="{{json_encode($accounts)}}"
        ></dashboard>

    </div>
</div>
<!-- end row -->

@endsection
