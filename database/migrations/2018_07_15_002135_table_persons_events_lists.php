<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TablePersonsEventsLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('persons_events_lists', function(Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('list_id');
            $table->string('name', 255);
            $table->timestamps();
        });

        Schema::table('persons_events_lists', function(Blueprint $table){
            $table->foreign('event_id')->references('events')->on('id');
            $table->foreign('user_id')->references('users')->on('id');
            $table->foreign('list_id')->references('lists')->on('id');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
