<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollumPlacesHours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("places", function(Blueprint $table){
            $table->time('hora_abertura')->nullable();
            $table->time('hora_fechamento')->nullable();
            $table->tinyInteger('dom')->default(0)->nullable();
            $table->tinyInteger('seg')->default(0)->nullable();
            $table->tinyInteger('ter')->default(0)->nullable();
            $table->tinyInteger('qua')->default(0)->nullable();
            $table->tinyInteger('qui')->default(0)->nullable();
            $table->tinyInteger('sex')->default(0)->nullable();
            $table->tinyInteger('sab')->default(0)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
