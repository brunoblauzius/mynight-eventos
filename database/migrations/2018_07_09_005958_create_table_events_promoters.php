<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEventsPromoters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events_promoters', function (Blueprint $table){
            $table->increments('id');
            $table->unsignedInteger('event_id');
            $table->unsignedInteger('list_id');
            $table->unsignedInteger('promoter_id');
            $table->unsignedInteger('user_id');
            $table->integer('amount');
            $table->timestamps();
        });

        Schema::table('events_promoters', function (Blueprint $table){
            $table->foreign('event_id')->references('id')->on('events');
            $table->foreign('list_id')->references('id')->on('lists');
            $table->foreign('promoter_id')->references('id')->on('promoters');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
