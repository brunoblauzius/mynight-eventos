<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollunsAddressEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function(Blueprint $table){
            $table->string('numero', 50)->index();
            $table->string('logradouro', 255)->index();
            $table->string('municipio', 255)->index();
            $table->string('cidade', 255)->index();
            $table->string('estado', 255)->index();
            $table->string('pais', 255)->index();
            $table->string('cep', 255)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
