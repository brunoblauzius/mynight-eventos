<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCollunsTablePlaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('places', function(Blueprint $table){
            $table->string('numero', 50);
            $table->string('logradouro', 255);
            $table->string('municipio', 255);
            $table->string('cidade', 255);
            $table->string('estado', 255);
            $table->string('pais', 255);
            $table->string('cep', 255);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
