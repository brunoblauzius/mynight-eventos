<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lists', function(Blueprint $table){
            $table->increments('id');
            $table->integer('amount')->default(0);
            $table->decimal('value', 8, 2)->default(0.00);
            $table->tinyInteger('gender')->nullable();
            $table->tinyInteger('status')->default(1);
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
