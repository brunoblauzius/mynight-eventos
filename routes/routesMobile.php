<?php
/**
 * Created by PhpStorm.
 * User: Bruno
 * Date: 23/12/2018
 * Time: 14:05
 */

Route::post('/usuarios/{id}/update', 'Usuarios\MobileController@update')->name('mobile.user.update');
Route::post('/usuarios/login', 'Usuarios\UsuariosController@login')->name('mobile.login');

Route::get('/eventos/{id}/carregar-modelo', 'PDV\ForcaVendasController@carregaModelo')->name('mobile.carregaModelo');
Route::post('/eventos/abrir-caixa', 'PDV\ForcaVendasController@abrirCaixa')->name('mobile.abrirCaixa');
Route::post('/eventos/payment-method', 'PDV\ForcaVendasController@store')->name('mobile.store');