<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'SiteController@index')->name('home');

Route::get('/eventos', 'EventsController@list')->name('events');
Route::get('/evento/{slug}', 'EventsController@event')->name('event');
Route::get('/casas', 'PlacesController@list')->name('place.list');
Route::get('/casa/{slug}', 'PlacesController@place')->name('place');
// Route::get('/evento', function() { return view('events.event'); })->name('event');
Route::get('/categorias', 'SiteController@categories')->name('categories');
Route::post('/contato', 'SiteController@contact')->name('contact.send');
Route::get('/para-empresas', 'SiteController@forCompanies')->name('for.companies');
Route::get('/empresas/ativar/{token}', 'SiteController@activeCompanies')->name('active.companies');


Route::get('/contato', function() { return view('pages.contact'); })->name('contact');
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function() {
    Route::get('/create-profile', function() {
        dd(Auth::user()->id);
    })->name('places.create');

    Route::get('/events', 'Admin\EventsController@list')->name('admin.events.list');
    Route::get('/events/{id}/promoters', 'Admin\EventsController@eventLists')->name('admin.event.promoters');
    Route::get('/events/{id}/export-sales-excel', 'Admin\EventsController@export')->name('admin.event.export.sales');
    Route::get('/events/{id}/listas', 'Admin\EventsController@eventLists')->name('admin.event.lists.vincular');
    Route::post('/events/attach-lists', 'Admin\EventsController@attachLists')->name('admin.event.attach.lists');
    Route::get('/events/{id}/edit', 'Admin\EventsController@edit')->name('admin.events.edit');
    Route::get('/events/create', 'Admin\EventsController@create')->name('admin.events.create');
    Route::post('/events/store', 'Admin\EventsController@store')->name('admin.events.store');
    Route::get('/events/{id}/minha-lista', 'Admin\EventsController@minhaLista')->name('admin.event.minha.lista');
    Route::post('/events/{id}/update', 'Admin\EventsController@update')->name('admin.events.update');
    Route::post('/events/{id}/upload-cover', 'Admin\EventsController@uploadCoverImage')->name('admin.events.upload-cover');
    Route::post('/events/attach-person', 'Admin\EventsController@attachPersonEventList')->name('admin.events.attach-person');
    Route::get('/events/{id}/portaria', 'Admin\EventsController@portaria')->name('admin.events.portaria');
    Route::get('/events/{id}/cadastro-caixa', 'Admin\EventsController@createCaixa')->name('admin.events.createCaixa');
    Route::post('/events/{id}/vincular-caixa', 'Admin\EventsController@vincularCaixa')->name('admin.events.vincularCaixa');
    Route::get('/events/{id}/desvincular-caixa/{user_id}', 'Admin\EventsController@desvincularCaixa')->name('admin.events.desvincularCaixa');
    Route::get('/events/{id}/lists', 'Admin\ListsController@index')->name('admin.events.lists');
    Route::get('/events/{id}/statistic', 'Admin\EventsController@statistic')->name('admin.events.statistic');
    Route::get('/events/{id}/create-bar', 'Admin\EventsController@createBar')->name('admin.events.createBar');
    Route::get('/events/{id}/produtos/{produto_id}/editar', 'Admin\EventsController@barEdit')->name('admin.event.produto.edit');
    Route::get('/events/{id}/produtos/{produto_id}', 'Admin\EventsController@barDesvinculate')->name('admin.event.produto.delete');
    Route::delete('/events/{id}/produtos/{produto_id}', 'Admin\EventsController@barDesvinculate')->name('admin.event.produto.delete');

    Route::get('/place/edit', 'Admin\PlacesController@edit')->name('admin.place.edit');
    Route::post('/place/update', 'Admin\PlacesController@update')->name('admin.place.update');
    Route::post('/place/upload-photo', 'Admin\PlacesController@uploadPhoto')->name('admin.place.upload-photo');
    Route::get('/galeries', 'Admin\GaleriesController@list')->name('admin.galeries');
    Route::get('/galery/{galery_id}', 'Admin\GaleriesController@galery')->name('admin.galery');
    Route::post('/galery/store', 'Admin\GaleriesController@store')->name('admin.galery.store');
    Route::post('/galery/{galery_id}/add-photos', 'Admin\GaleriesController@addPhotos')->name('admin.galery.addPhotos');

    Route::get('/promoters', 'Admin\PromotersController@index')->name('admin.promoters');
    Route::get('/promoters/cadastro', 'Admin\PromotersController@create')->name('admin.promoters.create');
    Route::get('/promoters/{id}/edit', 'Admin\PromotersController@edit')->name('admin.promoters.edit');
    Route::post('/promoters/{id}/update', 'Admin\PromotersController@update')->name('admin.promoters.update');
    Route::post('/promoters/store', 'Admin\PromotersController@store')->name('admin.promoters.store');
    Route::get('/events/{id}/promoters', 'Admin\PromotersController@eventsPromoters')->name('admin.event.promoters');
    Route::get('/events/{event_id}/promoters/desvincular/{id}', 'Admin\PromotersController@desvincularPromoter')->name('admin.event.desvincularPromoter');
    Route::post('/events/{id}/promoters/vinculate', 'Admin\PromotersController@eventsPromotersVinculate')->name('admin.event.promoters.vinculate');
    Route::post('/events/{id}/bar/vinculate', 'Admin\EventsController@eventsBarVinculate')->name('admin.event.bar.vinculate');
    Route::post('/events/bar/vinculate/update', 'Admin\EventsController@eventsBarUpdateVinculate')->name('admin.event.bar.update.vinculate');


    Route::get('/lists', 'Admin\ListsController@index')->name('admin.lists');
    Route::get('/list/{id}/edit', 'Admin\ListsController@edit')->name('admin.lists.edit.item');
    Route::post('/list/{id}/update', 'Admin\ListsController@update')->name('admin.lists.update.item');
    Route::post('/events/lists/store', 'Admin\ListsController@store')->name('admin.lists.store');
    Route::get('/events/lists/list-promoter', 'Admin\ListsController@getListPromoter')->name('admin.lists.listpromoter');


    /**
     * Usuarios
     */
    Route::get('/usuarios', 'Usuarios\UsuariosController@index')->name('usuarios.index');
    Route::get('/usuarios/cadastro', 'Usuarios\UsuariosController@create')->name('usuarios.create');
    Route::post('/usuario/store', 'Usuarios\UsuariosController@store')->name('usuario.store');
    Route::get('/usuario/{id}/delete', 'Usuarios\UsuariosController@destroy')->name('usuario.destroy');
    Route::get('/usuario/{id}/editar', 'Usuarios\UsuariosController@edit')->name('usuario.edit');
    Route::post('/usuario/{id}/update', 'Usuarios\UsuariosController@update')->name('usuario.update');

    /**
     * grupos produtos
     */
    Route::get('/grupos', 'Produtos\GruposProdutosController@index')->name('grupos.index');
    Route::post('/grupos/store', 'Produtos\GruposProdutosController@store')->name('admin.grupos.store');
    Route::get('/grupos/{id}/edit', 'Produtos\GruposProdutosController@edit')->name('admin.grupos.edit');
    Route::post('/grupos/update', 'Produtos\GruposProdutosController@update')->name('admin.grupos.update');

    /**
     * formas de pagamento
     */
    Route::get('/formas-pagamentos', 'FormaPagamento\FormasPagamentosController@index')->name('admin.formasPagamento');


    /**
     * PDV
     */
    Route::get('/forca-de-vendas', 'PDV\ForcaVendasController@list')->name('admin.pdv');
    Route::get('/events/{id}/forca-de-vendas', 'PDV\ForcaVendasController@create')->name('admin.events.pdv');
    Route::get('/forca-de-vendas/{id}/imprimir', 'PDV\ForcaVendasController@imprimir')->name('admin.pdv.imprimir');
    Route::post('/forca-de-vendas/fechar-venda', 'PDV\ForcaVendasController@store')->name('admin.pdv.store');
    Route::get('/carregar-modelo', 'PDV\ForcaVendasController@carregaModelo')->name('admin.carregaModelo');
    Route::get('/controle-financeiro', 'PDV\ForcaVendasController@index')->name('admin.pdv.sales');
    Route::post('/payment-method', 'PDV\ForcaVendasController@store')->name('admin.store');
    Route::post('/abrir-caixa', 'PDV\ForcaVendasController@abrirCaixa')->name('admin.abrirCaixa');
    Route::get('/sales/{id}', 'PDV\ForcaVendasController@printSale')->name('sales.printSale');
    Route::get('/sales/canceled/{id}', 'PDV\ForcaVendasController@canceled')->name('sales.canceled');


    /**
     * produtos
     */
    Route::get('/produtos', 'Produtos\ProdutosController@index')->name('produtos.index');
    Route::get('/produtos/{id}/editar', 'Produtos\ProdutosController@edit')->name('admin.produtos.edit');
    Route::get('/produtos/cadastrar', 'Produtos\ProdutosController@create')->name('admin.produto.create');
    Route::get('/produtos/lista-produtos', 'Produtos\ProdutosController@all')->name('admin.produtos.all');
    Route::post('/produtos/store', 'Produtos\ProdutosController@store')->name('admin.produto.store');
    Route::post('/produtos/{id}/update', 'Produtos\ProdutosController@update')->name('admin.produto.update');

    /**
     * notifications
     */
    Route::get('/notifications/show', 'Notification\NotificationsController@index')->name('notifications.index');

});

Route::get('/test', 'Notification\NotificationsController@create');

Broadcast::routes();

Route::get('/address', function(){
    $endereco = [];
    $google = new \App\Lib\GoogleMaps\GoogleMaps();
    $return = $google->getLatLongByAddress('rua leila diniz, 531 pinhais paraná');
    if(!empty($return)){
        $resultado = $return->results[0]->address_components;
        $endereco = [
          'number' => $resultado[0]->long_name,
          'street' => $resultado[1]->long_name,
          'municipio' => $resultado[2]->long_name,
          'cidade' => $resultado[3]->long_name,
          'estado' => $resultado[4]->long_name,
          'pais' => $resultado[5]->long_name,
          'cep' => $resultado[6]->long_name,
        ];
    }
    return response()->json($endereco);
});

