<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//include(getcwd().'/../routes/routesMobile.php');
Route::post('/usuarios/{id}/update', 'Usuarios\MobileController@update')->name('mobile.user.update');
Route::post('/usuarios/login', 'Usuarios\UsuariosController@login')->name('mobile.login');

Route::get('/eventos/{id}/carregar-modelo', 'PDV\ForcaVendasController@carregaModelo')->name('mobile.carregaModelo');
Route::post('/eventos/abrir-caixa', 'PDV\ForcaVendasController@abrirCaixa')->name('mobile.abrirCaixa');
Route::post('/eventos/payment-method', 'PDV\ForcaVendasController@store')->name('mobile.store');






Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('registrar', 'SiteController@create')->name('api.cadastro.site');


Route::delete('/list/{id}/destroy', 'Admin\ListsController@destroy')->name('api.destroy.item');
Route::delete('/grupos/{id}/delete', 'Produtos\GruposProdutosController@destroy')->name('admin.grupos.destroy');
Route::delete('/produtos/{id}/delete', 'Produtos\ProdutosController@destroy')->name('admin.produtos.destroy');


/**
 * PDV
 */
Route::get('/events/{id}/forca-de-vendas', 'PDV\ForcaVendasController@create')->name('admin.events.pdv');
Route::get('/forca-de-vendas/{id}/imprimir', 'PDV\ForcaVendasController@imprimir')->name('admin.pdv.imprimir');
Route::post('/forca-de-vendas/fechar-venda', 'PDV\ForcaVendasController@store')->name('admin.pdv.store');
Route::get('/carregar-modelo', 'PDV\ForcaVendasController@carregaModelo')->name('admin.carregaModelo');
Route::get('/controle-financeiro', 'PDV\ForcaVendasController@index')->name('admin.pdv.sales');
Route::post('/payment-method', 'PDV\ForcaVendasController@store')->name('admin.store');
Route::post('/abrir-caixa', 'PDV\ForcaVendasController@abrirCaixa')->name('admin.abrirCaixa');

Route::get('/sales/{id}', 'PDV\ForcaVendasController@printSale')->name('sales.printSale');
Route::get('/sales/canceled/{id}', 'PDV\ForcaVendasController@canceled')->name('sales.canceled');

Route::post('/forca-de-vendas/fechamento-caixa', 'PDV\FechamentosController@store')->name('admin.fechamento.caixa');
Route::get('/forca-de-vendas/fechamento-caixa/{id}', 'PDV\FechamentosController@show')->name('api.fechamento.caixa');

Route::get('/events/{id}/payment-sales', 'Admin\EventsController@paymentSales')->name('sales.paymentSales');

/**
 * Companies
 */
Route::post('/api/empresas/account', 'Empresa\EmpresasController@account')->name('empresas.account');


/**
 * USER
 */
Route::get('/usuarios/check-is-user', 'Usuarios\UsuariosController@checkIsUser')->name('usuarios.checkIsUser');


/**
 * notifications
 */
Route::get('/notifications/{id?}', 'Notification\NotificationsController@show')->name('notifications');