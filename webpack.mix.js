let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/notifications.js', 'public/js')
    .copy('resources/assets/dashboard', 'public/dashboard')
    .copy('resources/assets/plugins', 'public/plugins')
    .copy('node_modules/inputmask/dist/inputmask', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');
