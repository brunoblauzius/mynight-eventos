
function getJson(anchor,data,url){
    return $.ajax({
        url: url,
        method: 'get',
        dataType: 'json',
        data: data,
    });
}

function postJson(anchor,data,url){
    return $.ajax({
        url: url,
        method: 'post',
        dataType: 'json',
        data: data,
    });
}

$(document).ready(function(){
    $(".money").inputmask( 'currency',{"autoUnmask": true,
        radixPoint:",",
        groupSeparator: ".",
        allowMinus: false,
        prefix: 'R$ ',
        digits: 2,
        digitsOptional: false,
        rightAlign: true,
        unmaskAsNumber: true
    });
});