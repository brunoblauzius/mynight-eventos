<?php

namespace App\Providers;

use App\Event;
use App\Policies\EventsPolicy;
use App\Role;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
        Event::class => EventsPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('menu.pdv', function ($user) {
            return in_array($user->roles_id, [Role::GERENTE, Role::PROPRIETARIO, Role::CAIXA]);
        });
        Gate::define('menu.finance.control', function ($user) {
            return in_array($user->roles_id, [Role::GERENTE, Role::PROPRIETARIO, Role::ADM]);
        });
        Gate::define('menu.settings.default', function ($user) {
            return in_array($user->roles_id, [Role::GERENTE, Role::PROPRIETARIO, Role::ADM]);
        });
        Gate::define('menu.events', function ($user) {
            return in_array($user->roles_id, [Role::GERENTE, Role::PROPRIETARIO, Role::ADM]);
        });
        Gate::define('menu.setting.companies', function ($user) {
            return in_array($user->roles_id, [Role::PROPRIETARIO]);
        });

        Gate::define('menu.meus.locais', function ($user) {
            return in_array($user->roles_id, [Role::PROPRIETARIO]);
        });
    }
}
