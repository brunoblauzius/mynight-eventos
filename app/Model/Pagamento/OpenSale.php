<?php

namespace App\Model\Pagamento;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class OpenSale extends Model
{
    protected $table = 'open_sales';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    /**
     * MUTED
     */
    public function getCriadoEmAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y H:i:s');
    }

}
