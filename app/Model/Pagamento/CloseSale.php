<?php

namespace App\Model\Pagamento;

use App\Event;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class CloseSale extends Model
{
    protected $table = 'closures';

    protected $guarded = ['id'];


    /**
     * relations
     */

    public function event(){
        return $this->belongsTo(Event::class, 'events_id', 'id');
    }

    public function user(){
        return $this->belongsTo(User::class, 'users_id', 'id');
    }


    /**
     * MUTED
     */

    public function getCriadoEmAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y H:i:s');
    }
}
