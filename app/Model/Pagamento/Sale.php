<?php

namespace App\Model\Pagamento;

use App\Event;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    //
    protected $guarded = ['id'];

    public function user(){
        return $this->hasOne(User::class, 'id', 'users_id');
    }

    public function event(){
        return $this->hasOne(Event::class, 'id', 'events_id');
    }

    public function saleProducts(){
        return $this->hasMany(SaleProduct::class, 'sales_id');
    }

    public function paymentsMethods(){
        return $this->hasMany(PaymentMethod::class, 'sales_id');
    }


    public function getDataVendaAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y H:i:s');
    }

}
