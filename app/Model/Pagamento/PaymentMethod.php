<?php

namespace App\Model\Pagamento;

use App\Model\FormaPagamento\FormaPagamento;
use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    //
    protected $guarded = ['id'];

    /**
     * Relations
     */

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function formaDePagamento(){
        return $this->belongsTo(FormaPagamento::class, 'formas_pagamentos_id');
    }
}
