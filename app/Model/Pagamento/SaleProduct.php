<?php

namespace App\Model\Pagamento;

use App\Model\Produtos\Produto;
use Illuminate\Database\Eloquent\Model;

class SaleProduct extends Model
{
    //
    protected $table = 'sales_products';
    protected $guarded = ['id'];


    public function produto(){
        return $this->belongsTo(Produto::class, 'produtos_id');
    }

    public function sale(){
        return $this->belongsTo(Sale::class, 'sales_id');
    }
}
