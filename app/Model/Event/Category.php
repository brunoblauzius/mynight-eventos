<?php

namespace App\Model\Event;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'events_categories';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];
}
