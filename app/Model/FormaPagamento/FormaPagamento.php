<?php

namespace App\Model\FormaPagamento;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class FormaPagamento extends Model
{
    protected $table = 'formas_pagamentos';

    protected $guarded = ['id'];

    public function getCriadoEmAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y H:i:s');
    }

}
