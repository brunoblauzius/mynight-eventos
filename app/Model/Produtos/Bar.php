<?php

namespace App\Model\Produtos;

use Illuminate\Database\Eloquent\Model;

class Bar extends Model
{
    protected $table = 'events_produtos';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public function produto(){
        return $this->belongsTo(Produto::class, 'produtos_id', 'id');
    }
}
