<?php

namespace App\Model\Produtos;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produtos';

    protected $primaryKey = 'id';

    protected $guarded = ['id', 'cover'];

    public function grupo(){
        return $this->belongsTo(GrupoProduto::class, 'produto_grupos_id');
    }

    public function getCriadoEmAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y');
    }

    public function getThumbPathAttribute()
    {
        return !empty($this->attributes['image'])?asset('storage/'.$this->attributes['thumb_path']):'';
    }
}
