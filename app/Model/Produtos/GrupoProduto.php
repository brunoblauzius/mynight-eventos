<?php

namespace App\Model\Produtos;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class GrupoProduto extends Model
{
    protected $table = 'produto_grupos';

    protected $primaryKey= 'id';

    protected $guarded = ['id'];


    public function getCriadoEmAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y');
    }
}
