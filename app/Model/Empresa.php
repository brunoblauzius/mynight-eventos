<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{

    protected $table = 'empresas';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public function account(){
        return $this->belongsToMany(Accont::class, 'empresas_has_acconts', 'empresas_id', 'acconts_id')
            ->withPivot([
                'expirated_at'
            ]);
    }

}
