<?php

namespace App\Model\Notification;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Queue\SerializesModels;

class Notification extends Model
{

    protected $table = 'notifications';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    public function getCriadoEmAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y H:i:s');
    }

    /**
     * relations
     */
    public function user(){
        return $this->belongsTo(User::class,  'users_id');
    }
}
