<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Request;

class SendContact extends Mailable
{
    use Queueable, SerializesModels;

    public $params;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( Request $request)
    {
        $this->params = $request->all();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->from($this->params['email'], $this->params['name'])
            ->to('brunoblauzius@gmail.com', 'Bruno Blauzius Schuindt')
            ->subject($this->params['subject'])
            ->view('emails.site.contact')
            ->with([
                'message' => $this->params['message'],
                'name'    => $this->params['name'],
                'email'   => $this->params['email'],
                'subject' => $this->params['subject'],
            ]);
    }
}
