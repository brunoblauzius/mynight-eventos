<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PersonEventList extends Model
{
    protected $table = 'persons_events_lists';

    protected $primaryKey = 'id';

    protected $guarded = ['id'];


    public function user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function event(){
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }

    public function lista(){
        return $this->belongsTo(Lista::class, 'list_id', 'id');
    }

}
