<?php

namespace App\Observers;

use App\Place;
use App\Lib\GoogleMaps\GoogleMaps;
use App\Services\GoogleMapService;

class PlaceObserver
{
    public function creating(Place $place)
    {
        $this->updateLatLong($place);
    }

    public function updating(Place $place)
    {
        $this->updateLatLong($place);
    }

    private function updateLatLong(Place $place)
    {
        if(!empty($place->address)){
            $endereco    = GoogleMapService::getAddress($place->address);
            $place->fill($endereco);
        }
    }
}
