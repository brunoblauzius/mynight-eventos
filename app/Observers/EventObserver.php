<?php
/**
 * Created by PhpStorm.
 * User: Bruno
 * Date: 16/08/2018
 * Time: 21:51
 */

namespace App\Observers;


use App\Event;
use App\Services\GoogleMapService;

class EventObserver
{

    public function creating(Event $event){
        $endereco = GoogleMapService::getAddress($event->address);
        $event->fill($endereco);
    }


    public function updating(Event $event){
        $endereco = GoogleMapService::getAddress($event->address);
        $event->fill($endereco);
    }

}