<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use App\Place;
use App\Event;

class PlacesController extends Controller
{
    public function list(Request $request)
    {
        $places = Place::query()
            ->when($request->input('cidade') != null, function($query) use ($request){
                return $query->where('cidade', ucwords($request->input('cidade')));
            })
            ->when($request->input('search')!= null, function($query) use ($request){
                return $query->where('name', 'like','%'.$request->input('search').'%');
            })
            ->paginate(10);




        return view('places.list')
            ->with('places', $places)
            ->with('categories', Category::orderBy('name', 'asc')->get())
            ->with('search', $request->input('search'))
            ->with('cidade', $request->input('cidade'));
    }

    public function place($slug)
    {
        $place = Place::query()
            ->where('slug', $slug)
            ->firstOrFail();
        $events = Event::query()
            ->where('place_id', $place->id)
            ->where('end_at', '>=', date('Y-m-d'))
            ->orderBy('start_at')
            ->paginate(3);
        return view('places.place')
            ->with('place', $place)
            ->with('events', $events);
    }
}
