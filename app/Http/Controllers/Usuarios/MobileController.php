<?php

namespace App\Http\Controllers\Usuarios;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MobileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {

        $dataValidator = [
            'name' => 'required',
            'email' => 'required|email',
            'telefone' => 'required|string',
        ];

        if($request->has('password') && !empty($request->input('password'))){
            $dataValidator = array_merge(
                $dataValidator,
                [
                    'password'         => 'required|string|same:confirm_password',
                    'confirm_password' => 'required|string',
                ]);
        }

        $validator = validator(
            $request->all(),
            $dataValidator
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 422);
        }

        $user = User::find($id);
        $user->fill($request->except("password", "confirm_password"));
        if($request->has('password') && !empty($request->input('password'))){
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();
        return response()->json("atualizado com sucesso.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
