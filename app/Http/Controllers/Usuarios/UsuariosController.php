<?php

namespace App\Http\Controllers\Usuarios;

use App\Http\Controllers\Controller;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuarios = User::with('role')->where('empresas_id', Auth::user()->empresas_id)->get();
        return view('usuarios.index')->with('usuarios', $usuarios);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('id', '>', 1)->get();
        return view('usuarios.create')->with('roles', $roles)->with('user', new User());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validator = validator(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'login' => 'required',
                'roles_id' => 'required|integer',
                'password' => 'required|min:6|same:confirm_password',
                'confirm_password' => 'required|min:6',
            ]
        );

        if($validator->fails()){
            return redirect()->route('usuarios.create')->withErrors($validator)->withInput($request->all());
        }

        $user = new User($request->all());
        $user->place_id = Auth::user()->place_id;
        $user->empresas_id = Auth::user()->empresas_id;
        $user->password = bcrypt($user->password);
        $user->save();
        return redirect()->route('usuarios.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $user = User::with('role')->find($id);
        $roles = Role::where('id', '>', 1)->get();
        return view('usuarios.create')->with('roles', $roles)->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $validator = validator(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required|email',
                'login' => 'required',
                'roles_id' => 'required|integer',
            ]
        );

        if($validator->fails()){
            return redirect()->route('usuarios.create')->withErrors($validator)->withInput($request->all());
        }

        $user = User::find($id);
        $user->fill($request->all());
        if($request->has('password') && !empty($request->input('password'))){
            $user->password = bcrypt($request->input('password'));
        }
        $user->save();
        return redirect()->route('usuario.edit', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $user = User::find($id);
        $user->forceDelete();
        return redirect()->route('usuarios.index');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkIsUser(Request $request){
        $validator = validator(
            $request->all(),
            [
                'login' => 'required|unique:users,login'
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 422);
        }

        return response()->json('ok');
    }



    public function username()
    {
        $identity  = request()->get('email');
        $fieldName = filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'login';
        request()->merge([$fieldName => $identity]);
        return $fieldName;
    }

    /**
     * login mobile
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $validator = validator($request->all(),
            [
                'email' => 'required|string',
                'password' => 'required|string',
            ],
            [
                'email.required' => 'Login ou e-mail é requirido',
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 422);
        }

        if(Auth::attempt([$this->username() => $request->input('email'), 'password' => $request->input('password')])){
            $usuario = Auth::user();
            $usuario->empresa;
            $usuario->role;
            return response()->json($usuario);
        } else {
            return response()->json("credenciais informadas nao correspondem a nenhum usuario no sisstema", 400);
        }

    }




}

