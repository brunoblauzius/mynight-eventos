<?php

namespace App\Http\Controllers\Auth;

use App\Model\Empresa;
use App\Role;
use App\User;
use App\Place;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $empresa = new Empresa([
            'name' => trim(strip_tags($data['name']))
        ]);
        $empresa->save();

        $place = new Place();
        $place->name = $data['name'];
        $place->empresas_id = $empresa->id;
        $place->status = Place::STATUS_ACTIVE;
        $place->slug = Place::getSlug($data['name']);
        $place->save();


        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'roles_id' => Role::PROPRIETARIO,
            'place_id' => $place->id,
            'empresas_id' => $empresa->id,
            'password' => bcrypt($data['password']),
        ]);
    }
}
