<?php

namespace App\Http\Controllers;

use App\Category;
use App\Mail\SendContact;
use App\Mail\SendRegisterCompanies;
use App\Model\Empresa;
use App\Place;
use App\Role;
use App\User;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Http\Request;
use App\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class SiteController extends Controller
{
    public function index()
    {
        $events = Event::query()
            ->where('start_at', '>=', Carbon::now()->format('Y-m-d'))
            ->where('visible_website', 1)
            ->orderBy('start_at')
            ->limit(6)
            ->get();
        return view('pages.home')
            ->with('events', $events);
    }


    public function categories(){
        return view('events.categories')->with(
            'categories', Category::all()
        );
    }


    /**
     * @param Request $request
     * @return $this
     */
    public function contact(Request $request){

        $validator = validator(
            $request->all(),
            [
                'name' => 'required',
                'email' => 'required|email',
                'subject' => 'required',
                'message' => 'required|max:150',
            ]
        );

        if($validator->fails()){
            return redirect()->route('contact')->withErrors($validator)->withInput();
        }

        Mail::send(new SendContact($request));

    }


    public function forCompanies(){
        return view('pages.for_companies');
    }

    /**
     * @param Request $request
     * @param string $token
     */
    public function activeCompanies(Request $request, string $token){
        $user = User::findRememberToken($token)->first();
        if($user != null){
            $user->status = 1;
            $user->save();
            Auth::login($user);
            return redirect('/home');
        }
        return redirect('/', 302);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request){

        $data = $request->all();

        $validator = \validator(
                            $data,
                            [
                                'name' => 'required|string|max:255',
                                'nome_empresa' => 'required|string|max:255',
                                'email' => 'required|string|email|max:255|unique:users',
                                'confirmed' => 'required|string|min:6',
                                'password' => 'required|string|min:6|same:confirmed',
                            ]
                        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 422);
        }

        $empresa = new Empresa([
            'name' => trim(strip_tags($data['nome_empresa']))
        ]);
        $empresa->save();

        $place = new Place();
        $place->name = $data['name'];
        $place->empresas_id = $empresa->id;
        $place->status = Place::STATUS_ACTIVE;
        $place->slug = Place::getSlug($data['name']);
        $place->save();


        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'roles_id' => Role::PROPRIETARIO,
            'place_id' => $place->id,
            'status'   => 0,
            'empresas_id' => $empresa->id,
            'remember_token' => Crypt::encrypt($empresa->id),
            'password' => bcrypt($data['password']),
        ]);

        $empresa->users_id = $user->id;
        $empresa->save();

        Mail::send(new SendRegisterCompanies($user));

        return response()->json('Obrigado por se cadastrar, verifique seu email: <strong>'.$data['email'].'</strong> e confirme seu cadastro para utilizar o sistema.');
    }

}
