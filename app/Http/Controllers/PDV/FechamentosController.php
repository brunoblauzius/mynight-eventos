<?php

namespace App\Http\Controllers\PDV;

use App\Model\Pagamento\CloseSale;
use App\Model\Pagamento\OpenSale;
use App\Services\Event\EventService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FechamentosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator(
            $request->all(),
            [
                'events_id' => 'required|integer',
                'users_id'  => 'required|integer',
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 422);
        }

        $closureSales = new CloseSale($request->all());
        $closureSales->secure_id = uniqid(date('YmdHis'));
        $closureSales->save();

        return response()->json($closureSales);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $closeSales = CloseSale::with(['event', 'user'])->find($id);
        $openSales  = OpenSale::where([
            'events_id' => $closeSales->events_id,
            'users_id'  => $closeSales->users_id
        ])->first();

        return view('pdv.fechamento.show')->with('fechamento', collect([
            'abertura'     => $openSales,
            'fechamento'   => $closeSales,
            'vendas_canceladas'   => EventService::vendasCanceladas($closeSales->events_id, $closeSales->users_id),
            'produtos'            => EventService::produtosMaisVendios($closeSales->events_id, $closeSales->users_id),
            'resumo_caixa'        => EventService::paymentSales($closeSales->events_id, $closeSales->users_id),
        ]));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


}
