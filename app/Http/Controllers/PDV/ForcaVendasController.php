<?php

namespace App\Http\Controllers\PDV;

use App\Event;
use App\Lib\Utils;
use App\Model\FormaPagamento\FormaPagamento;
use App\Model\Pagamento\CloseSale;
use App\Model\Pagamento\OpenSale;
use App\Model\Pagamento\PaymentMethod;
use App\Model\Pagamento\Sale;
use App\Model\Pagamento\SaleProduct;
use App\Model\Produtos\Produto;
use App\Role;
use App\Services\Event\EventService;
use App\Services\PDV\ForcaVendaService;
use App\Services\PDV\ProdutosEventsService;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ForcaVendasController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $users = User::where([
            'empresas_id' => Auth::user()->empresas_id
        ])
        ->whereIn('roles_id', [Role::CAIXA, Role::PROPRIETARIO])
        ->get();

        $events = Event::where('empresas_id', Auth::user()->empresas_id)
            ->whereIn('id', Sale::groupBy('events_id')->select('events_id')->get())
            ->get();

        $sales = Sale::with(['saleProducts', 'event', 'paymentsMethods.formaDePagamento', 'user'])
            ->join('users', 'users.id', '=', 'sales.users_id')
            ->when(!empty($request->input('users_id')), function ($query) use ($request) {
                return $query->where('users_id', $request->input('users_id'));
            })
            ->when(!empty($request->input('events_id')), function ($query) use ($request) {
                return $query->where('events_id', $request->input('events_id'));
            })
            ->when(Auth::user()->roles_id == Role::CAIXA,
                function ($q) {
                    return $q->where('users_id', Auth::user()->id);
                },
                function ($q) {
                    return $q->where('empresas_id', Auth::user()->empresas_id);
                })
            ->select('sales.*')
            ->paginate(15);
        return view('pdv.index')
            ->with('events', $events)
            ->with('sales', $sales)
            ->with('users', $users);
    }


    /**
     * list events for sales products
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        $events = Event::where('empresas_id', Auth::user()->empresas_id)
            ->when(Auth::user()->roles_id == Role::CAIXA,
                function ($q) {
                    return $q->join('users_has_events', 'events.id', '=', 'users_has_events.events_id')
                             ->where('users_has_events.users_id', Auth::user()->id);
                })
            ->whereBetween(DB::raw("DATE(`start_at`)"), [
                Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->subDays(5)->format('Y-m-d'),
                Carbon::createFromFormat('Y-m-d', date('Y-m-d'))->addDays(5)->format('Y-m-d')]
            )
            ->orderBy('start_at', 'asc')
            ->select('events.*')
            ->groupBy('events.id')
            ->paginate(15);
        return view('pdv.list_events')->with('events', $events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $event_id)
    {
        $event = Event::findOrFail($event_id);
        return view('pdv.create')->with('event', $event);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $paymentMethods = collect();
        $salesProducts = collect();
        $total_amount = 0;
        $total_value  = 0;
        $totalPayment = 0;
        $total = 0;

        foreach ($request->input('formas_pagamentos') as $item) {
            $totalPayment += Utils::moneyFormat($item['valor']);
        }
        foreach ($request->input('lista_produtos_compra') as $item) {
            $total += $item['amount'] * $item['valor'];
        }

        if ($totalPayment <= 0) {
            return response()->json(['Pagamento não pode ser R$ 0,00.'], 422);
        } else if ($totalPayment < $total) {
            return response()->json(['O valor do pagamento é menor que o total do valor de produtos na comanda.'], 422);
        } else if ($totalPayment > $total) {
            return response()->json(['O valor do pagamento é maior que o total do valor de produtos na comanda.'], 422);
        }

        /**
         * Store Sales
         */
        $sales = new Sale([
            'users_id' => $request->input('users_id'),
            'events_id' => $request->input('events_id'),
        ]);
        $sales->save();
        /**
         * created Sales products
         */

        foreach ($request->input('lista_produtos_compra') as $item) {
            if ($item['amount'] > 0) {
                $total_amount += $item['amount'];
                $total_value  += ($item['amount'] * $item['valor']);
                $salesProducts->push([
                    'sales_id' => $sales->id,
                    'produtos_id' => $item['id'],
                    'amount' => $item['amount'],
                    'value' => $item['valor'],
                ]);
            }
        }
        SaleProduct::insert($salesProducts->toArray());
        /**
         * adicionando o total de amount e value na tabela sales
         */
        $sales->total_amount = $total_amount;
        $sales->total_value  = $total_value;
        $sales->save();

        /**
         * crated Payment Methods
         */
        foreach ($request->input('formas_pagamentos') as $item) {
            $paymentMethods->push([
                'formas_pagamentos_id' => $item['id'],
                'sales_id' => $sales->id,
                'valor' => Utils::moneyFormat($item['valor']),
            ]);
        }
        PaymentMethod::insert($paymentMethods->toArray());

        return response()->json([
            'id' => $sales->id,
            'message' => 'Pagamento efetuado com sucesso.'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     * @param Request $request
     * empresas_id
     * user_id
     * event_id
     * @return array
     */
    public function carregaModelo(Request $request, int $id = 0)
    {
        $evento_id   = ($id==0)?$request->input('event_id'):$id;
        $empresas_id = ($request->has('empresas_id'))?$request->input('empresas_id'):Auth::user()->empresas_id;

        $event = Event::with(['produtos',
                              'closure' => function($q)use($request){
                                  $q->where('users_id', $request->input('user_id'));
                              },
                              'openSale' => function($query)use($request){
                                    return $query->where('users_id', $request->input('user_id'));
                              }])->find($evento_id);

        return [
            'formas_pagamentos' => ForcaVendaService::formaDePagamento(),
            //'produtos' => $event->produtos,
            'caixa_fechado' => ($event->closure != null && $event->closure->count() > 0),
            'dados_fechamento' => ($event->closure!=null && $event->closure->count()>0)?$event->closure[0]:null ,
            'produtos_tabs' => ProdutosEventsService::list($event),
            'dados_abertura_caixa' => ($event->openSale == null) ? new OpenSale() : $event->openSale
        ];
    }


    /**
     * @param Request $request
     * @param int $sale_id
     * @return $this
     */
    public function printSale(Request $request, int $sale_id)
    {
        $sale = Sale::with(['event', 'user', 'saleProducts.produto', 'paymentsMethods'])->find($sale_id);

        $sale = ProdutosEventsService::printSale($sale);
        return view('pdv.print')->with('sale', $sale);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function abrirCaixa(Request $request)
    {

        $validator = validator($request->all(),
            [
                'value' => 'required',
                'users_id' => 'required|integer',
                'events_id' => 'required|integer'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors()->all(), 400);
        }

        $openSales = new OpenSale($request->all());
        $openSales->value = Utils::moneyFormat($request->input('value'));
        $openSales->save();

        return response()->json('Valor inserido na abertuda do caixa.');
    }


    /**
     * @param int $sales_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function canceled(int $sales_id){
        try{
            $sales = Sale::find($sales_id);
            $sales->canceled = 1;
            $sales->save();
            return response()->json(['Venda '.str_pad($sales_id, 3,'0',STR_PAD_LEFT).' cancelada com sucesso.'], 200);
        }catch(\Exception $ex){
            return response()->json([$ex->getMessage()], $ex->getCode());
        }
    }



}
