<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class EventsController extends Controller
{
    public function list(Request $request)
    {
        $events = Event::query()
            ->when(!empty($request->input('search')),
                function($query) use ($request){
                    $query->where('title', 'like', '%'.$request->input('search').'%');
                },
                function($query){
                    $query->where(DB::raw("DATE_FORMAT(`start_at`, '%Y-%m-%d')"), '>=', date('Y-m-d'));
                }
            )
            ->where('visible_website', 1)
            ->orderBy('start_at')
            ->paginate(15);
        return view('events.lista')
            ->with('events', $events->appends(Request::capture()->except('page')))
            ->with('search', $request->input('search'));
    }

    public function event($slug)
    {
        $event = Event::query()
            ->where('slug', $slug)
            ->firstOrFail();
        return view('events.event')
            ->with('event', $event);
    }
}
