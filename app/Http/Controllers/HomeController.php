<?php

namespace App\Http\Controllers;

use App\Model\Accont;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $usuario = Auth::user();
        $usuario->load(['empresa.account', 'role', 'place']);
        return view('home')->with([
            'usuario' => $usuario,
            'accounts' => Accont::all(),
        ]);
    }
}
