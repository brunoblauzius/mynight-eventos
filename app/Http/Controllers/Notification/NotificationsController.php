<?php

namespace App\Http\Controllers\Notification;

use App\Events\Notification\EstoqueBaixo;
use App\Model\Notification\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        Notification::where([
            'users_id' => Auth::user()->id,
            'status' => 0
        ])->update(['status' => 1]);

        $lista = Notification::with('user')
            ->where('users_id', Auth::user()->id)
            ->orderBy('id', 'DESC')
            ->paginate(15);

        return view('notifications.index')->with('lista', $lista);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

//       $notification = new Notification([
//        'title' => 'Baixa de estoque Evento XXX',
//        'description' => 'teste de evento notification',
//        'users_id' => 1,
//        'icon' => 'fa fa-tag'
//    ]);
        $notification = Notification::with('user')->find(1);
        event(new EstoqueBaixo($notification));
        return $notification;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,int $id = null)
    {
        $lista = Notification::with('user')
                                ->where('users_id', $id)
                                ->when($request->has('status'), function ($q) use($request){
                                    $q->where('status', $request->input('status'));
                                })
                                ->orderBy('id', 'DESC')
                                ->get();

        return [
            'total' => $lista->count(),
            'nao_visualizado' => $lista->where('status', 0)->count(),
            'lista' => $lista->when($request->has('limit'), function ($q) use($request){
                return $q->slice(0,$request->input('limit'));
            })
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
