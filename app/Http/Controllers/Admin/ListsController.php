<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Lib\Utils;
use App\Lista;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        return view('admin.lists.index')
            ->with('listas', Lista::where('place_id', \Auth::user()->place->id)->get());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(),
                        [
                            'description' => 'required',
                            'amount' => 'required|integer',
                            'value' => 'required',
                            'gender' => 'required',
                        ],
                        [
                            'required' => (':attribute é um campo requirido')
                        ]
                    );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        try{
            $list = new Lista($request->all());
            $list->place_id = \Auth::user()->place->id;
            $list->value    = Utils::moneyFormat($request->input('value'));
            $list->save();
            return response()->json(['lista cadastrada com sucesso.']);
        } catch (\Exception $ex){
            return response()->json($ex->getMessage(), 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $list = Lista::find($id);
        return view('lists.edit')->with('list', $list);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $validator = validator($request->all(),
            [
                'description' => 'required',
                'amount' => 'required|integer',
                'value' => 'required|numeric',
                'gender' => 'required',
            ],
            [
                'required' => (':attribute é um campo requirido')
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 400);
        }

        try{
            $list = Lista::find($id);
            $list->description = $request->input('description');
            $list->amount      = $request->input('amount');
            $list->value       = Utils::moneyFormat($request->input('value'));
            $list->gender      = $request->input('gender');
            $list->save();
            return response()->json(['lista cadastrada com sucesso.']);
        } catch (\Exception $ex){
            return response()->json($ex->getMessage(), 500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        try{

            $list = Lista::find($id);
            $list->forceDelete();
            return response()->json('Lista: '.$list->description.' deletada com sucesso.');

        } catch(\Exception $ex){
            return response()->json($ex->getMessage(), 500);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListPromoter(Request $request){
        $event     = Event::with([
                            'eventPromoters' => function($query) use ($request){
                                return $query->where('events_promoters.user_id', $request->input('user_id'))
                                             ->join('lists', 'lists.id', '=', 'events_promoters.list_id')
                                             ->select('lists.*');
                            }])
                            ->find($request->input('event_id'));
        return response()->json($event, 200);
    }

}
