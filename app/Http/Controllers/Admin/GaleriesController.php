<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Galery;
use App\Photo;

use App\Lib\Galery\GaleryModule;

class GaleriesController extends Controller
{
    public function list()
    {
        $galery = new Galery();
        $galeries = $galery->getGaleriesByPlace();

        return view('admin.galeries.list')
            ->with('galeries', $galeries);
    }

    public function galery($galery_id)
    {
        $galery = Galery::findOrFail($galery_id);
        return view('admin.galeries.galery')
            ->with('galery', $galery);

    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'photos.*' => 'image|max:2048'
        ]);

        $galery = new Galery();
        $galery_id = $galery->createGalery($request->input('title'));

        if ($request->hasFile('photos')) {
            $photos = $request->file('photos');
            $galery_module = new GaleryModule();
            $galery_module->uploadImages($galery_id, $photos);
        }

        return redirect()->route('admin.galeries');
    }

    public function addPhotos(Request $request, $galery_id)
    {
        $this->validate($request, [
            'photos.*' => 'image|max:2048'
        ]);

        if ($request->hasFile('photos')) {
            $photos = $request->file('photos');
            $galery_module = new GaleryModule();
            $galery_module->uploadImages($galery_id, $photos);
        }

        return redirect()->route('admin.galery', ['galery_id' => $galery_id]);
    }
}
