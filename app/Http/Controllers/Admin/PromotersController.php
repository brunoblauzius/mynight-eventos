<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use App\Promoter;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PromotersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promoters = Promoter::with(['user'])
                               ->where('place_id', Auth::user()->place_id)
                               ->paginate(10);
        return view('admin.promoters.index')->with('promoters', $promoters);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.promoters.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(),
            [
                'name'     =>'required',
                'email'    =>'required|email|unique:users,email',
                'password' =>'required|min:7',
            ],
            [
                'required' => ':attribute obrigatorio.',
                'name.required' => 'nome obrigatorio.',
                'unique'   => ':attribute ja esta cadastrado.',
                'email'   => ':attribute invalido.',
                'min'   => ':attribute tem que ter no minimo :size caracteres',
            ]);

        if($validator->fails()){
            return redirect('admin/promoters/cadastro')
                ->withErrors($validator)
                ->withInput();
        }


        $usuario = new User([
           'name' => $request->input('name'),
           'email' => $request->input('email'),
           'place_id' => Auth::user()->place_id,
           'password' => bcrypt($request->input('password')),
        ]);
        $usuario->save();

        $promoter = new Promoter([
           'name'     => $request->input('name'),
           'user_id'  => $usuario->id,
           'place_id' => $usuario->place_id,
           'status'   => 1,
        ]);
        $promoter->save();


        return  redirect('admin/promoters');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $promoter = Promoter::with('user')->find($id);
        return view('admin.promoters.edit')->with('promoter', $promoter);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {

        $validator = validator($request->all(),
            [
                'name'     =>'required',
                'email'    =>'required|email',
            ],
            [
                'required' => ':attribute obrigatorio.',
                'name.required' => 'nome obrigatorio.',
                'email'   => ':attribute invalido.'
            ]);

        if($validator->fails()){
            return redirect('admin/promoters/cadastro')
                ->withErrors($validator)
                ->withInput();
        }

        $promoter = Promoter::with('user')->find($id);
        $promoter->name        = $request->input('name');
        $promoter->user->name  = $request->input('name');
        $promoter->user->email = $request->input('email');
        if($request->has('password')) {
            $promoter->user->password = bcrypt($request->input('password'));
        }
        $promoter->user->save();
        $promoter->save();
        return redirect('admin/promoters');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $promoter = Promoter::find($id);
        $promoter->status = 0;
        $promoter->save();
        return redirect('admin/promoters');
    }


    /**
     * @param int $id
     * @return $this
     */
    public function eventsPromoters(int $id){

        $event     = Event::with(['eventLists',
                                  'eventPromoters' => function($query){
                                        return $query->join('lists', 'lists.id', '=', 'events_promoters.list_id')
                                                     ->select('lists.*', 'users.*');
                                  }])
                                  ->find($id);

        $promoters = User::where('empresas_id', Auth::user()->empresas_id)
                            ->where([
                                'roles_id' => Role::PROMOTER,
                                'status'   => 1
                            ])
                            ->get();

        return view('admin.events.promoters')
            ->with('event', $event)
            ->with('promoters', $promoters);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function eventsPromotersVinculate(Request $request, int $id){
        $validator = validator($request->all(),
            [
                'user_id'  =>'required|int',
                'list_id'      =>'required|int',
                'amount'       =>'required|int',
            ],
            [
                'user_id.required' => 'Promoter e obrigatorio.',
                'list_id.required' => 'Lista e obrigatorio.',
                'amount.required' => 'Quantidade e obrigatorio.',
            ]);

        if($validator->fails()){
            return redirect('admin/events/'.$id.'/promoters')
                ->withErrors($validator)
                ->withInput();
        }

        $event = Event::find($id);

        $event->eventPromoters()->attach([
                $request->input('user_id') => [
                    'amount' => $request->input('amount'),
                    'list_id' => $request->input('list_id'),
                ]]);

        return redirect('admin/events/'.$id.'/promoters');
    }

    /**
     * @param int $event_id
     * @param int $id
     * @return $this
     */
    public function desvincularPromoter(int $event_id, int $id){
        DB::table('events_promoters')->where('id', $id)->delete();
        return redirect('admin/events/'.$event_id.'/promoters')
            ->withInput();
    }


}
