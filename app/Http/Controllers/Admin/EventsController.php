<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportEventSales;
use App\Lib\Utils;
use App\Lista;
use App\Model\Event\Category;
use App\Model\Pagamento\PaymentMethod;
use App\Model\Pagamento\Sale;
use App\Model\Pagamento\SaleProduct;
use App\Model\Produtos\Bar;
use App\Model\Produtos\Produto;
use App\PersonEventList;
use App\Promoter;
use App\Role;
use App\Services\Event\EventService;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Event;
use App\Galery;

use App\Lib\GoogleMaps\GoogleMaps;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EventsController extends Controller
{
    /**
     * @return $this
     */
    public function list()
    {
        $events = Event::where('place_id', \Auth::user()->place->id)
            ->orderBy('id', 'DESC')
            ->paginate();
        $valueTotal = 0;

        return view('admin.events.list')
            ->with('events', $events);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $categories = Category::where('status', true)->get();
        return view('admin.events.create')->with('categories', $categories);
    }

    /**
     * @param int $id
     * @return $this
     */
    public function edit(int $id)
    {
        $event = Event::query()
            ->where('id', $id)
            ->where('place_id', \Auth::user()->place->id)
            ->firstOrFail();
        $galeries = Galery::where('place_id', \Auth::user()->place->id)
            ->where('status', Galery::STATUS_ACTIVE)
            ->orderBy('title')
            ->get();
        $categories = Category::where('status', true)->get();

        return view('admin.events.edit')
            ->with([
                'event' => $event,
                'galeries' => $galeries,
                'categories' => $categories
            ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'events_categories_id' => 'required|integer',
            'start_at' => 'date|date_format:d/m/Y',
            'end_at' => 'date|date_format:d/m/Y',
        ]);

        $event = new Event();
        $event->title       = $request->input('title');
        $event->description = $request->input('description');
        $event->meta_description = $request->input('meta_description');
        $event->visible_website = $request->input('visible_website');
        $event->events_categories_id = $request->input('events_categories_id');
        $event->start_at    = $request->input('start_at').' '.$request->input('start_hour').':00';
        $event->end_at      = $request->input('end_at').' '.$request->input('end_hour').':00';
        $event->galery_id   = $request->input('galery_id');
        $event->place_id    = \Auth::user()->place->id;
        $event->empresas_id    = \Auth::user()->empresas_id;
        $event->status      = Event::STATUS_ACTIVE;
        $event->slug        = Event::getValidSlug(\Auth::user()->place->name. " " .$event->title);

        if ($request->input('local') == 1) {
            $event->address = \Auth::user()->place->address;
        } else {
            $event->address = $request->input('address');
        }

        $maps = new GoogleMaps();
        $lat_long = $maps->getLatLongByAddress($event->address);
        if (count($lat_long->results) > 0) {
            $event->lat = $lat_long->results[0]->geometry->location->lat;
            $event->long = $lat_long->results[0]->geometry->location->lng;
        }

        $event->save();

        return redirect()->route('admin.events.edit', $event->id);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required'
        ]);

        $event = Event::query()
            ->where('id', $id)
            ->where('place_id', \Auth::user()->place->id)
            ->firstOrFail();
        $event->title       = $request->input('title');
        $event->description = $request->input('description');
        $event->meta_description = $request->input('meta_description');
        $event->galery_id   = $request->input('galery_id');
        $event->visible_website = $request->input('visible_website');
        $event->events_categories_id = $request->input('events_categories_id');
        $event->start_at    = $request->input('start_at').' '.$request->input('start_hour').':00';
        $event->end_at      = $request->input('end_at').' '.$request->input('end_hour').':00';

        if ($request->input('local') == 1) {
            $event->address = \Auth::user()->place->address;
        } else {
            $event->address = $request->input('address');
        }

        $maps = new GoogleMaps();
        $lat_long = $maps->getLatLongByAddress($event->address);
        if (count($lat_long->results) > 0) {
            $event->lat = $lat_long->results[0]->geometry->location->lat;
            $event->long = $lat_long->results[0]->geometry->location->lng;
        }

        $event->save();
        return redirect()->route('admin.events.edit', $event->id);
    }

    /**
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function uploadCoverImage($id, Request $request)
    {
        $request->validate([
            'cover' => 'required'
        ]);
        $event = Event::query()
            ->where('id', $id)
            ->where('place_id', \Auth::user()->place->id)
            ->firstOrFail();

        $image_path = $request->file('cover')->storeAs("public/events/{$id}/cover", 'event-cover-image.jpg');
        $event->image = str_replace('public/', '', $image_path);
        $event->save();

        return redirect()->route('admin.events.edit', ['id' => $id]);
    }


    /**
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function eventLists(int $id){
        $lists  = Lista::findPlace(Auth::user()->place->id)->get();
        $events = Event::with('eventLists')->find($id);
        return view('admin.events.lists-vincular')->with('lists',$lists)
                    ->with('event_id', $id)
                    ->with('lista_vinculado',$events->eventLists->pluck('id')->toArray());
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function attachLists(Request $request){
        try{
            $event = Event::find($request->input('event_id'));
            if($request->input('vinculado') == 1){
                $event->eventLists()->detach($request->input('lists_id'));
                return response()->json('Removida com sucesso.');
            } else {
                $event->eventLists()->attach([$request->input('lists_id')]);
                return response()->json('Lista adicionada com sucesso.');
            }

        } catch(\Exception $ex){
            return response()->json($ex->getMessage(), 500);
        }
    }


    /**
     * @param Request $request
     * @param int $id
     * @return $this
     */
    public function minhaLista(Request $request, int $id){
        $event = Event::with(['eventPromoters' => function($q){
            return $q->groupBy('user_id');
        },
        'minhaLista' => function($query){
            return $query->with(['user', 'lista']);
        }
        ])->find($id);

        $male   = 0;
        $female = 0;
        $unisex = 0;
        foreach ( $event->minhaLista as $item ){
            if($item->lista->gender == 1){
                $female++;
            } elseif ( $item->lista->gender == 2 ){
                $male++;
            } else {
                $unisex++;
            }
        }

        $totalPersonInList = [
            'male'   => $male,
            'female' => $female,
            'unisex' => $unisex,
        ];

        return view('admin.events.minha-lista')
                ->with('total', $totalPersonInList)
                ->with('event', $event);
    }


    /**
     * @param Request $request
     * @return $this
     */
    public function attachPersonEventList(Request $request){
        $validator = validator($request->all(),
            [
                'event_id' => 'required',
                'user_id'  => 'required',
                'list_id'  => 'required',
                'name'     => 'required',
            ],
            []);
        $event_id = $request->input('event_id');
        if($validator->fails()){
            return redirect('admin/events/'.$event_id.'/minha-lista')->withErrors($validator)->withInput();
        }
        $user = User::find($request->input('user_id'));
        $attachPerson = new PersonEventList([
           'event_id' => $request->input('event_id'),
           'user_id'  => $user->id,
           'list_id'  => $request->input('list_id'),
           'name'     => $request->input('name'),
        ]);
        $attachPerson->save();
        return redirect('admin/events/'.$event_id.'/minha-lista')->withInput();
    }


    /**
     * @param int $id
     * @return $this
     */
    public function portaria(Request $request, int $id){
        $event = Event::find($id);

        $listas = DB::table('persons_events_lists')
                    ->select([
                        DB::raw('users.name promoter'),
                        DB::raw('lists.description, gender'),
                        DB::raw('persons_events_lists.*')
                    ])
                    ->when($request->has('search'), function($query)use($request){
                        return $query->where('persons_events_lists.name', 'like', '%'.$request->input('search').'%');
                    })
                    ->where('persons_events_lists.event_id', $event->id)
                    ->join('users', 'users.id', '=', 'persons_events_lists.user_id')
                    ->join('lists', 'lists.id',  '=', 'persons_events_lists.list_id')
                    ->orderBy('persons_events_lists.name', 'asc')
                    ->paginate(15);

        return view('admin.events.portaria')
                ->with('listas', $listas)
                ->with('event', $event);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return $this
     */
    public function statistic(Request $request, int $id){
        $evento = Event::with([
            'eventBar' => function($query){
                return $query->with('produto');
            },
            'caixas',
            'closure',
                'sales' => function($q){
                    return $q->where('canceled', 0);
                }
            ])->find($id);
        $valueTotal = 0;

        /**
         * prdutos mais vendidos
         */
        $produtosMaisVendidos = EventService::produtosMaisVendios($id);

        /**
         * recuperando as vendas por usuarios
         */
        $salesPerUser = EventService::vendasPorUsuarios($id);

        /**
         * pagamentos
         */
        $payment_sales = EventService::paymentSales($id);

        /**
         * estoque
         */
        $sale = EventService::estoque($id);


        foreach ($evento->eventBar as $bar ){
            $valueTotal += ($bar->amount * $bar->value);
        }
        return view('admin.events.statistic')
            ->with('evento', $evento)
            ->with('products_sold', $sale)
            ->with('sales_per_user', $salesPerUser)
            ->with('payment_sales', $payment_sales)
            ->with('produtos_mais_vendidos', $produtosMaisVendidos)
            ->with('valueTotal', number_format($valueTotal, 2,',','.'));
    }


    /**
     * @param Request $request
     * @param int $id
     * @return $this
     */
    public function createBar(Request $request, int $id){
        $evento   = Event::with(['eventBar' => function($query){
                        return $query->with('produto');
                    }])->find($id);
        $produtos = Produto::where('empresas_id', Auth::user()->empresas_id)->get();
        return view('admin.events.create-bar')
            ->with([
                'event'   => $evento,
                'produtos' => $produtos
            ]);
    }


    /**
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function eventsBarVinculate(Request $request, int $id){
        $bar = new Bar($request->all());
        $bar->events_id = $id;
        $bar->value     = Utils::moneyFormat($request->input('value'));
        $bar->save();
        return redirect()->route('admin.events.createBar', $id);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function eventsBarUpdateVinculate(Request $request){

        $validator = validator(
                        $request->all(),
                        [
                            'id' => 'required|integer',
                            'amount' => 'required|integer',
                            'value' => 'required',
                        ]
                    );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 422);
        }

        $bar = Bar::find($request->input('id'));
        $bar->value     = Utils::moneyFormat($request->input('value'));
        $bar->amount    = intval($request->input('amount'));
        $bar->save();
        return redirect()->route('admin.events.createBar', $bar->events_id);
    }

    /**
     * @param int $event_id
     * @param int $produto_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function barDesvinculate(int $event_id , int $produto_id){
        Bar::where([
            'events_id' => $event_id,
            'produtos_id' => $produto_id,
        ])->forceDelete();
        return redirect()->route('admin.events.createBar', $event_id);
    }

    /**
     * @param int $event_id
     * @param int $produto_id
     * @return $this
     */
    public function barEdit(int $event_id , int $produto_id){

        $event = Event::find($event_id);

        $produto = Bar::where([
            'events_id' => $event_id,
            'produtos_id' => $produto_id,
        ])->first();

        $produtos = Produto::where('empresas_id', Auth::user()->empresas_id)->get();

        return view('admin.events.edit-bar')
            ->with('event', $event)
            ->with('produtos', $produtos)
            ->with('product', $produto);
    }
    /**
     * @param Request $request
     * @param int $id
     * @return $this
     */
    public function createCaixa(Request $request, int $id){
        $evento   = Event::with(['caixas'])->find($id);
        $caixas   = User::where([
            'roles_id'    => Role::CAIXA,
            'empresas_id' => Auth::user()->empresas_id,
        ])->get();

        return view('admin.events.vicular-caixa')
            ->with([
                'event'   => $evento,
                'users'   => $caixas,
            ]);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return $this
     */
    public function vincularCaixa(Request $request, int $id){
        $evento   = Event::find($id);
        if($request->isMethod('post')){
            $evento->caixas()->attach([$request->input('users_id')]);
        }
        return redirect()->route('admin.events.createCaixa', $id);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return $this
     */
    public function desvincularCaixa(Request $request, int $id, int $user_id){
        $evento   = Event::find($id);
        $evento->caixas()->detach($user_id);
        return redirect()->route('admin.events.createCaixa', $id);
    }


    /**
     * @param int $event_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function paymentSales(int $event_id){
        /**
         * pagamentos
         */
        $new_list = collect();
        $new_list->push(['Task', 'Hours per Day']);
        $payment_sales = EventService::paymentSales($event_id);
        foreach ( $payment_sales as $type ){
            $new_list->push([$type->nome, floatval($type->valor)]);
        }
        return response()->json($new_list);
    }


    /**
     * @param int $id
     * @return \Illuminate\Http\Response|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(int $id){
        $event = Event::find($id);
        return (new ExportEventSales($event))->download($event->slug.'.xlsx');
    }

}
