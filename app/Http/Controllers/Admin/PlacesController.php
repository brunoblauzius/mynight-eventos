<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Place;

class PlacesController extends Controller
{
    public function edit()
    {
        $place = \Auth::user()->place;
        return view('admin.places.edit')
            ->with('place', $place);
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $place = Place::query()->findOrFail(\Auth::user()->place->id);
        $place->name = $request->input('name');
        $place->slug = str_slug($request->input('name'), '-');
        $place->phone = $request->input('phone');
        $place->address = $request->input('address');
        $place->description = $request->input('description');
        $place->hora_abertura = $request->input('hora_abertura');
        $place->hora_fechamento = $request->input('hora_fechamento');
        $place->dom = $request->input('dom');
        $place->seg = $request->input('seg');
        $place->ter = $request->input('ter');
        $place->qua = $request->input('qua');
        $place->qui = $request->input('qui');
        $place->sex = $request->input('sex');
        $place->sab = $request->input('sab');
        $place->save();

        return redirect()->route('admin.place.edit');
    }

    public function uploadPhoto(Request $request)
    {
        $request->validate([
            'photo' => 'required'
        ]);

        $place = Place::findOrFail(\Auth::user()->place->id);
        $image_path = $request->file('photo')->storeAs("public/places/{$place->id}", 'photo.jpg');
        $place->photo = str_replace('public/', '', $image_path);
        $place->save();

        return redirect()->route('admin.place.edit');
    }
}
