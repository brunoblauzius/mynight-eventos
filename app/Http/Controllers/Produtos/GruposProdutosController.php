<?php

namespace App\Http\Controllers\Produtos;

use App\Model\Produtos\GrupoProduto;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class GruposProdutosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grupos = GrupoProduto::where('default', 1)
                                ->orWhere('empresas_id', Auth::user()->empresas_id)
                                ->paginate(15);
        return view('gruposProdutos.index')->with('grupos', $grupos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator($request->all(),
            [
                'descricao' => 'required',
                'status'    => 'required',
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 422);
        }

        $grupo = new GrupoProduto($request->all());
        $grupo->empresas_id = Auth::user()->empresas_id;
        $grupo->save();
        return response()->json(['grupo adicionado com sucesso.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $grupo = GrupoProduto::find($id);
        return response()->json($grupo);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id = null)
    {
        $validator = validator($request->all(),
            [
                'descricao' => 'required',
                'status'    => 'required',
                'id'        => 'integer',
            ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->all(), 422);
        }
        $grupo = GrupoProduto::find($request->input('id'));
        $grupo->fill($request->all());
        $grupo->save();
        return response()->json(['Grupo editado com sucesso.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        try{
            $grupo = GrupoProduto::find($id);
            $grupo->forceDelete();
            return response()->json(['grupo excluído com sucesso.']);
        } catch (\Exception $ex){
            return response()->json([$ex->getMessage()], 500);
        }
    }
}
