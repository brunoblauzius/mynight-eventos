<?php

namespace App\Http\Controllers\Produtos;

use App\Lib\Utils;
use App\Model\Produtos\GrupoProduto;
use App\Model\Produtos\Produto;
use App\Services\ProdutoImageService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProdutosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produtos = Produto::where('empresas_id', Auth::user()->empresas_id)->orderBy('name', 'asc')->paginate(20);
        return view('produtos.index')->with('produtos', $produtos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $grupoProdutos = GrupoProduto::where('default', 1)
                                    ->orWhere('empresas_id', Auth::user()->empresas_id)
                                    ->orderBy('descricao', 'asc')
                                    ->get();
        return view('produtos.create')->with('grupoProdutos', $grupoProdutos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $photo = [];
        $validator = validator(
            $request->all(),
            [
                'name' => 'required|unique:produtos,name',
                'valor'     => 'required',
                'produto_grupos_id'     => 'required|integer',
                'status'    => 'required',
            ]
        );

        if($validator->fails()){
            return redirect()->route('admin.produto.create')->withErrors($validator)->withInput();
        }

        $produto = new Produto($request->all());
        $produto->empresas_id = Auth::user()->empresas_id;
        $produto->valor       = Utils::moneyFormat($request->input('valor'));
        $produto->save();
        if($request->hasFile('cover') && !empty($request->file('cover'))){
            $photo = ProdutoImageService::upload($produto->id, $request->file('cover'));
        }
        $produto->fill(array_merge($photo, $request->all()));
        $produto->save();
        return redirect()->route('produtos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $id)
    {
        $produto = Produto::find($id);
        $grupoProdutos = GrupoProduto::where('default', 1)
            ->orWhere('empresas_id', Auth::user()->empresas_id)
            ->orderBy('descricao', 'asc')
            ->get();
        return view('produtos.edit')->with('grupoProdutos', $grupoProdutos)->with('produto',$produto);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $photo = [];
        $validator = validator(
            $request->all(),
            [
                'name'      => 'required',
                'valor'     => 'required',
                'status'    => 'required',
            ]
        );

        if($validator->fails()){
            return redirect()->route('admin.produto.update', $id)->withErrors($validator)->withInput();
        }

        $produto              = Produto::find($id);
        if($request->hasFile('cover') && !empty($request->file('cover'))){
            $photo = ProdutoImageService::upload($produto->id, $request->file('cover'));
        }
        $produto->fill(array_merge($photo, $request->all()));
        $produto->valor       = Utils::moneyFormat($request->input('valor'));
        $produto->save();
        return redirect()->route('produtos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $produto = Produto::find($id);
        $produto->forceDelete();
        return response()->json(['Produto deletado com sucesso.']);
    }

    /**
     * get all products by company
     * @param Request $request
     * @return mixed
     */
    public function all(Request $request){
        if($request->has('empresas_id')){
            $empresas_id = $request->input('empresas_id');
        } else {
            $empresas_id = Auth::user()->empresas_id;
        }
        return Produto::where([
            'empresas_id' => $empresas_id,
            'status'      => 1
        ])->get();
    }
}
