<?php

if (! function_exists('cover_image')) {
    function cover_image($image)
    {
        if (file_exists(public_path('storage/'. $image))) {
            return asset('storage/'. $image);
        } else {
            return 'http://placehold.it/1980x600';
        }
    }
}

if (!function_exists('place_photo')) {
    function place_photo($photo)
    {
        if (file_exists(public_path('storage/'. $photo))) {
            return asset('storage/'. $photo);
        } else {
            return 'http://placehold.it/200x200';
        }
    }
}
