<?php

namespace App\Lib\GoogleMaps;

class GoogleMaps
{
    const API_URL = 'https://maps.googleapis.com/maps/api/geocode/json';
    const API_KEY = 'AIzaSyCAUErdhbm98UVF1T7VdnDH-w-dXgacVsQ';

    public function getLatLongByAddress($address) {
        $client = new \GuzzleHttp\Client();
        $params = [
            'key' => self::API_KEY,
            'address' => $address
        ];
        $res = $client->request('GET', self::API_URL . '?' . http_build_query($params));
        return json_decode($res->getBody());
    }
}
