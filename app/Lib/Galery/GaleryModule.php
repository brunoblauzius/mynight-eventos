<?php

namespace App\Lib\Galery;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Image;

use App\Photo;

class GaleryModule
{
    public function uploadImages($galery_id, $photos)
    {
        foreach ($photos as $photo) {
            $paths = $this->upload($galery_id, $photo);
            Photo::photoStore($galery_id, $paths);
        }
    }

    public function upload($galery_id, $photo)
    {
        $filename = $this->getUniqueFilename("public/galeries/{$galery_id}/", $photo);
        $photo_path = $photo->storeAs("public/galeries/{$galery_id}",  $filename);
        $thumb_path = $this->generateThumb($galery_id, $photo, $filename);

        return [
            'photo_path' => ltrim($photo_path, "public/"),
            'thumb_path' => $thumb_path
        ];
    }

    private function getUniqueFilename($path, $photo)
    {
        $original_name = $photo->getClientOriginalName();
        $filename = pathinfo($original_name, PATHINFO_FILENAME);
        $extension = pathinfo($original_name, PATHINFO_EXTENSION);
        while (Storage::exists($path . $filename .'.'. $extension)) {
            $filename = $filename . 0;
        }

        return $filename . '.' . $extension;
    }

    private function generateThumb($galery_id, $photo, $filename)
    {
        $thumb_path = storage_path('app/public/galeries/' . $galery_id . '/thumb');
        File::exists($thumb_path) or File::makeDirectory($thumb_path);

        $thumb = Image::make($photo->getRealPath());
        $thumb->fit(450, 300);
        $thumb->save($thumb_path . '/' . $filename);

        return "galeries/{$galery_id}/thumb/{$filename}";
    }
}
