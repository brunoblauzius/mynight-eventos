<?php

namespace App;

use App\Model\Pagamento\CloseSale;
use App\Model\Pagamento\OpenSale;
use App\Model\Pagamento\Sale;
use App\Model\Produtos\Bar;
use App\Model\Produtos\Produto;
use App\Observers\EventObserver;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    const STATUS_ACTIVE = 1;

    protected $guarded = ['id'];


    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function galery()
    {
        return $this->belongsTo(Galery::class);
    }

    public static function getValidSlug($slug) : string {
        $valid_slug = str_slug($slug);
        $cont = 1;
        while (self::query()->where('slug', $valid_slug)->count() != 0) {
            $valid_slug = $slug . $cont;
            $cont++;
        }
        return $valid_slug;
    }

    public function eventBar(){
        return $this->hasMany(Bar::class,'events_id','id');
    }

    public function sales(){
        return $this->hasMany(Sale::class,'events_id','id');
    }

    public function closure(){
        return $this->hasMany( CloseSale::class, 'events_id', 'id');
    }

    public function openSale(){
        return $this->hasOne(OpenSale::class,'events_id','id');
    }

    public function produtos(){
        return $this->belongsToMany(Produto::class,'events_produtos', 'events_id', 'produtos_id');
    }

    public function caixas(){
        return $this->belongsToMany(User::class,'users_has_events', 'events_id', 'users_id');
    }

    public function eventLists(){
        return $this->belongsToMany(Lista::class,'events_lists','event_id','list_id');
    }

    public function eventPromoters(){
        return $this->belongsToMany(User::class,'events_promoters','event_id', 'user_id')
            ->withPivot([
                'amount', 'list_id', 'id'
            ]);
    }

    public function eventPromotersList(){
        return $this->belongsToMany(Lista::class,'events_promoters','event_id', 'list_id')
            ->withPivot([
                'amount', 'user_id', 'id'
            ]);
    }


    public function minhaLista(){
        return $this->hasMany(PersonEventList::class,'event_id', 'id');
    }


    public function setStartAtAttribute($value){
        $this->attributes['start_at'] = Carbon::createFromFormat('d/m/Y H:i:s', $value)->format('Y-m-d H:i:s');
    }

    public function getExibeDataIniAttribute(){
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['start_at'])->format('d/m/Y');
    }

    public function getExibeHoraIniAttribute(){
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['start_at'])->format('H:i');
    }

    public function getExibeStartAtAttribute(){
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['start_at'])->format('d/m/Y H:i:s');
    }

    public function getDataStartAttribute(){
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['start_at'])->format('d/m/Y');
    }

    public function getHoraStartAttribute(){
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['start_at'])->format('H:i:s');
    }

    public function setEndAtAttribute($value){
        $this->attributes['end_at'] = Carbon::createFromFormat('d/m/Y H:i:s', $value)->format('Y-m-d H:i:s');
    }

    public function getExibeEndAtAttribute(){
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['end_at'])->format('d/m/Y H:i:s');
    }

    public function getExibeDataFimAttribute(){
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['end_at'])->format('d/m/Y');
    }

    public function getExibeHoraFimAttribute(){
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['end_at'])->format('H:i');
    }
}
