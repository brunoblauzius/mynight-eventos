<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lista extends Model
{
    protected $table = 'lists';
    protected $primaryKey = 'id';
    protected $guarded = ['id'];


    public function scopeFindPlace($query, int $place_id){
        return $query->where('place_id', $place_id);
    }


}
