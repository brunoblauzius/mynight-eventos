<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Auth;

class Galery extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const ITEMS_PER_PAGE = 9;

    public function photos()
    {
        return $this->hasMany(Photo::class);
    }

    public function getGaleriesByPlace()
    {
        $model = new self;
        return $model->where('place_id', Auth::user()->place->id)
            ->where('status', self::STATUS_ACTIVE)
            ->orderBy('id', 'DESC')
            ->paginate(self::ITEMS_PER_PAGE);
    }

    public function createGalery($title)
    {
        $model = new self;
        $model->title = $title;
        $model->place_id = Auth::user()->place->id;
        $model->status = self::STATUS_ACTIVE;
        $model->save();

        return $model->id;
    }
}
