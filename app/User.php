<?php

namespace App;

use App\Model\Empresa;
use Carbon\Carbon;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    public function scopeFindRememberToken($query, string $token){
        return $query->where('remember_token', $token);
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $guarded = ['confirm_password'];

    public function place()
    {
        return $this->belongsTo(Place::class);
    }


    public function empresa()
    {
        return $this->belongsTo(Empresa::class, 'empresas_id');
    }

    public function promoter()
    {
        return $this->hasOne(Promoter::class, 'user_id', 'id');
    }

    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'roles_id');
    }


    public function getCriadoEmAttribute()
    {
        return Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['created_at'])->format('d/m/Y');
    }

}
