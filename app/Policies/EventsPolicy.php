<?php

namespace App\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class EventsPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
    }


    public function list(User $user){
        return in_array($user->roles_id, [Role::GERENTE, Role::PROPRIETARIO, Role::ADM]);
    }

}
