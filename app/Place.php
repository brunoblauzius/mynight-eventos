<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    protected $guarded = ['id'];

    static function getSlug($name)
    {
        $slug = str_slug($name);
        while (self::query()->where('slug', $slug)->count() > 0) {
            $slug = $slug.'-0';
        }
        return $slug;
    }


    /**
     * get events
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function events(){
        return $this->hasMany(Event::class, 'place_id');
    }
}
