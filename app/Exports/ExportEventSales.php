<?php
/**
 * Created by PhpStorm.
 * User: Bruno
 * Date: 13/11/2018
 * Time: 22:55
 */

namespace App\Exports;


use App\Event;
use App\Services\PDV\ForcaVendaService;
use App\Services\PDV\SalesServices;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromView;

class ExportEventSales implements FromView
{
    use Exportable;

    private $event = null;

    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        return view('exports.event-sales')
            ->with('sales', SalesServices::sales($this->event))
            ->with('methods_payment', ForcaVendaService::formaDePagamento())
            ->with('sales_payment', SalesServices::paymentMethod($this->event));
    }
}