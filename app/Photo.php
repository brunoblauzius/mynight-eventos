<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public function galery()
    {
        return $this->belongsTo(Galery::class);
    }

    public static function photoStore($galery_id, $paths)
    {
        $photo = new self;
        $photo->galery_id = $galery_id;
        $photo->file = $paths['photo_path'];
        $photo->thumb = $paths['thumb_path'];
        $photo->order = 0;
        return $photo->save();
    }
}
