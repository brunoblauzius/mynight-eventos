<?php
/**
 * Created by PhpStorm.
 * User: Bruno
 * Date: 13/11/2018
 * Time: 23:01
 */

namespace App\Services\PDV;


use App\Event;
use App\Model\Pagamento\Sale;
use Illuminate\Support\Facades\DB;

class SalesServices
{

    public static function sales(Event $event){

        return DB::select("SELECT 
                                sales.id AS codigo_venda,
                                events.title as evento,
                                users.name as usuario,
                                produtos.name as produto,
                                sales_products.value AS valor,
                                sales_products.amount AS qtde,
                                sales_products.amount * sales_products.value AS total,
                                sales.created_at AS criado_em,
                                (CASE
                                    WHEN (sales.canceled = ?) THEN 'CANCELADO'
                                    ELSE ''
                                END) AS status
                            FROM
                                sales_products
                                    INNER JOIN
                                sales ON sales.id = sales_products.sales_id
                                    INNER JOIN
                                produtos ON sales_products.produtos_id = produtos.id
                                    INNER JOIN
                                users ON sales.users_id = users.id
                                    INNER JOIN
                                events ON sales.events_id = events.id
                            WHERE
                                sales.events_id = ?;",[1, $event->id] );
    }


    public static function paymentMethod(Event $event){
        return Sale::with([
            'paymentsMethods.formaDePagamento'
        ])->where('events_id', $event->id)
          ->get();
    }

}