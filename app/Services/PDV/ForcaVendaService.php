<?php
/**
 * Created by PhpStorm.
 * User: Bruno
 * Date: 15/11/2018
 * Time: 13:30
 */

namespace App\Services\PDV;


class ForcaVendaService
{

    public static function formaDePagamento(){
        return [
                    [
                        'id' => 1,
                        'nome' => 'Crédito',
                        'valor' => 0.00
                    ],
                    [
                        'id' => 2,
                        'nome' => 'Débito',
                        'valor' => 0.00
                    ],
                    [
                        'id' => 3,
                        'nome' => 'Dinheiro',
                        'valor' => 0.00
                    ],
                    [
                        'id' => 8,
                        'nome' => 'Voucher',
                        'valor' => 0.00
                    ]
                ];
    }

}