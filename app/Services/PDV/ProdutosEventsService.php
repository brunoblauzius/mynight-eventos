<?php
/**
 * Created by PhpStorm.
 * User: Bruno
 * Date: 23/10/2018
 * Time: 22:49
 */

namespace App\Services\PDV;


use App\Event;
use App\Model\Pagamento\Sale;
use Illuminate\Support\Collection;

class ProdutosEventsService
{

    public static function list( Event $event ) : Collection{
        $event->load('eventBar.produto.grupo');
        $nova_lista = new Collection();
        $label = new Collection();
        foreach ( $event->eventBar as $lista ){
            if($label->count() == 0){
                $label->push($lista->produto->grupo);
            } else if(!in_array($lista->produto->grupo->id, $label->pluck('id')->toArray())){
                $label->push($lista->produto->grupo);
            }
        }
        $nova_lista->offsetSet('labels', $label);

        //dd($event->eventBar);

        foreach ( $event->eventBar as $lista ){
            foreach ( $label as $key => $item ){
                if($item->id == $lista->produto->produto_grupos_id){
                    unset($lista->produto->grupo);
                    $lista->produto->valor = $lista->value;
                    $produtos[intval($item->id)][] = $lista->produto;
                }
            }
        }
        $nova_lista->offsetSet('produtos', $produtos);

        return $nova_lista;
    }


    /**
     * @param Sale $sale
     * @return Sale
     */
    public static function printSale(Sale $sale)
    {
        $newList = collect();
        foreach ( $sale->saleProducts as $produto ){
            if($produto->amount > 1){
                for($i = 0; $i < ($produto->amount); $i++) {
                    $newList->push($produto);
                }
            } else {
                $newList->push($produto);
            }
        }
        $sale->saleProducts = $newList;
        return $sale;
    }


}