<?php
/**
 * Created by PhpStorm.
 * User: Bruno
 * Date: 04/11/2018
 * Time: 14:15
 */

namespace App\Services\Event;


use App\Model\Pagamento\Sale;
use App\Model\Pagamento\SaleProduct;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class EventService
{

    /**
     * @param int $id
     * @return mixed
     */
    public static function estoque(int $id) : Collection
    {
        $estoque = DB::select('SELECT 
                                    produtos.id,
                                    (events_produtos.amount - SUM(sales_products.amount)) AS estoque
                                FROM
                                    sales
                                        INNER JOIN
                                    sales_products ON sales.id = sales_products.sales_id
                                        INNER JOIN
                                    events_produtos ON (sales.events_id = events_produtos.events_id AND sales_products.produtos_id = events_produtos.produtos_id)
                                        INNER JOIN 
                                    produtos ON produtos.id = sales_products.produtos_id
                                WHERE
                                    sales.events_id = ?
                                    AND sales.canceled = ?
                                GROUP BY sales_products.produtos_id;', [$id, 0]);
        return collect($estoque);
    }


    /**
     * @param int $id
     * @param int|null $user_id
     * @return Collection
     */
    public static function paymentSales(int $id, int $user_id = null) : Collection{
        $PARAM = '';
        $params = [$id , 0];
        if($user_id != null){
            $PARAM = ' AND sales.users_id = ? ';
            $params = array_merge($params, [$user_id]);
        }

        $payment = DB::select("
            SELECT 
                formas_pagamentos.nome,
                SUM(payment_methods.valor) AS valor
            FROM
                payment_methods
                    INNER JOIN
                sales ON sales.id = payment_methods.sales_id
                    INNER JOIN
                formas_pagamentos ON formas_pagamentos.id = payment_methods.formas_pagamentos_id
            WHERE
                sales.events_id = ?
                    AND sales.canceled = ?
                    {$PARAM}
            GROUP BY payment_methods.formas_pagamentos_id
            ORDER BY formas_pagamentos.nome ASC;
        ", $params);
        return collect($payment);
    }


    /**
     * @param int $id
     * @param int|null $user_id
     * @return Collection
     */
    public static function produtosMaisVendios(int $id, int $user_id = null) : Collection
    {
        return SaleProduct::with(['sale','produto'])
            ->select([
                DB::raw('sales_products.id '),
                DB::raw('sales_products.produtos_id'),
                DB::raw('sales_products.sales_id'),
                DB::raw('(SUM(sales_products.amount) * sales_products.value) as value'),
                DB::raw('SUM(sales_products.amount) as amount'),
            ])
            ->join('sales', 'sales.id', '=', 'sales_products.sales_id')
            ->when($user_id!=null, function($q) use($user_id){
                $q->where('sales.users_id', $user_id);
            })
            ->where('sales.events_id', $id)
            ->where('sales.canceled', 0)
            ->groupBy('sales_products.produtos_id')
            ->orderBy('amount', 'DESC')
            ->orderBy('value', 'DESC')
            ->get();
    }


    /**
     * @param int $id
     * @param int|null $user_id
     * @return Collection
     */
    public static function vendasCanceladas(int $id, int $user_id = null) : Collection{
        $sales = Sale::with('saleProducts')
                        ->when($user_id!=null, function ($q)use($user_id){
                            $q->where('users_id', $user_id);
                        })
                        ->where(
                            ['events_id' => $id, 'canceled'  => 1]
                        )->get();
        return $sales;
    }

    /**
     * @param int $id
     * @return Collection
     */
    public static function vendasPorUsuarios(int $id) : Collection{
        return Sale::with(['user'])
            ->select(DB::raw('SUM(total_amount) as total_amount, SUM(total_value) as total_value, users_id'))
            ->groupBy('users_id')
            ->where('events_id', $id)
            ->where('canceled', 0)
            ->get();
    }


}