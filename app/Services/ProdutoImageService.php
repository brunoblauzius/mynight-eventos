<?php
/**
 * Created by PhpStorm.
 * User: Bruno
 * Date: 15/10/2018
 * Time: 13:10
 */

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Image;

class ProdutoImageService
{


    public static function upload(int $id, $photo)
    {

        $filename = self::getUniqueFilename("/public/produtos/{$id}/", $photo);
        $photo_path = $photo->storeAs("/public/produtos/{$id}",  $filename);
        $thumb_path = self::generateThumb($id, $photo, $filename);

        return [
            'photo_path' => str_replace('public/', null, $photo_path),
            'thumb_path' => $thumb_path,
            'image'      => $filename,
        ];
    }

    private static function getUniqueFilename($path, $photo)
    {
        $original_name = $photo->getClientOriginalName();
        $filename = pathinfo($original_name, PATHINFO_FILENAME);
        $extension = pathinfo($original_name, PATHINFO_EXTENSION);
        while (Storage::exists($path . $filename .'.'. $extension)) {
            $filename = $filename . 0;
        }

        return $filename . '.' . $extension;
    }

    private static function generate(int $id, $photo, $filename)
    {
        $thumb_path = storage_path('app/public/produtos/' . $id );
        File::exists($thumb_path) or File::makeDirectory($thumb_path);

        $thumb = Image::make($photo->getRealPath());
        $thumb->fit(300, 300);
        $thumb->save($thumb_path . '/' . $filename);
        return "/produtos/{$id}/{$filename}";
    }

    private static function generateThumb(int $id, $photo, $filename)
    {
        $thumb_path = storage_path('app/public/produtos/' . $id . '/thumb');
        File::exists($thumb_path) or File::makeDirectory($thumb_path);

        $thumb = Image::make($photo->getRealPath());
        $thumb->fit(100, 100);
        $thumb->save($thumb_path . '/' . $filename);

        return "/produtos/{$id}/thumb/{$filename}";
    }

}