<?php
/**
 * Created by PhpStorm.
 * User: Bruno
 * Date: 13/08/2018
 * Time: 21:45
 */

namespace App\Services;


use App\Lib\GoogleMaps\GoogleMaps;

class GoogleMapService
{

    public static function getAddress(string $address) : array {
        $maps = new GoogleMaps();
        $endereco = [];
        $lat_long = $maps->getLatLongByAddress($address);
        if (count($lat_long->results) > 0) {
            $resultado = $lat_long->results[0]->address_components;
            $endereco = [
                'numero'     => $resultado[0]->long_name,
                'logradouro' => $resultado[1]->long_name,
                'municipio'  => $resultado[2]->long_name,
                'cidade'     => $resultado[3]->long_name,
                'estado'     => $resultado[4]->long_name,
                'pais'       => $resultado[5]->long_name,
                'cep'        => $resultado[6]->long_name,
                'lat'        => $lat_long->results[0]->geometry->location->lat,
                'long'        => $lat_long->results[0]->geometry->location->lng,
            ];
        }
        return $endereco;
    }

}