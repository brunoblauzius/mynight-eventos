<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    const ADM   = 1;
    const PROPRIETARIO   = 2;
    const PROMOTER   = 3;
    const GERENTE    = 4;
    const CAIXA = 6;


    protected $table = 'roles';

    protected $guarded = ['id'];

}
